#
#  Copyright (c) 2020 - 2022 Henix, henix.fr
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

FROM maven:3.6.3-adoptopenjdk-11 AS java-retriever

COPY docker/mvn_repository_settings.xml /root/.m2/settings.xml

ARG JAVA_PLUGINS_VERSION
ARG ALLURE_VERSION
ARG MAVEN_USERNAME
ARG MAVEN_PASSWORD
RUN sh -xe -c "mkdir -p /java-services/ ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-allure-report-collector:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-allure-report-collector.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-cucumber-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-cucumber-report-interpreter.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-cypress-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-cypress-report-interpreter.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-postman-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-postman-report-interpreter.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-result-aggregator:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-result-aggregator.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-robotframework-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-robotframework-report-interpreter.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-soapui-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-soapui-report-interpreter.jar ;\
        mvn dependency:get -Dartifact=org.opentestfactory.plugins:otf-surefire-report-interpreter:${JAVA_PLUGINS_VERSION}:jar -Dtransitive=false -Ddest=/java-services/otf-surefire-report-interpreter.jar;\
        apt update; apt install -y unzip;\
        cd /opt;mvn dependency:copy -Dartifact=io.qameta.allure:allure-commandline:${ALLURE_VERSION}:zip -DoutputDirectory=/opt;\
        unzip /opt/allure-commandline-${ALLURE_VERSION}.zip -d /opt"

FROM python:3.8-slim

ARG version
ARG ALLURE_VERSION
ARG QUALITY_GATE_VERSION

WORKDIR /app

COPY pip.conf /root/.config/pip/pip.conf
COPY conf /app/conf
COPY conf/squashtf.yaml /app

RUN pip3 install --no-cache-dir opentf-orchestrator===${version}
RUN pip3 install --no-cache-dir opentf-qualitygate===${QUALITY_GATE_VERSION}
# This should become unnecessary in the future
RUN mkdir -p /app/services/qualitygate;mv /usr/local/lib/python3.8/site-packages/opentf/qualitygate/service.yaml /app/services/qualitygate

RUN sh -xe -c "mkdir -p /usr/share/man/man1/;apt update;apt -y install openjdk-11-jre-headless;apt-get clean;rm -rvf /usr/share/man/man1/"
COPY --from=java-retriever /java-services /app/plugins/java
COPY --from=java-retriever /opt/allure-${ALLURE_VERSION} /opt/allure-commandline
ENV PATH=${PATH}:/opt/allure-commandline/bin
COPY conf/java-manifests/allure-collector_plugin.yaml /app/plugins/allure-collector/plugin.yaml
COPY conf/java-manifests/cucumber-interpreter_plugin.yaml /app/plugins/cucumber-interpreter/plugin.yaml
COPY conf/java-manifests/cypress-interpreter_plugin.yaml /app/plugins/cypress-interpreter/plugin.yaml
COPY conf/java-manifests/postman-interpreter_plugin.yaml /app/plugins/postman-interpreter/plugin.yaml
COPY conf/java-manifests/result-aggregator_plugin.yaml /app/plugins/result-aggregator/plugin.yaml
COPY conf/java-manifests/robotframework-interpreter_plugin.yaml /app/plugins/robotframework-interpreter/plugin.yaml
COPY conf/java-manifests/soapui-interpreter_plugin.yaml /app/plugins/soapui-interpreter/plugin.yaml
COPY conf/java-manifests/surefire-interpreter_plugin.yaml /app/plugins/surefire-interpreter/plugin.yaml

RUN rm -r /app/conf/java-manifests

EXPOSE 7774
EXPOSE 7775
EXPOSE 7776
EXPOSE 38368
EXPOSE 24368

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /etc/squashtf

CMD ["python", "/usr/local/lib/python3.8/site-packages/opentf/scripts/startup.py"]

ARG SCM_VERSION
LABEL SCM_VERSION=${SCM_VERSION}
LABEL JAVA_PLUGINS_VERSION=${JAVA_PLUGINS_VERSION}