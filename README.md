# OpenTestFactory Orchestrator

Automate, customize, and execute your testing workflows right in your
repository with the OpenTestFactory Orchestrator.  You can discover, create,
and share plugins to perform any job you'd like, and combine plugins in a
completely customized workflow.

## Getting started

### About OpenTestFactory Orchestrator Workflows

The OpenTestFactory Orchestrator Workflows enables you to create custom
software development life cycle (SDLC) workflows directly in your source code
repository.

### Configuring a Workflow

You can create custom workflows to automate your project's software development
life cycle processes.

### [About OpenTestFactory Orchestrator plugins](https://opentestfactory.org/plugins/about-plugins.html)

Generators and providers plugins are individual tasks that you can combine to
create jobs and customize your workflow.  You can create our own generators and
providers plugins or use and customize plugins shared by the OpenTestFactory
community.

## Popular articles

### [Installation](https://opentestfactory.org/installation.html)

### [Workflow Syntax](https://opentestfactory.org/specification/workflows.html)

A workflow is a configurable automated proces made up of one or more jobs.  You
must create a YAML file to define your workflow configuration.

### [Context and expressions syntax](https://opentestfactory.org/specification/context-and-expression-syntax.html)

You can access context information and evaluate expressions in workflows and
plugins.
