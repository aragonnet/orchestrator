# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Arranger unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy']
from opentf.core import arranger

mockresponse = Response()
mockresponse.status_code = 200

WORKFLOW = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {"SERVER": "production", "NAME": "FOO ${{ squashtf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "resources": {
        "testmanagers": [
            {
                "testmanager": "demo",
                "type": "squashtm",
                "name": "MyDemoProject",
                "endpoint": "MySquashTMConnection",
            }
        ],
        "repositories": [
            {
                "name": "common",
                "repository": "SquashTestOrg/CommonTools",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
            {
                "name": "testsuite",
                "repository": "SquashTestOrg/DemoTestSuite",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
        ],
    },
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python ${{ matrix.python-version }}",
            "needs": "prepare",
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}

WORKFLOW2 = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {
        "SERVER": "production",
        "NAME": "FOO ${{ squashtf.workflow }}",
        "user": "UsEr",
        "pwd": "pASSwORD",
    },
    "defaults": {"runs-on": "dummy"},
    "resources": {
        "repositories": [
            {
                "name": "common",
                "repository": "SquashTestOrg/CommonTools",
                "type": "bitbucket",
                "endpoint": "MySquashTestOrgConnection",
            },
            {
                "name": "testsuite",
                "repository": "SquashTestOrg/DemoTestSuite",
                "type": "bitbucket",
                "endpoint": "https://${{ variables.user }}:${{ variables.pwd }}@bitbucket.org/",
            },
        ],
    },
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
    },
}

WORKFLOW_GENERATOR = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {"SERVER": "production", "NAME": "FOO ${{ squashtf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "jobs": {
        "persistence-tests": {
            "name": "Persistence tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "foobaR",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}


ACTIONSCHECKOUT_BEFORE_HOOK = [
    {
        'name': 'my pre-checkout hook',
        'events': [{'categoryPrefix': 'actions', 'category': 'checkout'}],
        'before': [{'run': 'echo about to perform a checkout'}],
    }
]


class TestArranger(unittest.TestCase):
    def setUp(self):
        app = arranger.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    # Helpers tests

    def test_scl_1(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo/bar@v0')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryPrefix'], 'foo'
        )
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'bar')
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryVersion'], 'v0'
        )

    def test_scl_2(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryPrefix'], '-')
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'foo')
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryVersion'], '-')

    def test_scl_3(self):
        metadata = {}
        arranger._set_category_labels(metadata, 'foo/bar')
        self.assertTrue(metadata.get('labels'))
        self.assertEqual(
            metadata['labels']['opentestfactory.org/categoryPrefix'], 'foo'
        )
        self.assertEqual(metadata['labels']['opentestfactory.org/category'], 'bar')
        self.assertEqual(metadata['labels']['opentestfactory.org/categoryVersion'], '-')

    def test_eitems_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_items, '${{ var.baz }}', {}
        )

    def test_eitems_2(self):
        response = arranger.evaluate_items({'foo': 'hello world'}, {})
        self.assertEqual(response, {'foo': 'hello world'})

    def test_eif_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_if, 'variables.baz == true', {}
        )

    def test_eif_2(self):
        response = arranger.evaluate_if('variables.baz', {'variables': {'baz': True}})
        self.assertTrue(response)

    def test_estr_1(self):
        self.assertRaises(
            arranger.ExecutionError, arranger.evaluate_str, '${{ var.baz }}', {}
        )

    def test_estr_2(self):
        response = arranger.evaluate_str('hello world', {})
        self.assertEqual(response, 'hello world')

    # Workflow class tests

    def test_workflow(self):
        wf = arranger.Workflow(WORKFLOW)
        self.assertEqual(wf.variables['SERVER'], 'production')
        self.assertEqual(wf.variables['NAME'], "FOO TestSuite Example")
        self.assertTrue(wf.manifest['jobs']['prepare']['name'], 'prepare')
        self.assertTrue(
            wf.manifest['jobs']['persistence-tests']['name'], 'Persistence tests'
        )

    def test_workflow_contexts(self):
        wf = arranger.Workflow(WORKFLOW)
        ctxts = wf.get_contexts()
        self.assertTrue('squashtf' in ctxts)
        self.assertTrue('resources' in ctxts)

    def test_workflow_variables(self):
        wf = arranger.Workflow(WORKFLOW)
        # should return a copy
        variables = wf.get_variables()
        variables['NAME'] = 'foo'
        self.assertNotEqual(variables['NAME'], wf.variables['NAME'])

    def test_workflow_repositories(self):
        wf = arranger.Workflow(WORKFLOW2)
        ctxts = wf.get_contexts()
        self.assertTrue('resources' in ctxts)
        self.assertTrue('repositories' in ctxts['resources'])
        repositories = ctxts['resources']['repositories']
        self.assertTrue('testsuite' in repositories)
        self.assertTrue('/UsEr:' in repositories['testsuite']['endpoint'])
        self.assertTrue(':pASSwORD@' in repositories['testsuite']['endpoint'])

    def test_workflow_workflow_id(self):
        wf = arranger.Workflow(WORKFLOW)
        self.assertEqual(wf.get_workflow_id(), WORKFLOW['metadata']['workflow_id'])

    def test_workflow_needs_1(self):
        wf = arranger.Workflow(WORKFLOW)
        wf.set_job_result('prepare', 'success')
        needs = wf.get_needs(['prepare'])
        self.assertTrue('prepare' in needs)
        self.assertEqual(needs['prepare']['result'], 'success')
        self.assertRaises(KeyError, wf.get_needs, 'unknown')

    def test_workflow_needs_2(self):
        wf = arranger.Workflow(WORKFLOW)
        wf.set_job_result('prepare', 'success')
        wf.set_job_output('prepare', 'foo', 'bar')
        needs = wf.get_needs(['prepare'])
        self.assertTrue('prepare' in needs)
        self.assertEqual(needs['prepare']['result'], 'success')
        self.assertEqual(needs['prepare']['outputs']['foo'], 'bar')

    def test_workflow_metadata(self):
        src = WORKFLOW.copy()
        src['metadata'] = src['metadata'].copy()
        src['metadata']['actor'] = 'Who, me?'
        src['metadata']['token'] = 'secret'
        wf = arranger.Workflow(src)
        metadata = wf.get_metadata()
        self.assertFalse('actor' in metadata)
        self.assertTrue('name' in metadata)
        self.assertFalse('token' in metadata)

    def test_workflow_jobscount(self):
        wf = arranger.Workflow(WORKFLOW)
        org = wf._jobs_count
        wf.inc_jobs_count()
        self.assertEqual(wf._jobs_count, org + 1)

    def test_workflow_jobscount_of(self):
        wf = arranger.Workflow(WORKFLOW)
        wf._jobs_count = arranger.MAX_JOBS
        self.assertRaises(arranger.ExecutionError, wf.inc_jobs_count)

    def test_job_stepscount(self):
        wf = arranger.Workflow(WORKFLOW)
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        org = job._steps_count
        job.inc_steps_count()
        self.assertEqual(job._steps_count, org + 1)

    def test_job_stepscount_of(self):
        wf = arranger.Workflow(WORKFLOW)
        job = arranger.Job(wf.manifest['jobs']['prepare'], wf)
        job._steps_count = arranger.MAX_STEPS
        self.assertRaises(arranger.ExecutionError, job.inc_steps_count)

    def test_workflow_fail_first(self):
        wf = arranger.Workflow(WORKFLOW)
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.arranger.publish', mock):
            wf.fail()
        self.assertTrue(wf.done)
        self.assertEqual(wf.status, arranger.STATUS_FAILURE)
        self.assertEqual(mock.call_count, 2)

    def test_workflow_fail_raised(self):
        wf = arranger.Workflow(WORKFLOW)
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.arranger.publish', mock):
            wf.fail(raised=True)
        self.assertTrue(wf.done)
        self.assertEqual(wf.status, arranger.STATUS_FAILURE)
        self.assertEqual(mock.call_count, 1)

    def test_workflow_fail_fail(self):
        wf = arranger.Workflow(WORKFLOW)
        wf.status = arranger.STATUS_FAILURE
        wf.done = True
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.arranger.publish', mock):
            wf.fail(raised=True)
        self.assertTrue(wf.done)
        self.assertEqual(wf.status, arranger.STATUS_FAILURE)
        self.assertEqual(mock.call_count, 0)

    def test_workflow_fail_canceled(self):
        wf = arranger.Workflow(WORKFLOW)
        wf.status = arranger.STATUS_CANCELED
        wf.done = True
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.arranger.publish', mock):
            wf.fail(raised=True)
        self.assertTrue(wf.done)
        self.assertEqual(wf.status, arranger.STATUS_CANCELED)
        self.assertEqual(mock.call_count, 0)

    # Job class tests

    def test_job(self):
        wf = arranger.Workflow(WORKFLOW)
        job = arranger.Job(WORKFLOW['jobs']['prepare'], wf)
        self.assertEqual(job.sequence_id, arranger.CHANNEL_REQUEST)

    def test_job_outoforder(self):
        wf = arranger.Workflow(WORKFLOW)
        self.assertRaises(
            KeyError, arranger.Job, WORKFLOW['jobs']['persistence-tests'], wf
        )

    def test_job_process_stepsjob(self):
        wf = arranger.Workflow(WORKFLOW)
        job = arranger.Job(wf.jobs_backlog.pop(0), wf)
        job.process()
        self.assertTrue(wf.steps_backlog)

    def test_job_process_generatorjob(self):
        wf = arranger.Workflow(WORKFLOW_GENERATOR)
        wf.semaphore = MagicMock(return_value=True)
        job = arranger.Job(wf.jobs_backlog.pop(0), wf)
        mock_publish = MagicMock()
        with patch('opentf.core.arranger.publish', mock_publish):
            job.process()
        mock_publish.assert_called_once()

    def test_job_process_step(self):
        wf = arranger.Workflow(WORKFLOW)
        wf.semaphore = MagicMock(return_value=True)
        job = arranger.Job(wf.jobs_backlog.pop(0), wf)
        job.process()
        mock_publish = MagicMock()
        with patch('opentf.core.arranger.publish', mock_publish):
            arranger._process_step(wf.steps_backlog.pop(0), job)
        mock_publish.assert_called_once()

    # process_executionresult

    def test_process_executionresult_invalid(self):
        self.assertRaises(arranger.BadRequest, arranger.process_executionresult, {})

    # _add_providersteps

    def test_add_providersteps_windows_empty(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        job.workflow.steps_backlog = []
        arranger._add_providersteps(
            job,
            {},
            {'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'}, 'steps': []},
        )
        self.assertEqual(len(job.workflow.steps_backlog), 0)

    def test_add_providersteps_windows_notempty(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        job.workflow.steps_backlog = []
        arranger._add_providersteps(
            job,
            {},
            {'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'}, 'steps': [{}]},
        )
        self.assertEqual(len(job.workflow.steps_backlog), 1)
        self.assertEqual(
            job.workflow.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD']
        )

    def test_add_providersteps_windows_wd_child(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        job.workflow.steps_backlog = []
        arranger._add_providersteps(
            job,
            {},
            {
                'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'},
                'steps': [{'working-directory': 'aBc'}],
            },
        )
        self.assertEqual(len(job.workflow.steps_backlog), 1)
        self.assertEqual(
            job.workflow.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD']
        )
        self.assertEqual(job.workflow.steps_backlog[0]['working-directory'], 'aBc')

    def test_add_providersteps_windows_wd_parent(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        job.workflow.steps_backlog = []
        arranger._add_providersteps(
            job,
            {'working-directory': 'fOo'},
            {'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'}, 'steps': [{}]},
        )
        self.assertEqual(len(job.workflow.steps_backlog), 1)
        self.assertEqual(
            job.workflow.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD']
        )
        self.assertEqual(job.workflow.steps_backlog[0]['working-directory'], 'fOo')

    def test_add_providersteps_windows_wd_parentchild(self):
        job = MagicMock()
        job.contexts = {'runner': {'os': 'windows'}}
        job.workflow.steps_backlog = []
        arranger._add_providersteps(
            job,
            {'working-directory': 'fOo'},
            {
                'metadata': {'step_origin': [], 'step_id': 'sTeP_iD'},
                'steps': [{'working-directory': 'aBc'}],
            },
        )
        self.assertEqual(len(job.workflow.steps_backlog), 1)
        self.assertEqual(
            job.workflow.steps_backlog[0]['metadata']['step_origin'], ['sTeP_iD']
        )
        self.assertEqual(job.workflow.steps_backlog[0]['working-directory'], 'fOo\\aBc')

    # _apply_provider_hooks

    def test_apply_provider_hooks_nohook(self):
        mock = MagicMock()
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks([], {}, [], [], [], {})
        mock.assert_called_once()

    def test_apply_provider_hooks_nomatch(self):
        mock = MagicMock()
        steps = []
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks(
                steps, {}, ACTIONSCHECKOUT_BEFORE_HOOK, [], [], {}
            )
        self.assertFalse(steps)

    def test_apply_provider_hooks_nofull(self):
        mock = MagicMock()
        steps = []
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout2',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                [],
                [],
                {},
            )
        self.assertFalse(steps)

    def test_apply_provider_hooks_prematch(self):
        mock = MagicMock()
        steps = [{'run': 'sTeP'}]
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                [],
                [],
                {},
            )
        self.assertEqual(len(steps), 2)
        self.assertEqual(steps[-1], {'run': 'sTeP'})

    def test_apply_provider_hooks_prematch2(self):
        mock = MagicMock()
        steps = [{'run': 'sTeP'}]
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                ACTIONSCHECKOUT_BEFORE_HOOK,
                [],
                {},
            )
        self.assertEqual(len(steps), 3)
        self.assertEqual(steps[-1], {'run': 'sTeP'})

    def test_apply_provider_hooks_prematch3(self):
        mock = MagicMock()
        steps = [{'run': 'sTeP'}]
        with patch('opentf.core.arranger.debug', mock):
            arranger._apply_provider_hooks(
                steps,
                {
                    'opentestfactory.org/categoryPrefix': 'actions',
                    'opentestfactory.org/category': 'checkout',
                },
                ACTIONSCHECKOUT_BEFORE_HOOK,
                ACTIONSCHECKOUT_BEFORE_HOOK,
                ACTIONSCHECKOUT_BEFORE_HOOK,
                {},
            )
        self.assertEqual(len(steps), 4)
        self.assertEqual(steps[-1], {'run': 'sTeP'})
