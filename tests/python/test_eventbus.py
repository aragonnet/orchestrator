# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""EventBus unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy']
from opentf.core import eventbus

mockresponse = Response()
mockresponse.status_code = 200

########################################################################

RESOLVPATH_DATA = {'foo': {'bar': {'baz': 'bAZ'}}}
LABELS_DATA = {'foo.bar.baz': 'bAZ', 'foo': 'fOO'}
METADATALABELS_DATA = {'metadata': {'labels': LABELS_DATA}}
FIELDS_DATA = {'generator': 'opentestfactory.org/template.generator@v1'}

SUBSCRIPTION_NAME = 'cafebabe'
SUBSCRIPTIONS_DATA = {
    SUBSCRIPTION_NAME: {
        'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
        'status': {'publicationCount': 0},
    }
}


class TestEventBus(unittest.TestCase):
    def setUp(self):
        app = eventbus.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_subscription_noselector(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.json['details'].get('uuid'))
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre + 1)

    def test_subscription(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchFields": {"def.i-1.y": "12"}},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 201)
        workflow_uuid = response.json['details'].get('uuid')
        self.assertIsNotNone(workflow_uuid)
        annotations = eventbus.SUBSCRIPTIONS[workflow_uuid]['metadata']['annotations']
        self.assertFalse(annotations[eventbus.LABEL_SELECTOR])
        self.assertEqual(annotations[eventbus.FIELD_SELECTOR], 'def.i-1.y==12')
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre + 1)
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.eventbus.make_status_response', mock):
            eventbus.cancel_subscription(workflow_uuid)
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre)
        self.assertEqual(mock.call_args.args[0], 'OK')
        with patch('opentf.core.eventbus.make_status_response', mock):
            eventbus.cancel_subscription(workflow_uuid)
        self.assertEqual(mock.call_args.args[0], 'NotFound')
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre)

    def test_subscription_kind(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchKind": "KIND"},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 201)
        workflow_uuid = response.json['details'].get('uuid')
        self.assertIsNotNone(workflow_uuid)
        annotations = eventbus.SUBSCRIPTIONS[workflow_uuid]['metadata']['annotations']
        self.assertFalse(annotations[eventbus.LABEL_SELECTOR])
        self.assertEqual(annotations[eventbus.FIELD_SELECTOR], 'kind==KIND')
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre + 1)
        self.assertTrue(eventbus.is_interested(workflow_uuid, {'kind': 'KIND'}))
        self.assertFalse(eventbus.is_interested(workflow_uuid, {'kind': 'kIND'}))

    def test_subscription_labels(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchLabels": {"a.b.c": "12"}},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 201)
        workflow_uuid = response.json['details'].get('uuid')
        self.assertIsNotNone(workflow_uuid)
        annotations = eventbus.SUBSCRIPTIONS[workflow_uuid]['metadata']['annotations']
        self.assertTrue(annotations[eventbus.LABEL_SELECTOR])
        self.assertFalse(annotations[eventbus.FIELD_SELECTOR])
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre + 1)

    def test_subscription_badselector(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchLabels": {"def.i-1.": "12"}},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre)

    def test_subscription_emptyselector(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre)

    def test_publication(self):
        response = self.app.post(
            '/publications',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Publication",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchFields": {"def.i-1.y": "12"}},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertTrue('no matching subscription' in response.json['message'])

    def test_split_exprs_1(self):
        what = eventbus._split_exprs('foo=bar')
        self.assertEqual(len(what), 1)

    def test_split_exprs_2(self):
        what = eventbus._split_exprs('foo=bar,bar=foobar')
        self.assertEqual(len(what), 2)
        self.assertEqual(what[0], 'foo=bar')

    def test_split_exprs_3(self):
        what = eventbus._split_exprs('foo in (bar)')
        self.assertEqual(len(what), 1)

    def test_split_exprs_4(self):
        what = eventbus._split_exprs('foo notin (bar, baz)')
        self.assertEqual(len(what), 1)

    def test_split_exprs_5(self):
        what = eventbus._split_exprs('foo')
        self.assertEqual(len(what), 1)
        self.assertEqual(what[0], 'foo')

    def test_split_exprs_6(self):
        what = eventbus._split_exprs('foo,!bar,foo in (bar), foo notin (baz, baz)')
        self.assertEqual(len(what), 4)

    def test_resolve_path_1(self):
        status, value = eventbus._resolve_path('foo.bar.baz', RESOLVPATH_DATA)
        self.assertTrue(status)
        self.assertEqual(value, 'bAZ')

    def test_resolve_path_2(self):
        status, value = eventbus._resolve_path('foo.bar.foobar', RESOLVPATH_DATA)
        self.assertFalse(status)
        self.assertIsNone(value)

    def test_evaluate_fields_1(self):
        self.assertTrue(eventbus._evaluate_fields('', RESOLVPATH_DATA))

    def test_evaluate_fields_2(self):
        self.assertTrue(eventbus._evaluate_fields('foo.bar.baz==bAZ', RESOLVPATH_DATA))

    def test_evaluate_fields_3(self):
        self.assertFalse(eventbus._evaluate_fields('foo.bar.baz!=bAZ', RESOLVPATH_DATA))

    def test_evaluate_fields_4(self):
        self.assertTrue(eventbus._evaluate_fields('foo.bar.baz', RESOLVPATH_DATA))

    def test_evaluate_fields_5(self):
        self.assertFalse(eventbus._evaluate_fields('!foo.bar.baz', RESOLVPATH_DATA))

    def test_evaluate_fields_6(self):
        self.assertFalse(eventbus._evaluate_fields('foo.bar.foobar', RESOLVPATH_DATA))

    def test_evaluate_fields_7(self):
        self.assertTrue(eventbus._evaluate_fields('!foo.bar.foobar', RESOLVPATH_DATA))

    def test_evaluate_fields_8(self):
        self.assertTrue(
            eventbus._evaluate_fields('foo.bar.baz in (bar, bAZ)', RESOLVPATH_DATA)
        )

    def test_evaluate_fields_9(self):
        self.assertFalse(
            eventbus._evaluate_fields('foo.bar.baz in (bar, baz)', RESOLVPATH_DATA)
        )

    def test_evaluate_fields_10(self):
        self.assertTrue(
            eventbus._evaluate_fields('foo.bar.baz notin (bar, baz)', RESOLVPATH_DATA)
        )

    def test_evaluate_fields_11(self):
        self.assertFalse(
            eventbus._evaluate_fields('foo.bar.foobar in (bar, bAZ)', RESOLVPATH_DATA)
        )

    def test_evaluate_fields_12(self):
        self.assertTrue(
            eventbus._evaluate_fields(
                'foo.bar.foobar notin (bar, baz)', RESOLVPATH_DATA
            )
        )

    def test_evaluate_fields_13(self):
        self.assertRaises(
            ValueError, eventbus._evaluate_fields, 'foo.bar.baz * 2', RESOLVPATH_DATA
        )

    def test_evaluate_1(self):
        self.assertTrue(eventbus._evaluate('', LABELS_DATA))

    def test_evaluate_2(self):
        self.assertTrue(eventbus._evaluate('foo.bar.baz==bAZ', LABELS_DATA))

    def test_evaluate_3(self):
        self.assertFalse(eventbus._evaluate('foo.bar.baz!=bAZ', LABELS_DATA))

    def test_evaluate_4(self):
        self.assertTrue(eventbus._evaluate('foo.bar.baz', LABELS_DATA))

    def test_evaluate_5(self):
        self.assertFalse(eventbus._evaluate('!foo.bar.baz', LABELS_DATA))

    def test_evaluate_6(self):
        self.assertFalse(eventbus._evaluate('foo.bar.foobar', LABELS_DATA))

    def test_evaluate_7(self):
        self.assertTrue(eventbus._evaluate('!foo.bar.foobar', LABELS_DATA))

    def test_evaluate_8(self):
        self.assertTrue(eventbus._evaluate('foo.bar.baz in (bar, bAZ)', LABELS_DATA))

    def test_evaluate_9(self):
        self.assertFalse(eventbus._evaluate('foo.bar.baz in (bar, baz)', LABELS_DATA))

    def test_evaluate_10(self):
        self.assertTrue(eventbus._evaluate('foo.bar.baz notin (bar, baz)', LABELS_DATA))

    def test_evaluate_11(self):
        self.assertFalse(
            eventbus._evaluate('foo.bar.foobar in (bar, bAZ)', LABELS_DATA)
        )

    def test_evaluate_12(self):
        self.assertTrue(
            eventbus._evaluate('foo.bar.foobar notin (bar, baz)', LABELS_DATA)
        )

    def test_evaluate_13(self):
        self.assertTrue(
            eventbus._evaluate(
                'generator==opentestfactory.org/template.generator@v1', FIELDS_DATA
            )
        )

    def test_matchfieldselector_1(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, ''))

    def test_matchfieldselector_2(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo'))

    def test_matchfieldselector_2_(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo.bar'))

    def test_matchfieldselector_3(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo,foo.bar'))

    def test_matchfieldselector_4(self):
        self.assertFalse(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo,foobar'))

    def test_matchfieldselector_5(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo.bar.baz'))

    def test_matchfieldselector_6(self):
        self.assertTrue(eventbus._match_field_selector(RESOLVPATH_DATA, 'foo,!bar'))

    def test_matchfieldselector_7(self):
        self.assertTrue(
            eventbus._match_field_selector(RESOLVPATH_DATA, 'foo,foo.bar.baz==bAZ')
        )

    def test_matchlabelselector_1(self):
        self.assertTrue(eventbus._match_label_selector(METADATALABELS_DATA, ''))

    def test_matchlabelselector_2(self):
        self.assertTrue(eventbus._match_label_selector(METADATALABELS_DATA, 'foo'))

    def test_matchlabelselector_2_(self):
        self.assertFalse(eventbus._match_label_selector(METADATALABELS_DATA, 'foo.bar'))

    def test_matchlabelselector_3(self):
        self.assertTrue(
            eventbus._match_label_selector(METADATALABELS_DATA, 'foo,foo.bar.baz')
        )

    def test_matchlabelselector_4(self):
        self.assertFalse(
            eventbus._match_label_selector(METADATALABELS_DATA, 'foo,foobar')
        )

    def test_matchlabelselector_5(self):
        self.assertTrue(
            eventbus._match_label_selector(
                METADATALABELS_DATA, 'foo in (foo, fOO),!bar'
            )
        )

    def test_matchlabelselector_6(self):
        self.assertTrue(eventbus._match_label_selector(METADATALABELS_DATA, 'foo,!bar'))

    def test_matchlabelselector_7(self):
        self.assertTrue(
            eventbus._match_label_selector(METADATALABELS_DATA, 'foo,foo.bar.baz==bAZ')
        )

    def test_dispatch_publication_1(self):
        eventbus.dispatch_publication([], {})

    def test_dispatch_publication_2(self):
        mock_post = MagicMock()
        with patch('opentf.core.eventbus.SUBSCRIPTIONS', SUBSCRIPTIONS_DATA), patch(
            'requests.post', mock_post
        ):
            eventbus.dispatch_publication([SUBSCRIPTION_NAME], {})


if __name__ == '__main__':
    unittest.main()
