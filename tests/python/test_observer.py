# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import sys
import unittest

from datetime import datetime
from unittest.mock import MagicMock, patch

from requests import Response

import yaml


mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy']
from opentf.core import observer


class TestObserver(unittest.TestCase):
    def setUp(self):
        app = observer.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_is_matches(self):
        self.assertTrue(observer._is({'apiVersion': 'v', 'kind': 'k'}, 'v/k'))

    def test_is_casesensitive(self):
        self.assertFalse(observer._is({'apiVersion': 'v', 'kind': 'K'}, 'v/k'))

    def test_clean_expired_workflows(self):
        observer.clean_expired_workflows()
        self.assertFalse(observer.WORKFLOWS_LASTSEEN)

    def test_clean_expired_workflows_notexpired(self):
        lastseen = {'notexpired': datetime.now()}
        with patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen):
            observer.clean_expired_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 1)
            self.assertTrue('notexpired' in observer.WORKFLOWS_LASTSEEN)

    def test_clean_expired_workflows_expired(self):
        lastseen = {'expired': datetime(year=2000, month=12, day=3)}
        with patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen):
            observer.clean_expired_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 0)
            self.assertFalse('expired' in observer.WORKFLOWS_LASTSEEN)

    def test_clean_expired_workflows_mixed(self):
        lastseen = {
            'expired': datetime(year=2000, month=12, day=3),
            'notexpired': datetime.now(),
        }
        with patch('opentf.core.observer.WORKFLOWS_LASTSEEN', lastseen):
            observer.clean_expired_workflows()
            self.assertEqual(len(observer.WORKFLOWS_LASTSEEN), 1)
            self.assertFalse('expired' in observer.WORKFLOWS_LASTSEEN)
            self.assertTrue('notexpired' in observer.WORKFLOWS_LASTSEEN)

    def test_get_workflows(self):
        mock = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock):
            observer.get_workflows()
        mock.assert_called_once()
        self.assertTrue('details' in mock.call_args.kwargs)
        self.assertFalse(mock.call_args.kwargs['details']['items'])

    def test_get_workflow_status(self):
        mock = MagicMock()
        with patch('opentf.core.observer.make_status_response', mock):
            observer.get_workflow_status('abc')
        mock.assert_called_once()


if __name__ == '__main__':
    unittest.main()
