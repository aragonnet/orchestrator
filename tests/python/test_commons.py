# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

import yaml


mockresponse = Response()
mockresponse.status_code = 200

import opentf.commons


########################################################################

WORKFLOW_OK = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {"SERVER": "production", "NAME": "FOO ${{ squashtf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python ${{ matrix.python-version }}",
            "needs": "prepare",
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}

WORKFLOW_NOK_UNKNOWNJOB = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {"SERVER": "production", "NAME": "FOO ${{ squashtf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python ${{ matrix.python-version }}",
            "needs": ["prepare", "unknown"],
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}

WORKFLOW_NOK_CIRCULARJOB = {
    "apiVersion": "opentestfactory.org/v1alpha1",
    "kind": "Workflow",
    "metadata": {"name": "TestSuite Example", "workflow_id": "WoRkFlOw_Id"},
    "variables": {"SERVER": "production", "NAME": "FOO ${{ squashtf.workflow }}"},
    "defaults": {"runs-on": "dummy"},
    "jobs": {
        "prepare": {
            "runs-on": "ssh",
            "steps": [
                {
                    "uses": "actions/checkout@v2",
                    "with": {"repository": "${{ common }}"},
                },
                {"run": "ls /"},
                {
                    "run": "echo $PATH $YOLO",
                    "variables": {"SERVER": "yoloserver", "YOLO": "yada"},
                },
            ],
        },
        "ui-tests": {
            "name": "UI tests - Python ${{ matrix.python-version }}",
            "needs": ["prepare", "persistence-tests"],
            "strategy": {
                "matrix": {"python-version": ["3.6", "3.7", "3.8"]},
                "max-parallel": 2,
            },
            "steps": [
                {
                    "uses": "actions/setup-python@vv2",
                    "with": {"python-version": "${{ matrix.python-version }}"},
                },
                {
                    "uses": "robotframework/robot@v1",
                    "with": {
                        "datasource": "${{ testsuite }}/path/to/robot.txt",
                        "include": "fooANDbar",
                    },
                },
            ],
        },
        "persistence-tests": {
            "name": "Persistence tests",
            "needs": "ui-tests",
            "strategy": {"parallel": 4},
            "timeout-minutes": 1,
            "generator": "${{ demo }}",
            "with": {"test_plan": "foo", "test_suite": "bar", "test_iteration": "baz"},
        },
    },
}


########################################################################


class TestObserver(unittest.TestCase):

    # validate_pipeline

    def test_validate_pipeline_1(self):
        result = opentf.commons.validate_pipeline(WORKFLOW_OK)
        self.assertTrue(result[0])

    def test_validate_pipeline_2(self):
        result = opentf.commons.validate_pipeline(WORKFLOW_NOK_UNKNOWNJOB)
        self.assertFalse(result[0])

    def test_validate_pipeline_3(self):
        result = opentf.commons.validate_pipeline(WORKFLOW_NOK_CIRCULARJOB)
        self.assertFalse(result[0])

    # get_execution_sequence

    def test_get_execution_sequence_1(self):
        result = opentf.commons.get_execution_sequence(WORKFLOW_OK)
        self.assertEqual(result, ['prepare', 'ui-tests', 'persistence-tests'])

    # def test_get_execution_sequence_2(self):
    #     result = opentf.commons.get_execution_sequence(WORKFLOW_NOK_UNKNOWNJOB)
    #     self.assertIsNone(result)

    def test_get_execution_sequence_3(self):
        result = opentf.commons.get_execution_sequence(WORKFLOW_NOK_CIRCULARJOB)
        self.assertIsNone(result)


if __name__ == '__main__':
    unittest.main()
