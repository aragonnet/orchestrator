# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

import yaml


mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy']
from opentf.core import localcleaner


class TestObserver(unittest.TestCase):
    def setUp(self):
        app = localcleaner.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)


if __name__ == '__main__':
    unittest.main()
