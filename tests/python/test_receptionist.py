# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Receptionist unit tests."""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

import yaml


mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy']
from opentf.core import receptionist


class TestReceptionist(unittest.TestCase):
    def setUp(self):
        app = receptionist.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_simpleworkflow(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {"tests": {"generator": "xxx"}},
                },
            )
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.json['details'].get('workflow_id'))
        mock.assert_called_once()

    def test_workflow_yaml(self):
        with open('tests/python/resources/order_1.yaml') as f:
            workflow = yaml.safe_load(f)

        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data=yaml.safe_dump(workflow),
                headers={'Content-type': 'application/x-yaml'},
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()

    def test_workflow_json(self):
        with open('tests/python/resources/order_1.yaml') as f:
            workflow = yaml.safe_load(f)

        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json=workflow,
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()

    def test_workflow_nojobs(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_circular(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Workflow",
                    "metadata": {"name": "foo"},
                    "jobs": {
                        "test1": {"needs": ["test2"], "generator": "xxx"},
                        "test2": {"needs": ["test1"], "generator": "xxx"},
                    },
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_with_nofiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={'variables': 'A=B'},
                # files={'workflow': open('tests/python/resources/order_1.yaml', 'rb')}
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_from_file(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/order_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        self.assertTrue('variables' in mock.call_args.args[0])
        self.assertTrue('A' in mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['A'], 'B')

    def test_workflow_varsfrom_file(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': open('tests/python/resources/vars', 'rb'),
                    'workflow': open('tests/python/resources/order_1.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()
        self.assertTrue('variables' in mock.call_args.args[0])
        self.assertTrue('A' in mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['A'], 'B')
        self.assertTrue('B' in mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['B'], 'DDDD')
        self.assertTrue('C' in mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['C'], 'hi there!')
        self.assertFalse('' in mock.call_args.args[0]['variables'])
        self.assertEqual(mock.call_args.args[0]['variables']['SERVER'], 'nonono')

    def test_workflow_with_missingfiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/filedemo.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 422)
        mock.assert_not_called()

    def test_workflow_with_nomissingfiles(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.core.receptionist.publish', mock):
            response = self.app.post(
                '/workflows',
                data={
                    'variables': 'A=B',
                    'workflow': open('tests/python/resources/filedemo.yaml', 'rb'),
                    'report': open('tests/python/resources/filedemo.yaml', 'rb'),
                },
            )
        self.assertEqual(response.status_code, 201)
        mock.assert_called_once()


if __name__ == '__main__':
    unittest.main()
