# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""ActionProvider unit tests.

- actions selection tests
- create-file format tests
- formatters tests
"""

import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy']

from opentf.plugins.actionprovider import checkout, main, files

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    "contexts": {'runner': {'os': 'linux'}},
    "step": {'uses': 'foo'},
    "runs-on": "linux",
}

PREPAREINCEPTION_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'prepare-inception',
}
CREATEFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'create-file',
}
DELETEFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'delete-file',
}
TOUCHFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'touch-file',
}
CHECKOUT_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'checkout',
}
GETFILES_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'get-files',
}
GETFILE_LABELS = {
    'opentestfactory.org/categoryPrefix': 'action',
    'opentestfactory.org/category': 'get-file',
}

REPOSITORY_0 = REPOSITORY_1_PUBLIC = 'http://github.com/a/b.git'
REPOSITORY_1 = 'http://foo:@github.com/a/b.git'
REPOSITORY_2 = 'https://foo:@github.com/a/b.git'
REPOSITORY_3 = 'https://:bar@github.com/a/b.git'
REPOSITORY_4 = 'https://foo:bar@github.com/a/b.git'
REPOSITORY_5 = 'http://foo:@github.com:8080/a/b.git'

REPOSITORY_01_PRIVATE = 'http://aBC:dEF@github.com/a/b.git'

CONTEXTS_PUBLIC = {
    'resources': {
        'repositories': {
            'abc': {'repository': 'a/b.git', 'endpoint': 'http://github.com/'}
        }
    }
}
CONTEXTS_PRIVATE = {
    'resources': {
        'repositories': {
            'abc': {'repository': 'a/b.git', 'endpoint': 'http://aBC:dEF@github.com/'}
        }
    }
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestActionProvider(unittest.TestCase):
    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    # actions

    def test_inception_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = PREPAREINCEPTION_LABELS
            command['step'] = {'with': {'foo.xml': {'url': 'yada'}}, 'uses': 'foo'}
            _dispatch(files.prepareinception_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('::inception::WORKFLOW_ID::foo.xml::yada', steps[0]['run'])

    def test_touchfile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.touchfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('touch foo', steps[0]['run'])

    def test_touchfile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.touchfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('@type nul >>foo', steps[0]['run'])

    def test_deletefile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DELETEFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.deletefile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('rm -f foo', steps[0]['run'])

    def test_deletefile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = DELETEFILE_LABELS
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.deletefile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('@if exist foo @del /f/q foo', steps[0]['run'])

    def test_getfile_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            _dispatch(files.getfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('echo "::attach::`pwd`/foo"', steps[0]['run'])

    def test_getfile_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'path': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual('@echo ::attach::%CD%\\foo', steps[0]['run'])

    def test_getfiles_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'pattern': 'foo'}, 'uses': 'foo'}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            'for f in foo ; do echo "::attach::$(pwd)/$f" ; done;', steps[0]['run']
        )

    def test_getfiles_action_warn_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue(
            'for f in foo ; do echo "::attach::$(pwd)/$f" ; done' in steps[0]['run']
        )
        self.assertTrue(
            '''if test -z "$(find . -maxdepth 1 -name 'foo' -print -quit)"; then'''
            in steps[0]['run']
        )

    def test_getfiles_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': 'foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            '(@for %%i in (foo) do @echo ::attach::%CD%\\%%i)', steps[0]['run']
        )

    def test_getfiles_action_windows_type_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': 'foo', 'type': 'bar'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue(' type=bar' in steps[0]['run'])

    def test_getfiles_action_warn_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': 'foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            '(@for %%i in (foo) do @echo ::attach::%CD%\\%%i) & if not exist foo echo ::warning::bar',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {'with': {'pattern': '**/foo'}, 'uses': 'foo'}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            'for f in $(find -name \\foo); do echo "::attach::$(pwd)/$f" ; done;',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_warn_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': '**/foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue(
            'if test -z "$(find . -name \'foo\' -print -quit)"' in steps[0]['run']
        )
        self.assertTrue(
            '''else for f in $(find -name \\foo); do echo "::attach::$(pwd)/$f" ; done'''
            in steps[0]['run']
        )

    def test_getfilesrecurse_action_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {'with': {'pattern': '**\\foo'}, 'uses': 'foo'}
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            '@for /f %%i in (\'dir /s /b "foo"\') do @echo ::attach::%%i',
            steps[0]['run'],
        )

    def test_getfilesrecurse_action_warn_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILES_LABELS
            command['step'] = {
                'with': {'pattern': '**\\foo', 'warn-if-not-found': 'bar'},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.getfiles_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertEqual(
            '(@for /f %%i in (\'dir /s /b "foo"\') do @echo ::attach::%%i) || echo ::warning::bar & exit 0',
            steps[0]['run'],
        )

    def test_createfile_action_ini_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CREATEFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'ini',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue('[foo]\nbar=baz\n' in steps[0]['run'])

    def test_createfile_action_ini_windows_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'ini',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue('W2Zvb10KYmFyPWJhego=' in steps[0]['run'])

    def test_createfile_action_json_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'json',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue('{"foo": {"bar": "baz"}}' in steps[0]['run'])

    def test_createfile_action_yaml_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = GETFILE_LABELS
            command['step'] = {
                'with': {
                    'data': {'foo': {'bar': 'baz'}},
                    'format': 'yaml',
                    'path': 'foo.ini',
                },
                'uses': 'foo',
            }
            _dispatch(files.createfile_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 1)
        self.assertTrue('foo:\n  bar: baz\n' in steps[0]['run'])

    # # checkout

    def test_checkout_repository(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {'with': {'repository': 'rEPOSITORY'}, 'uses': 'foo'}
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        self.assertTrue('git clone rEPOSITORY' in steps[0]['run'])

    def test_checkout_repositoryref(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': 'rEPOSITORY', 'ref': 'fOOBAR'},
                'uses': 'foo',
            }
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertTrue('fOOBAR' in run)
        self.assertFalse('cd rEPOSITORY' in run)
        self.assertTrue('git clone -b fOOBAR rEPOSITORY' in run)

    def test_checkout_repositorysha_1(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'rEPOSITORY'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertTrue('cd rEPOSITORY' in run)
        self.assertTrue(f'git checkout {sha}' in run)
        self.assertTrue(f'git clone -n {repository}' in run)

    def test_checkout_repositorysha_1bis(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertTrue('cd rEPOSITORY' in run)
        self.assertTrue(f'git checkout {sha}' in run)
        self.assertTrue(f'git clone -n {repository}' in run)

    def test_checkout_repositorysha_2(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY.git'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertTrue('cd rEPOSITORY' in run)
        self.assertTrue(f'git checkout {sha}' in run)
        self.assertTrue(f'git clone -n {repository}' in run)

    def test_checkout_repositorysha_3(self):
        sha = 'AAAABBBBCCCCDDDDEEEEFFFF0000111122223333'
        repository = 'https://example.com/foo/rEPOSITORY.git/'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = CHECKOUT_LABELS
            command['step'] = {
                'with': {'repository': repository, 'ref': sha},
                'uses': 'foo',
            }
            _dispatch(checkout.checkout_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 1)
        run = steps[0]['run']
        self.assertTrue('cd rEPOSITORY' in run)
        self.assertTrue(f'git checkout {sha}' in run)
        self.assertTrue(f'git clone -n {repository}' in run)

    def test_maybe_override_0(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_0, None), REPOSITORY_0)

    def test_maybe_override_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_1, None), REPOSITORY_1)

    def test_maybe_override_2(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_2, None), REPOSITORY_2)

    def test_maybe_override_3(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_3, None), REPOSITORY_3)

    def test_maybe_override_4(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_4, None), REPOSITORY_4)

    def test_maybe_override_5(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_5, None), REPOSITORY_5)

    def test_maybe_override_0_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_0, {}), REPOSITORY_0)

    def test_maybe_override_1_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_1, {}), REPOSITORY_1)

    def test_maybe_override_2_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_2, {}), REPOSITORY_2)

    def test_maybe_override_3_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_3, {}), REPOSITORY_3)

    def test_maybe_override_4_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_4, {}), REPOSITORY_4)

    def test_maybe_override_5_1(self):
        self.assertEqual(checkout.maybe_override(REPOSITORY_5, {}), REPOSITORY_5)

    def test_maybe_override_0_2(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, {'resources': {}}), REPOSITORY_0
        )

    def test_maybe_override_0_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, CONTEXTS_PUBLIC), REPOSITORY_0
        )

    def test_maybe_override_1_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_1, CONTEXTS_PUBLIC), REPOSITORY_1_PUBLIC
        )

    def test_maybe_override_0_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_0, CONTEXTS_PRIVATE),
            REPOSITORY_01_PRIVATE,
        )

    def test_maybe_override_1_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_1, CONTEXTS_PRIVATE),
            REPOSITORY_01_PRIVATE,
        )

    def test_maybe_override_2_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_2, CONTEXTS_PUBLIC), REPOSITORY_2
        )

    def test_maybe_override_3_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_3, CONTEXTS_PUBLIC), REPOSITORY_3
        )

    def test_maybe_override_4_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_4, CONTEXTS_PUBLIC), REPOSITORY_4
        )

    def test_maybe_override_5_3(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_5, CONTEXTS_PUBLIC), REPOSITORY_5
        )

    def test_maybe_override_2_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_2, CONTEXTS_PRIVATE), REPOSITORY_2
        )

    def test_maybe_override_3_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_3, CONTEXTS_PRIVATE), REPOSITORY_3
        )

    def test_maybe_override_4_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_4, CONTEXTS_PRIVATE), REPOSITORY_4
        )

    def test_maybe_override_5_3_private(self):
        self.assertEqual(
            checkout.maybe_override(REPOSITORY_5, CONTEXTS_PRIVATE), REPOSITORY_5
        )

    # low-level formatters

    def test_createfile_ini_content(self):
        data = {
            "global": {"firstGlobal": "strange_value"},
            "test": {"key1": "value1", "key2": "value2"},
        }

        content = files.produce_ini_content(data)
        expected = (
            '[global]\nfirstGlobal=strange_value\n[test]\nkey1=value1\nkey2=value2\n'
        )
        self.assertEqual(expected, content)

    # create-archive

    def test_createarchive_action_windows_file_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) > 1)
        self.assertEqual('@if exist foo.tar @del /f/q foo.tar', steps[0]['run'])

    def test_createarchive_action_windows_glob_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['**/foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) > 4)
        self.assertTrue("""'dir /b /s "foo"'""" in steps[3]['run'])

    def test_createarchive_action_windows_folder_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['foo/']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        print(steps)
        self.assertTrue(len(steps) > 4)
        self.assertTrue("""'dir /b /s "foo/"'""" in steps[3]['run'])

    def test_createarchive_action_linux_glob_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata']['labels'] = TOUCHFILE_LABELS
            command['step'] = {
                'with': {'path': 'foo.tar', 'patterns': ['**/foo']},
                'uses': 'foo',
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            _dispatch(files.createarchive_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) > 4)
        self.assertTrue("""find .  -name "foo" -print""" in steps[3]['run'])


if __name__ == '__main__':
    unittest.main()
