# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""ActionProvider unit tests.

- actions selection tests
- create-file format tests
- formatters tests
"""

import unittest

from unittest.mock import MagicMock, patch

from requests import Response

import yaml

from squashtf.plugins.sshee import main

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

EXECUTIONCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    "step": {},
    "job": {
        "name": "JOBNAME",
        "runs-on": "linux",
    },
}

POOLS = yaml.safe_load(
    '''pools:
  aws:
  - host: ec2-3-250-212-135.eu-west-1.compute.amazonaws.com
    port: 22
    username: ec2-user
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: reject
    tags: [ssh, linux]
  ssh-dummy:
  - host: dummy.example.com
    username: foo
    password: secret
    tags: [ssh, linux]
  ssh1:
  - host: ec2-3-250-212-135.eu-west-1.compute.amazonaws.com
    port: 22
    username: ec2-user
    password: foo
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: reject
    tags: [ssh]
  - hosts: [foo.example.com, bar.example.com]
    port: 22
    username: ec2-user
    password: foo
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: auto-add
    tags: [example]
  agents:
  - host: robotframework.agents
    port: 2222
    username: linuxserver.io
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, roboframework]
  - host: cypress-agent.agents
    port: 22
    username: squashtf
    script_path: /home/squashtf
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, cypress]
  - host: junit.agents
    port: 22
    username: squashtf
    script_path: /home/squashtf
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, junit]
'''
)

########################################################################


class TestSSHEE(unittest.TestCase):
    def setUp(self):
        app = main.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT']['targets'] = ['aws', 'sshdummy']
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def test_listjobsteps(self):
        JOB_ID = 'foo'
        what = main.list_job_steps(JOB_ID)
        self.assertEqual(len(main.JOBS), 1)
        self.assertTrue(JOB_ID in main.JOBS)


if __name__ == '__main__':
    unittest.main()
