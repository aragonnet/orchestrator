# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cucumber Provider unit tests.

- execute tests
- cucumber tests
"""

import unittest

from unittest.mock import MagicMock, patch

from requests import Response

from opentf.plugins.cucumber import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    "contexts": {'runner': {'os': 'linux'}},
    "step": {'uses': 'foo'},
    "runs-on": "linux",
}


########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestCucumber(unittest.TestCase):
    """
    The class containing the Cucumber provider unit test methods.
    """

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    ###########################
    # Tests for Cucumber < 5

    # execute

    def test_execute_nok(self):
        """
        In a Cucumber < 5 context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber/execute'}
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('\'test\'' in msg)

    def test_execute_ok(self):
        """
        In a Cucumber < 5 context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/execute',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 4)

    # def test_execute_ok_wd(self):
    #     mock = MagicMock(return_value=mockresponse)
    #     with patch('opentf.toolkit.core.publish', mock):
    #         command = PROVIDERCOMMAND.copy()
    #         command['step'] = {
    #             'uses': 'cucumber/execute',
    #             'with': {
    #                 'test': 'cucumberProject/src/test/java/features/sample.feature'
    #             },
    #             'working-directory': 'foo',
    #         }
    #         _dispatch(implementation.execute_action, command)
    #     mock.assert_called_once()
    #     steps = mock.call_args[0][0]['steps']
    #     self.assertTrue(len(steps) == 4)
    #     self.assertEqual(steps[0]['working-directory'], 'foo')
    #     self.assertEqual(steps[1]['working-directory'], 'foo')
    #     self.assertEqual(steps[2]['working-directory'], 'foo/cucumberProject/target')
    #     self.assertEqual(steps[3]['working-directory'], 'foo')

    # cucumber

    def test_cucumber_nok(self):
        """
        In a Cucumber < 5 context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber/cucumber'}
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('\'test\'' in msg)

    def test_cucumber_nok_unknownreport(self):
        """
        In a Cucumber < 5 context, checks the error message content when a
        non-existing reporter name is provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'foobar'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('Unexpected reporter' in msg)

    def test_cucumber_ok_noreport(self):
        """
        In a Cucumber < 5 context, checks the length and purpose of steps when
        no report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('', steps[0]['run'])
        self.assertEqual('', steps[2]['run'])

    def test_cucumber_ok_junit(self):
        """
        In a Cucumber < 5 context, checks the presence of the XML report when
        JUnit report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertEqual(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )

    def test_cucumber_ok_junithtml(self):
        """
        In a Cucumber < 5 context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 4)
        self.assertTrue('rm -f cucumberProject/target/report.xml' in steps[0]['run'])
        self.assertTrue('rm -rf cucumberProject/target/html-report' in steps[0]['run'])
        self.assertTrue(
            'rm -f cucumberProject/target/html-report.tar' in steps[0]['run']
        )
        self.assertTrue(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
            in steps[3]['run']
        )
        self.assertFalse(
            'echo "::attach::`pwd`/cucumberProject/target/html-report"'
            in steps[3]['run']
        )
        self.assertTrue(
            'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
            in steps[3]['run']
        )

    def test_cucumber_ok_tags(self):
        """
        In a Cucumber < 5 context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tag': 'TaG1',
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertTrue(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
            in steps[2]['run']
        )
        self.assertTrue(' --tags @TaG1"' in steps[1]['run'])

    def test_cucumber_nok_tags_tag_expression(self):
        """
        In a Cucumber < 5 context, checks the error message content when both
         'tag' and 'tags' fields are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'tag': 'TaG1',
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('fields cannot coexist' in msg)

    ###########################
    # Tests for Cucumber 5+

    # 'execute' action

    def test_execute5_nok(self):
        """
        In a Cucumber 5+ context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber5/execute'}
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('\'test\'' in msg)

    def test_execute5_ok(self):
        """
        In a Cucumber 5+ context, checks the presence of 4 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/execute',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.execute5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 4)

    # 'cucumber5' action

    def test_cucumber5_nok(self):
        """
        In a Cucumber 5+ context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cucumber5/cucumber'}
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('\'test\'' in msg)

    def test_cucumber5_nok_unknownreport(self):
        """
        In a Cucumber 5+ context, checks the error message content when a
        non-existing reporter name is provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'foobar'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('Unexpected reporter' in msg)

    def test_cucumber5_ok_noreport(self):
        """
        In a Cucumber 5+ context, checks the length and purpose of steps when
        no report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature'
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('', steps[0]['run'])
        self.assertEqual('', steps[2]['run'])

    def test_cucumber5_ok_junit(self):
        """
        In a Cucumber 5+ context, checks the presence of the XML report when
        JUnit report in included within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertEqual(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"',
            steps[2]['run'],
        )

    def test_cucumber5_ok_junithtml(self):
        """
        In a Cucumber 5+ context, checks the presence of the XML and HTML report
        files when both reporters are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit', 'html'],
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 4)
        self.assertTrue('rm -f cucumberProject/target/report.xml' in steps[0]['run'])
        self.assertTrue('rm -rf cucumberProject/target/html-report' in steps[0]['run'])
        self.assertTrue(
            'rm -f cucumberProject/target/html-report.tar' in steps[0]['run']
        )
        self.assertTrue(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
            in steps[3]['run']
        )
        self.assertFalse(
            'echo "::attach::`pwd`/cucumberProject/target/html-report"'
            in steps[3]['run']
        )
        self.assertTrue(
            'echo "::attach::`pwd`/cucumberProject/target/html-report.tar"'
            in steps[3]['run']
        )

    def test_cucumber5_ok_tags(self):
        """
        In a Cucumber 5+ context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tag': 'TaG1',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertTrue(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
            in steps[2]['run']
        )
        self.assertTrue('-Dcucumber.filter.tags=\"@TaG1\"' in steps[1]['run'])

    def test_cucumber5_ok_tag_expression(self):
        """
        In a Cucumber 5+ context, checks the presence of the tag filter syntax
        within the launch step, along with the tag described in the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'reporters': ['junit'],
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertTrue(len(steps) == 3)
        self.assertEqual('rm -f cucumberProject/target/report.xml', steps[0]['run'])
        self.assertTrue(
            'echo "::attach type=application/vnd.opentestfactory.cucumber-surefire+xml::`pwd`/cucumberProject/target/report.xml"'
            in steps[2]['run']
        )
        self.assertTrue('-Dcucumber.filter.tags=\"@TaG1 and @TaG2\"' in steps[1]['run'])

    def test_cucumber5_nok_tags_tag_expression(self):
        """
        In a Cucumber 5+ context, checks the error message content when both
         'tag' and 'tags' fields are provided within the PEAC.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cucumber5/cucumber',
                'with': {
                    'test': 'cucumberProject/src/test/java/features/sample.feature',
                    'tag': 'TaG1',
                    'tags': '@TaG1 and @TaG2',
                },
            }
            _dispatch(implementation.cucumber5_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertTrue('fields cannot coexist' in msg)


if __name__ == '__main__':
    unittest.main()
