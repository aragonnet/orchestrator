# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Expressions unit tests."""

import unittest

from opentf.commons import expressions


CONTEXTS_1 = {'variables': {'foo': 'fOO', 'bar': 'bAR'}}

CONTEXTS_2 = {
    'variables': {'foo': 'fOO', 'bar': 'bAR'},
    'squashtf': {
        'workflow': 'wORKFLOW',
        'job': 'jOB',
        'actor': 'me',
        'token': 'tOKEN',
        'step': 'sTEP',
    },
    'job': {'status': 'failure'},
    'steps': {
        'step_1': {
            'outputs': {'output_s1': 'oUTPUT_S1', 'output_s2': 'conclusion'},
            'outcome': 'failure',
            'conclusion': 'failure',
        },
        'step_2': {'outcome': 'success', 'conclusion': 'success'},
        'step_3': {'outputs': {'output_s1': None}},
        'step_4': {'outputs': {'output_s1': False, 'output_s2': True}},
    },
    'runner': {'os': 'Linux', 'temp': '/tmp'},
    'need': {'job_1': {'result': 'success', 'outputs': {'output_j1': 'oOUTPUT_J1'}}},
}

ITEMS = {
    'zero': 'not an expression',
    'one': '${{ variables.bar }}',
    'two': 'Hi there, ${{ squashtf.actor }}.',
    'three': [0, 1, '2', 3, '${{ variables.foo}}', [11, 22, '${{ 33 }}'], {'a': 12}],
    'four': {
        'four1': '${{ job.status }}',
        'four2': "${{ steps.step_2.outcome == 'success'}}",
    },
}


class TestExpressions(unittest.TestCase):
    def test_mres_1(self):
        response = expressions._maybe_remove_expression_syntax('foo')
        self.assertEqual(response, 'foo')

    def test_mres_2(self):
        response = expressions._maybe_remove_expression_syntax('${{foo}}')
        self.assertEqual(response, 'foo')

    def test_mres_3(self):
        response = expressions._maybe_remove_expression_syntax('  ${{ foo  }}')
        self.assertEqual(response, 'foo')

    def test_ee_1(self):
        response = expressions.evaluate('variables.foo', CONTEXTS_1)
        self.assertEqual(response, 'fOO')

    def test_ee_2(self):
        self.assertRaises(KeyError, expressions.evaluate, 'variables.baz', CONTEXTS_1)

    def test_ee_3(self):
        response = expressions.evaluate("variables['foo']", CONTEXTS_1)
        self.assertEqual(response, 'fOO')

    def test_ee_3_bis(self):
        self.assertRaises(Exception, expressions.evaluate, 'variables[==]', CONTEXTS_1)

    def test_ee_4(self):
        response = expressions.evaluate('steps.step_1.outcome', CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_5(self):
        response = expressions.evaluate("steps['step_1'].outcome", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_6(self):
        response = expressions.evaluate("steps.step_1['outcome']", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_7(self):
        response = expressions.evaluate("steps['step_1']['outcome']", CONTEXTS_2)
        self.assertEqual(response, 'failure')

    def test_ee_8(self):
        response = expressions.evaluate("'hi there'", CONTEXTS_2)
        self.assertEqual(response, 'hi there')

    def test_ee_9(self):
        response = expressions.evaluate("'hi ''foo'' yada'", CONTEXTS_2)
        self.assertEqual(response, "hi 'foo' yada")

    def test_ee_10(self):
        response = expressions.evaluate("8.23", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_10_p(self):
        response = expressions.evaluate("(8.23)", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_10_p(self):
        response = expressions.evaluate("((8.23))", CONTEXTS_2)
        self.assertEqual(response, 8.23)

    def test_ee_11(self):
        response = expressions.evaluate("8.23 == 8.23", CONTEXTS_2)
        self.assertEqual(response, True)

    def test_ee_12(self):
        response = expressions.evaluate("8.23 != 8.23", CONTEXTS_2)
        self.assertEqual(response, False)

    def test_ee_13(self):
        response = expressions.evaluate("variables['foo'] == 'fOO'", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_14(self):
        response = expressions.evaluate("'fOO' == variables['foo']", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_14_p(self):
        response = expressions.evaluate("('fOO' == variables['foo'])", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_14_ci(self):
        response = expressions.evaluate("'foo' == variables['foo']", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_14_ci_p(self):
        response = expressions.evaluate("('foo') == variables['foo']", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_14_ci_p_p(self):
        response = expressions.evaluate("('foo') == (variables['foo'])", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_15(self):
        response = expressions.evaluate("'fOO' != variables['foo']", CONTEXTS_1)
        self.assertEqual(response, False)

    def test_ee_16(self):
        response = expressions.evaluate("'fON' <= variables['foo']", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_17(self):
        response = expressions.evaluate("'fON' >= variables['foo']", CONTEXTS_1)
        self.assertEqual(response, False)

    def test_ee_18(self):
        response = expressions.evaluate(
            "steps.step_1.outcome == steps.step_1.conclusion", CONTEXTS_2
        )
        self.assertEqual(response, True)

    def test_ee_19(self):
        response = expressions.evaluate(
            "steps.step_2[steps.step_1.outputs.output_s2] == steps.step_2.conclusion",
            CONTEXTS_2,
        )
        self.assertEqual(response, True)

    def test_ee_20_eq(self):
        response = expressions.evaluate("0x10 == 16", CONTEXTS_2)
        self.assertEqual(response, True)

    def test_ee_20_gt(self):
        response = expressions.evaluate("0x10 > 16", CONTEXTS_2)
        self.assertEqual(response, False)

    def test_ee_20_lt(self):
        response = expressions.evaluate("0x10 < 16", CONTEXTS_2)
        self.assertEqual(response, False)

    def test_ee_21(self):
        response = expressions.evaluate("'' == 0", CONTEXTS_2)
        self.assertEqual(response, True)

    def test_ee_22(self):
        response = expressions.evaluate("'0' == 0", CONTEXTS_2)
        self.assertEqual(response, True)

    def test_ee_22_no(self):
        response = expressions.evaluate("'1' == 0", CONTEXTS_2)
        self.assertEqual(response, False)

    def test_ee_23(self):
        response = expressions.evaluate("'17' == 0x11", CONTEXTS_2)
        self.assertEqual(response, True)

    def test_ee_24(self):
        response = expressions.evaluate(
            "steps.step_3.outputs.output_s1 == 0", CONTEXTS_2
        )
        self.assertEqual(response, True)

    def test_ee_25(self):
        response = expressions.evaluate(
            "steps.step_3.outputs.output_s1 != 0", CONTEXTS_2
        )
        self.assertEqual(response, False)

    def test_ee_26_false(self):
        response = expressions.evaluate(
            "steps.step_4.outputs.output_s1 == 0", CONTEXTS_2
        )
        self.assertEqual(response, True)

    def test_ee_27_true(self):
        response = expressions.evaluate(
            "steps.step_4.outputs.output_s2 == 0", CONTEXTS_2
        )
        self.assertEqual(response, False)

    def test_ee_28_true(self):
        response = expressions.evaluate("'abc' == 0", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_29_true(self):
        response = expressions.evaluate("steps.step_1 == 0", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_30(self):
        response = expressions.evaluate_items(ITEMS, CONTEXTS_2)
        self.assertEqual(set(response), set(ITEMS))
        self.assertEqual(response['one'], 'bAR')
        self.assertEqual(response['two'], 'Hi there, me.')
        self.assertNotEqual(response['three'], ITEMS['three'])
        self.assertEqual(response['four']['four1'], 'failure')
        self.assertTrue(response['four']['four2'])
        self.assertEqual(response['three'][4], 'fOO')
        self.assertEqual(response['three'][5][2], 33)

    def test_eb_40(self):
        response = expressions.evaluate_bool('always()', CONTEXTS_2)
        self.assertTrue(response)

    def test_eb_41(self):
        response = expressions.evaluate_bool('success()', CONTEXTS_2)
        self.assertFalse(response)

    def test_eb_42(self):
        response = expressions.evaluate_bool('cancelled()', CONTEXTS_2)
        self.assertFalse(response)

    def test_eb_43(self):
        response = expressions.evaluate_bool('failure()', CONTEXTS_2)
        self.assertTrue(response)

    def test_eb_44(self):
        response = expressions.evaluate_bool('failure ( ) ', CONTEXTS_2)
        self.assertTrue(response)

    def test_eb_45(self):
        self.assertRaises(Exception, expressions.evaluate_bool, 'failure(', CONTEXTS_2)

    def test_eb_46(self):
        self.assertRaises(Exception, expressions.evaluate_bool, 'foo()', CONTEXTS_2)

    def test_eb_47(self):
        response = expressions.evaluate_bool("job.status == 'failure'", CONTEXTS_2)
        self.assertTrue(response)

    def test_eb_48(self):
        response = expressions.evaluate_bool("fAlSe", CONTEXTS_2)
        self.assertFalse(response)

    def test_ee_50(self):
        self.assertRaises(Exception, expressions.evaluate, '1+2', CONTEXTS_1)

    def test_ee_51(self):
        self.assertRaises(Exception, expressions.evaluate, '1 2', CONTEXTS_1)

    def test_ee_52(self):
        self.assertRaises(Exception, expressions.evaluate, "'1' '2'", CONTEXTS_1)

    def test_ee_53(self):
        self.assertRaises(Exception, expressions.evaluate, "job.status '2'", CONTEXTS_2)

    def test_ee_54(self):
        self.assertRaises(Exception, expressions.evaluate, "==", CONTEXTS_2)

    def test_ee_55(self):
        self.assertRaises(
            Exception, expressions.evaluate, "job.status.'abc'", CONTEXTS_2
        )

    def test_ee_and(self):
        response = expressions.evaluate("(8.23 != 8.23) && true", CONTEXTS_1)
        self.assertEqual(response, False)

    def test_ee_and_2(self):
        self.assertRaises(
            Exception, expressions.evaluate, "(8.23 != 8.23 && true", CONTEXTS_1
        )

    def test_ee_and_3(self):
        self.assertRaises(
            Exception, expressions.evaluate, "((8.23 != 8.23) && true", CONTEXTS_1
        )

    def test_ee_and_4(self):
        self.assertRaises(
            Exception, expressions.evaluate, "(8.23 != 8.23) && (true", CONTEXTS_1
        )

    def test_ee_and_5(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome == steps.step_1.conclusion) && (steps.step_1.outcome != steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertEqual(response, False)

    def test_ee_and_6(self):
        response = expressions.evaluate("true && true && false", CONTEXTS_1)
        self.assertEqual(response, False)

    def test_ee_and_7(self):
        response = expressions.evaluate("true && true && false && true", CONTEXTS_1)
        self.assertEqual(response, False)

    def test_ee_and_8(self):
        response = expressions.evaluate("true && true && true", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_or(self):
        response = expressions.evaluate("(8.23 != 8.23) || true", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_or_2(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome == steps.step_1.conclusion) || (steps.step_1.outcome != steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertEqual(response, True)

    def test_ee_or_2_bis(self):
        response = expressions.evaluate(
            "(steps.step_1.outcome != steps.step_1.conclusion) || (steps.step_1.outcome == steps.step_1.conclusion)",
            CONTEXTS_2,
        )
        self.assertEqual(response, True)

    def test_ee_and_or(self):
        response = expressions.evaluate("(true && false) || (true && true)", CONTEXTS_1)
        self.assertEqual(response, True)

    def test_ee_and_or_2(self):
        response = expressions.evaluate(
            "(true && false) || (false && true)", CONTEXTS_1
        )
        self.assertEqual(response, False)

    def test_ee_and_or_2(self):
        response = expressions.evaluate(
            "true || (true && false) || (false && true)", CONTEXTS_1
        )
        self.assertEqual(response, True)


if __name__ == '__main__':
    unittest.main()
