#!/bin/bash

# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ -d /run/user/$(id -u) ]] && [[ -w /run/user/$(id -u) ]]; then
        RUNDIR=/run/user/$(id -u)
else
        mkdir /tmp/squash-tf-2
        RUNDIR=/tmp/squash-tf-2
fi

stop(){
    echo "Killing $1"
    kill $(cat $RUNDIR/$1.pid)
    rm $RUNDIR/$1.pid
}

. $(dirname $0)/launcher.cfg

for service in ${CORE_SERVICE_LIST}; do
    stop $service
done

for service in ${PLUGIN_SERVICE_LIST}; do
    stop $service
done

sleep ${START_INTERVAL_SECONDS}s

stop eventbus
