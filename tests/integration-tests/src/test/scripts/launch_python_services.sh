#!/bin/bash

# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. $(dirname $0)/launcher.cfg

if [[ -d /run/user/$(id -u) ]] && [[ -w /run/user/$(id -u) ]]; then
	RUNDIR=/run/user/$(id -u)
else
	mkdir /tmp/squash-tf-2
	RUNDIR=/tmp/squash-tf-2
fi

usage()
{
    echo "$0 [--help | --debug]"
    echo "--help : print this message"
    echo "--debug : activate debug output in services"
    exit 0
}

start(){
    python3.8 -m squashtf.$1.$2 --context insecure &
    echo $! | tee $RUNDIR/$2.pid
    sleep ${START_INTERVAL_SECONDS}s
}

if [[ $# -gt 0 ]]; then
    case $1 in
         --help)
                 usage
                 ;;
         --debug)
                 export DEBUG_LEVEL=DEBUG
                 ;;
         *)
                echo "Unknown parameter $1" >&2
                exit 1
                ;;
    esac
fi

$(dirname $0)/stop_python_services.sh

cd ${CONF_ROOT}

start core eventbus

for service in ${CORE_SERVICE_LIST}; do
    start core $service
done

for service in ${PLUGIN_SERVICE_LIST}; do
    start plugins $service
done
