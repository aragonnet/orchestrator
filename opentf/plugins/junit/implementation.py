# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the junit provider.

The following categories are provided:

- execute
- params
- mvntest
"""

from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
MVNTEST_CATEGORY = 'mvntest'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'
OPENTF_PARAM_FILE = '_SQUASH_TF_TESTCASE_PARAM_FILES'

REPORT_TYPE = 'application/vnd.opentestfactory.junit-surefire+xml'

########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: junit/execute@v1
      with:
        test: path/to/test/root/qualified.testsuite.ClassName#testName
    ```

    The path to test root is a relative path from the step's working
    directory.  If not specified, this is the job's working directory,
    in which case it must include the repository name.
    """
    core.debug('Identified as the "execute" action')

    test_reference = inputs['test']

    parts = test_reference.partition('/')
    datasource = ''
    test_case_arg = test_reference
    while len(parts[2].rstrip()) > 0:
        datasource = datasource + parts[0] + '/'
        test_case_arg = parts[2]
        parts = parts[2].partition('/')

    test_class = test_case_arg.partition('#')[0]
    target = f'{datasource}target'
    txt_report = f'{target}/surefire-reports/{test_class}.txt'
    xml_report = f'{target}/surefire-reports/TEST-{test_class}.xml'
    junit_run_log = f'{target}/junit-run-log.txt'

    if core.runner_on_windows():
        create_target_dir = {
            'run': f'mkdir {target}'.replace('/', '\\'),
            'continue-on-error': True,
        }
        error_log = {
            'run': f'findstr /r "ERROR WARN" {target}\\junit-run-log.txt',
            'continue-on-error': True,
        }
    else:
        create_target_dir = {'run': f'mkdir -p {target}'}
        error_log = {
            'run': f'grep -E "(ERROR|WARN)" {target}/junit-run-log.txt',
            'continue-on-error': True,
        }

    steps = [
        {'uses': 'actions/delete-file@v1', 'with': {'path': txt_report}},
        {'uses': 'actions/delete-file@v1', 'with': {'path': xml_report}},
        create_target_dir,
        {
            'run': f'mvn -f "{datasource}pom.xml" '
            f'-Dtest={test_case_arg} -Dmaven.test.failure.ignore=true test > {junit_run_log}'
        },
        error_log,
        {'run': core.attach_file(txt_report)},
        {'run': core.attach_file(xml_report, type=REPORT_TYPE)},
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: junit/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

     `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.debug('Identified as the "param" action')

    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )

    path = f'{uuid4()}.ini'
    steps = [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {'run': core.export_variable(OPENTF_PARAM_FILE, path)},
    ]

    return steps


########################################################################
# 'mvntest' action


def mvntest_action(inputs):
    """Process 'mvntest' action.

    `mvntest` actions have a mandatory `test` input.  It may contain
    a properties input too.

    ```yaml
    - uses: junit/mvntest@v1
      with:
        test: class#method
        properties:
          foo: value1
          bar: value2
    ```

    This action will attach all `*.txt` and `*.xml` files found in the
    `target/surefire-reports` directory.
    """
    core.debug('Identified as the "mvntest" action')

    test = inputs['test']

    if props := inputs.get('properties'):
        properties = ' '.join(f'-D{k}={v}' for k, v in props.items())
    else:
        properties = ''

    txt_reports = 'target/surefire-reports/*.txt'
    xml_reports = 'target/surefire-reports/*.xml'
    steps = [
        {'uses': 'actions/delete-file@v1', 'with': {'path': txt_reports}},
        {'uses': 'actions/delete-file@v1', 'with': {'path': xml_reports}},
        {'run': f'mvn -Dtest={test} {properties} test', 'continue-on-error': True},
        {
            'uses': 'actions/get-files',
            'with': {'pattern': '*.txt'},
            'working-directory': 'target/surefire-reports',
        },
        {
            'uses': 'actions/get-files',
            'with': {
                'pattern': '*.xml',
                'type': REPORT_TYPE,
            },
            'working-directory': 'target/surefire-reports',
        },
    ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    MVNTEST_CATEGORY: mvntest_action,
}
