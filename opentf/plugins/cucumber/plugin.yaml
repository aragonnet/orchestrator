# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


###################################################
# 'execute' action for cucumber 5+

apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber5/execute
  description: |
    Processes 'execute' action for Cucumber 5 and up.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: cucumber5/execute@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#feature_name#scenario
    ```

    `feature_name` and `scenario` are optional and will not be used by the
    provider (the whole `.feature` file will be executed).

    However, these additional elements can be used by publisher plugins to only
    report the execution status of the targeted feature or scenario.

    Will attach the junit report  as `report.xml` and the html report as `html-report.tar`.
branding:
  icon: play-circle
  color: red
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber5
    category: execute
    categoryVersion: v1
inputs:
  test:
    description: the datasource to use
    required: true
---

###################################################
# 'execute' action for cucumber < 5

apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber/execute
  description: |
    Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: cucumber/execute@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#feature_name#scenario
    ```

    `feature_name` and `scenario` are optional and will not be used by the
    provider (the whole `.feature` file will be executed).

    However, these additional elements can be used by publisher plugins to only
    report the execution status of the targeted feature or scenario.

    Will attach the junit report  as `report.xml` and the html report as `html-report.tar`.
branding:
  icon: play-circle
  color: red
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber
    category: execute
    categoryVersion: v1
inputs:
  test:
    description: the datasource to use
    required: true
---

###################################################
# 'params' action for cucumber 5+

apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber5/params
  description: |
    Processes 'params' action for Cucumber 5+.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: cucumber5/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
branding:
  icon: plus-circle
  color: red
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber5
    category: params
    categoryVersion: v1
inputs:
  data:
    description: the data to use for the automated test
    required: true
  format:
    description: the format to use for the automated test data
    required: true
---

###################################################
# 'params' action for cucumber

apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber/params
  description: |
    Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: cucumber/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
       format: format
    ```

    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
branding:
  icon: plus-circle
  color: red
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber
    category: params
    categoryVersion: v1
inputs:
  data:
    description: the data to use for the automated test
    required: true
  format:
    description: the format to use for the automated test data
    required: true
---


###################################################
# native action for cucumber 5+

# plugin.yaml
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber5/cucumber
  description: |
    Processes 'cucumber5' action for Cucumber v5.0.0 and up.

    ```yaml
    - uses: cucumber5/cucumber@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#MyFeature#Selected scenario name
        tag: tag1
        tags: "@tag1 and @tag2"
        reporters:
          - junit
          - html
        extra-options: "-Dcucumber.ansi-colors.disabled=true"
    ```
    The `tag` field is optional, allowing to target one or many specific tagged
    scenarios or dataset within the `.feature` file. Please note that the `tag`
    value must not include the initial `@` character.

    Since Cucumber 5.7.0 (see the details in the
    <a href="https://github.com/cucumber/cucumber-jvm/blob/main/CHANGELOG.md#570-2020-05-01" target="_blank">changelog</a>),
    the allowed format supports tag expressions, which can target scenarios in a more
    specific way.

    To use this feature, you can fill the `tags` field using following sample formats:

    - `"@tag1 @tag2"`: will execute scenarios tagged by `@tag1` **or** `@tag2`.
    - `"@tag1 and @tag2"`: will execute scenarios tagged by **both** `@tag1` **and** `@tag2`.
    - `"@tag1 and not @tag2"`: will execute scenarios tagged by `@tag1` **but not** `@tag2`.
    - `"not (@tag1 or @tag2)"`: will execute scenarios tagged by **neither** `@tag1` **nor**
      `@tag2`.

    Double quotes are mandatory.

    For more details about Cucumber tag expressions, please read the
    <a href="https://cucumber.io/docs/cucumber/api/#tags" target="_blank">tag chapter</a>
    in the Cucumber documentation.

    *Please note that `tag` and `tags` fields cannot coexist*, thus using both of them
    in the same job will return an error.

    There is a list of 3 available reporters for your test reports:

    - JUnit (noted `junit`, attached as `report.xml`)
    - HTML (noted `html`, attached as `html-report.tar`)

    Any, all, or none of the 3 reporters can be added to the list of desired generated
    reports.  Note that without the JUnit reporter cucumber test result will not appear
    in Allure reports.

    The `extra-options` tag is optional too, allowing to specify additional content and
    parameters to Cucumber.

    Since the `cucumber.options` package is deprecated since Cucumber 5, there is now a
    list of supported properties you can find on the
    <a href="https://cucumber.io/docs/cucumber/api/#options" target="_blank">Options chapter</a>
    of the Cucumber documentation. To properly add an option within the `extra-options`
    tag, use the syntax below:

    - `-Dcucumber.[module].[param]=[value]`

    Module is optional as in `-Dcucumber.glue=com.example.glue` but can be mandatory
    as in `-Dcucumber.ansi-colors.disabled=true`

    Please note that we use several properties within our implementation, thus we do
    not recommend adding them in your extra options:

    - `cucumber.filter.tags` (used in the `tag` option)
    - `cucumber.plugin` (used in the `reporters` list)
    - `cucumber.features` (used in the `test` option)

branding:
  icon: play
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber5
    category: cucumber
    categoryVersion: v1
inputs:
  test:
    description: the datasource to use
    required: true
  tag:
    description: targets a specific tagged scenario or dataset
    required: false
  reporters:
    description: the desired reports
    required: false
  extra-options:
    description: additional parameters to pass to Cucumber
    required: false
---

###################################################
# 'execute' action for cucumber < 5

# plugin.yaml
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cucumber
  action: cucumber/cucumber
  description: |
    Processes 'cucumber' action for Cucumber versions up to 5.

    ```yaml
    - uses: cucumber/cucumber@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature
        tag: tag1
        reporters:
        - junit
        - html
        extra-options: extra
    ```

    `tag` is optional, allowing to target a specific tagged scenario or dataset
    in the `.feature` file. Please note that the `tag` value must not include
    the initial `@` character.

    `extra-options` is optional too, allowing to specify additional content
    for the `cucumber.options` property.

    Any, all, or none of the 3 reporters can be added to the list of
    desired generated reports.

    Will attach the junit report  as `report.xml` and the html report as `html-report.tar`.
branding:
  icon: play
cmd: 'python3 -m opentf.plugins.cucumber.main'
events:
  - categoryPrefix: cucumber
    category: cucumber
    categoryVersion: v1
inputs:
  test:
    description: the datasource to use
    required: true
  tag:
    description: target a specific tagged scenario or dataset
    required: false
  reporters:
    description: the desired reports
    required: false
  extra-options:
    description: additional parameters to pass to Cucumber
    required: false
