# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the cucumber provider.

Since v2021-12, Cucumber 5 and up are supported. This changes the
provider implementation as it now provides two versions of each
action (execute, params and cucumber):
- one for execution environments running Cucumber versions up to 5 ;
- one for those running Cucumber 5 and up.
For maintainability purposes, common content within these functions has
been factorised into other functions called by both version-specific actions.
As some versions of cucumber6 only produce valid HTML reports when using UTF-8,
we set maven's JVM to run with UTF-8 file encoding.

The following categories are provided:

- params5
- params
- param_preparation: called by both 'params'

- execute5
- execute
- execute_common_preparation: called by both 'execute'

- cucumber5
- cucumber
- cucumber_prepare_launch: called by both
- cucumber_create_steps: called by both
- cucumber_launch_command: called by cucumber_create_steps
"""

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = "cucumber/execute"
EXECUTE5_CATEGORY = "cucumber5/execute"
PARAM_CATEGORY = "cucumber/params"
PARAM5_CATEGORY = "cucumber5/params"
CUCUMBER_CATEGORY = "cucumber/cucumber"
CUCUMBER5_CATEGORY = "cucumber5/cucumber"

SQUASHTM_FORMAT = "tm.squashtest.org/params@v1"
SQUASHTM_TAG = "SQUASH_TF_CUCUMBER_TEST_TAG"

HTML_REPORT_NAME = "html-report.html"
HTML_ARCHIVE_PATH = "target/html-report.tar"
REPORTERS = {
    "junit": "target/report.xml",
    "html": f"target/{HTML_REPORT_NAME}",
}

CUCUMBER_TAG_SYNTAX = "--tags "
CUCUMBER5_TAG_SYNTAX = "-Dcucumber.filter.tags="

REPORT_TYPE = 'application/vnd.opentestfactory.cucumber-surefire+xml'


########################################################################
# 'execute' common action called by version-specific native actions


def execute_common_preparation(inputs):
    """
    The method called by both `execute_action` and `execute5_action` methods.
    """
    parameters = {
        "test": inputs["test"],
        "reporters": ["junit", "html"],
        "extra-options": f"%{SQUASHTM_TAG}%"
        if core.runner_on_windows()
        else f"${SQUASHTM_TAG}",
    }
    return parameters


########################################################################
## 'execute5' action (for Cucumber 5 and up)


def execute5_action(inputs):
    """
    The Cucumber 5 and up execute action.
    """
    parameters = execute_common_preparation(inputs)
    return cucumber5_action(parameters)


########################################################################
## 'execute' action (for Cucumber up to 4)


def execute_action(inputs):
    """
    The execute action for Cucumber versions up to 4.
    """
    parameters = execute_common_preparation(inputs)
    # maybe add @if not defined {SQUASHTM_TAG} @echo "{SQUASHTM_TAG}= " >> %OPENTF_VARIABLES%
    return cucumber_action(parameters)


########################################################################
# 'params' preparation called by version-specific actions


def param_preparation(inputs, tag_syntax):
    """
    The method called by both `param_action` and `param5_action` to:
        - check the params format
        - check the keys conformity in data
        - export data sets into execution environment variables
    """
    data = inputs["data"]
    format_ = inputs["format"]

    if format_ != SQUASHTM_FORMAT:
        core.fail("Unknown format value for params.")

    if data.keys() - {"global", "test"}:
        core.fail(
            "Unexpected keys found in data, was only expecting global and/or test."
        )
    if "test" in data and (dsname := data["test"].get("DSNAME")):
        value = f"{tag_syntax}@{dsname}"
    else:
        value = " "

    return [{"run": core.export_variable(SQUASHTM_TAG, value)}]


########################################################################
# 'params' action for Cucumber 5 and up


def param5_action(inputs):
    """
    The parameters action for Cucumber 5 and up.
    """
    return param_preparation(inputs, CUCUMBER5_TAG_SYNTAX)


########################################################################
# 'params' action for Cucumber up to 4


def param_action(inputs):
    """
    The parameters action for Cucumber versions up to 4.
    """
    return param_preparation(inputs, CUCUMBER_TAG_SYNTAX)


########################################################################
# 'cucumber' preparation called by version-specific actions (version-insensitive)


def cucumber_prepare_launch(inputs):
    """
    The method called by both `cucumber_action` and `cucumber5_action` to:
    - extract data from PEAC ;
    - check reporters conformity ;
    - check tag compatibility.
    Returns a dictionary containing all relevant data to the version-specific native action.
    """
    datasource, _, testsource = inputs["test"].partition("/")

    reporters = set(inputs.get("reporters", []))
    if reporters - set(REPORTERS):
        core.fail(f"Unexpected reporter(s) {reporters - set(REPORTERS)}.")

    prepare, reports, attachments = [], [], []

    for reporter in reporters:
        reports.append(f"{reporter}:{REPORTERS[reporter]}")
        root = f"{datasource}/{REPORTERS[reporter]}"

        if reporter == "html":
            prepare.append(core.delete_directory(root))
            prepare.append(core.delete_file(f"{datasource}/{HTML_ARCHIVE_PATH}"))
            attachments.append(core.attach_file(f"{datasource}/{HTML_ARCHIVE_PATH}"))
        else:
            prepare.append(core.delete_file(root))
            attachments.append(core.attach_file(root, type=REPORT_TYPE))

    feature_path = testsource.partition("#")[0]

    tag, tags, extra_options = '', '', ''
    if tags := inputs.get("tags"):
        if inputs.get("tag"):
            core.fail(
                "Conflict identified in PEaC: 'tag' and 'tags' fields cannot coexist"
            )
    elif tag := inputs.get("tag"):
        tags = f'@{tag}'

    if inputs.get("extra-options"):
        extra_options = inputs.get("extra-options")

    params = {
        "datasource": datasource,
        "feature_path": feature_path,
        "reporters": reporters,
        "prepare": prepare,
        "reports": reports,
        "attachments": attachments,
        "tags": tags,
        "extra-options": extra_options,
    }

    return params


########################################################################
# 'cucumber' steps creation (version-insensitive)


def cucumber_create_steps(inputs):
    """
    The method called by both `cucumber_action` and `cucumber5_action`
    after `cucumber_prepare_launch` to prepare steps.
    """
    steps = [
        {"run": "\n".join(inputs['prepare'])},
        {
            'run': cucumber_launch_command(inputs['datasource'], inputs['command']),
            'variables': {
                "_JAVA_OPTIONS": f"{use_remote_variable('_JAVA_OPTIONS')} -Dfile.encoding=UTF-8"
            },
        },
    ]
    if "html" in inputs['reporters']:
        steps.append(
            {
                "run": f"tar -cvf {HTML_ARCHIVE_PATH} -C target {HTML_REPORT_NAME}",
                "working-directory": f"{inputs['datasource']}",
            }
        )

    steps.append({'run': '\n'.join(inputs['attachments'])})
    return steps


def use_remote_variable(varname):
    """
    This method uses an execution environment variable in a step, using  OS-specific constructs.
    """
    if core.runner_on_windows():
        return f'%{varname}%'
    return f'${varname}'


########################################################################
# 'cucumber' JVM launch command (version-insensitive)


def cucumber_launch_command(datasource, input_):
    """
    The method called by `cucumber_create_steps` to send the
    right version-specific syntax to the JVM.
    """
    command = (
        f'mvn test -f "{datasource}/pom.xml" -Dmaven.test.failure.ignore=true{input_}'
    )
    if 'cucumber.options' in input_:
        command += '"'
    return command


########################################################################
# 'cucumber5' action (for Cucumber 5 and up)


def cucumber5_action(inputs):
    """
    The Cucumber 5 and up native action.
    """
    params = cucumber_prepare_launch(inputs)
    feature_path, reports = params['feature_path'], params['reports']
    params[
        'command'
    ] = f' -Dcucumber.features="{feature_path}" -Dcucumber.plugin="{",".join(reports)}"'

    if tags := params['tags']:
        params['command'] += f' {CUCUMBER5_TAG_SYNTAX}"{tags}"'

    if extra_options := params['extra-options']:
        params['command'] += f' {extra_options}'

    steps = cucumber_create_steps(params)

    return steps


########################################################################
# 'cucumber' action (for Cucumber up to 4)


def cucumber_action(inputs):
    """
    The native action for Cucumber up to 4.
    """
    params = cucumber_prepare_launch(inputs)
    feature_path, reports = params['feature_path'], params['reports']
    params[
        'command'
    ] = f' -Dcucumber.options="{feature_path} {" ".join("--plugin " + r for r in reports)}'

    if tags := params['tags']:
        params['command'] += f' {CUCUMBER_TAG_SYNTAX}{tags}'

    if extra_options := params['extra-options']:
        params['command'] += f' {extra_options}'

    steps = cucumber_create_steps(params)

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    EXECUTE5_CATEGORY: execute5_action,
    PARAM_CATEGORY: param_action,
    PARAM5_CATEGORY: param5_action,
    CUCUMBER_CATEGORY: cucumber_action,
    CUCUMBER5_CATEGORY: cucumber5_action,
}
