# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A simple publisher plugin.

This publisher plugin copies published documents to a S3 bucket.

# Usage

```
python3 -m opentf.plugins.s3publisher.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from typing import Any, List

import json
import os
import sys

from flask import request
import boto3

from opentf.commons import (
    EXECUTIONRESULT,
    WORKFLOWRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
)

########################################################################
## Constants

CONFIG_FILE = 'conf/s3publisher.yaml'


########################################################################
## Main

app = make_app(
    name='s3publisher',
    description='Create and start a S3 publisher plugin.',
    configfile=CONFIG_FILE,
)

context = app.config['CONTEXT']
s3credentials = context['s3credentials']


@app.route('/inbox', methods=['POST'])
def process_result():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('BadRequest', 'Body must be a JSON object.')

    if 'apiVersion' not in body or 'kind' not in body:
        return make_status_response('BadRequest', 'Missing apiVersion or kind')

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    if schema in (EXECUTIONRESULT, WORKFLOWRESULT):
        valid, extra = validate_schema(schema, body)
        if not valid:
            return make_status_response(
                'BadRequest', f'Not a valid {body["kind"]} request: {extra}'
            )
    else:
        return make_status_response(
            'BadRequest', f'No kind or invalid kind: {body.get("kind")}.'
        )

    workflow_id = body['metadata']['workflow_id']
    if 'attachments' in body:
        push_attachments_to_s3(body['attachments'], workflow_id)

    return make_status_response('OK', '')


def push_attachments_to_s3(attachments: List[str], workflow_id: str):
    """Push attachments to a s3 bucket.

    # Required parameters

    - attachments: a list of strings
    - workflow_id: a string
    """
    if check_credential_file():
        if client := create_s3_client():
            app.logger.debug('Files to upload: %s', attachments)
            for output in attachments:
                try:
                    client.upload_file(
                        output,
                        context['bucket'],
                        f'{workflow_id}/{output.split("/")[-1]}',
                    )
                    app.logger.debug('Uploaded file: %s', output)
                except Exception as err:
                    app.logger.error('Could not upload file: %s', str(err))


def check_credential_file() -> bool:
    """Ensure credential file exists."""
    cred_file_exist = os.path.exists(s3credentials) and os.access(
        s3credentials, os.R_OK
    )
    if not cred_file_exist:
        app.logger.debug('Missing S3 credentials, ignoring attachments.')
    return cred_file_exist


def create_s3_client() -> Any:
    """Create s3 client using credential file."""
    try:
        with open(s3credentials, 'r', encoding='utf-8') as f:
            settings = json.load(f)
        client = boto3.client(
            's3',
            region_name=settings['region_name'],
            endpoint_url=settings['endpoint_url'],
            aws_access_key_id=settings['aws_access_key_id'],
            aws_secret_access_key=settings['aws_secret_access_key'],
        )
    except Exception as err:
        app.logger.error('Could not initialize S3: %s', str(err))
        client = None

    return client


def main(plugin):
    """Start the S3 Publisher service."""
    try:
        plugin.logger.info(f'Subscribing to {EXECUTIONRESULT}.')
        uuid_e = subscribe(kind=EXECUTIONRESULT, target='/inbox', app=plugin)
        plugin.logger.info(f'Subscribing to {WORKFLOWRESULT}.')
        uuid_w = subscribe(kind=WORKFLOWRESULT, target='/inbox', app=plugin)
    except Exception as err:
        plugin.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        if not check_credential_file():
            plugin.logger.warning(f'Missing S3 credentials {s3credentials}.')
        plugin.logger.info('Starting S3 publisher service.')
        run_app(plugin)
    finally:
        unsubscribe(uuid_w, app=plugin)
        unsubscribe(uuid_e, app=plugin)


if __name__ == '__main__':
    main(app)
