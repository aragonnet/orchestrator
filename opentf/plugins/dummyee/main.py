# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A dummy execution environment plugin.

This execution environment stub serialize communications but does not
attempt to parallelize jobs and workflows.

# Usage

```
python3 -m opentf.plugins.dummyee.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from queue import Queue
import sys
import threading

from flask import request

from opentf.commons import (
    EXECUTIONCOMMAND,
    EXECUTIONERROR,
    EXECUTIONRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    make_event,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/dummyee.yaml'

W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green
O = '\033[33m'  # orange
B = '\033[34m'  # blue
P = '\033[35m'  # purple


########################################################################
## Main

STEPS_QUEUE = Queue()
JOBS = {}

app = make_app(
    name='dummyee',
    description='Create and start an execution env plugin.',
    configfile=CONFIG_FILE,
)


def list_job_steps(job_id):
    """List all steps for job."""
    if job_id not in JOBS:
        JOBS[job_id] = {}
    return JOBS[job_id]


def add_job_step(job_id, command):
    """Adding new step to existing job."""
    JOBS[job_id][command['metadata']['step_sequence_id']] = command


def handle_commands():
    """Handle steps."""
    while True:
        command = STEPS_QUEUE.get()
        metadata = command['metadata']
        job_id = metadata['job_id']

        if metadata['step_sequence_id'] in list_job_steps(job_id):
            continue

        add_job_step(job_id, command)

        try:
            print(
                f'{B}{job_id} {metadata["step_sequence_id"]} {G}{command["scripts"]}{W}'
            )
            publish(
                make_event(
                    EXECUTIONRESULT,
                    metadata=metadata,
                    status=0,
                ),
                context=app.config['CONTEXT'],
            )
        except Exception as err:
            print(err)
            publish(
                make_event(
                    EXECUTIONERROR,
                    metadata=metadata,
                    details={'error': str(err)},
                ),
                context=app.config['CONTEXT'],
            )


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(EXECUTIONCOMMAND, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid ExecutionCommand request: {extra}'
        )

    metadata = body['metadata']
    job_id = metadata['job_id']

    steps = list_job_steps(job_id)
    if metadata['step_sequence_id'] in steps:
        return make_status_response(
            'AlreadyExists',
            f'Step {metadata["step_sequence_id"]} already received for job {job_id}.',
        )

    STEPS_QUEUE.put(body)

    return make_status_response(
        'OK', f'Step #{metadata["step_sequence_id"]} added to job {job_id} queue.'
    )


def main():
    """Start the Arranger service."""
    try:
        app.logger.info('Starting steps handling thread.')
        threading.Thread(target=handle_commands, daemon=True).start()
    except Exception as err:
        app.logger.error('Could not start steps handling thread: %s.', str(err))
        sys.exit(2)

    try:
        app.logger.info('Subscribing to ExecutionCommand.')
        uuid_e = subscribe(
            kind=EXECUTIONCOMMAND,
            fields={'runs-on': 'dummy'},
            target='inbox',
            app=app,
        )
    except Exception as err:
        app.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        app.logger.info('Starting dummy execution environment agent.')
        run_app(app)
    finally:
        unsubscribe(uuid_e, app=app)


if __name__ == '__main__':
    main()
