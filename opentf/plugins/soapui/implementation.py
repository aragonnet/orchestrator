# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the soapui provider.

The following categories are provided:

- execute
- params
- soapui
"""

from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
SOAPUI_CATEGORY = 'soapui'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

REPORT_TYPE = 'application/vnd.opentestfactory.soapui-surefire+xml'

SOAPUI_REPORTS = f'''
echo "::attach type={REPORT_TYPE}::`pwd`/output.xml"
echo "::attach::`pwd`/log.html"
echo "::attach::`pwd`/report.html"
'''


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: soapui/execute@v1
      with:
        test: path/to/test.xml#testsuiteName#testCaseName
    ```
    """
    test_reference = inputs['test']

    parts = test_reference.partition('#')
    datasource = parts[0]
    if parts[2].rstrip():
        parts = parts[2].partition('#')
        test_suite_arg = parts[0]
        if parts[2].rstrip():
            test_case_arg = f'-c"{parts[2]}" '
        else:
            test_case_arg = ''
    else:
        test_suite_arg = test_case_arg = ''

    steps = [
        {
            'uses': 'actions/delete-file@v1',
            'with': {'path': f'TEST-{test_suite_arg}.xml'},
        },
        {'run': 'touch dynamic_env.sh'},
        {
            'run': f'. dynamic_env.sh;testrunner.sh -j {test_case_arg}"{datasource}" '
            '|| echo SoapUI failed with rc:$?'
        },
        {'run': f'echo "::attach type={REPORT_TYPE}::`pwd`/TEST-{test_suite_arg}.xml"'},
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: soapui/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )

    path = f'{uuid4()}.ini'
    steps = [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {'run': f'echo export _SQUASH_TF_TESTCASE_PARAM_FILES={path} > dynamic_env.sh'},
    ]

    return steps


########################################################################
# 'soapui' action


def soapui_action(inputs):
    """Process 'soapui' action.

    `soapui` actions have a mandatory `project` input:

    ```yaml
    - uses: soapui/soapui@v1
      with:
        project: foo
    ```

    ```yaml
    - uses: soapui/soapui@v1
      with:
        project: foo
        testcase: bar
        testsuite: baz
        user: user
        host: host:port
        endpoint: https://host:port/foo
        properties:
          PROPERTY1: value1
          PROPERTY2: value2
        format: PDF
        type: JUnit-Style HTML Report
        target: foo/bar
    ```

    If the action is used more than once in a job, it is up to the
    caller to ensure no previous test execution results remains before
    executing a new test.

    It is also up to the caller to attach the relevant reports so that
    publishers can do their job too, by using the `actions/get-files@v1`
    action or some other means.
    """
    project = inputs['project']

    args = ''
    if testcase := inputs.get('testcase'):
        args += f'-c"{testcase}" '
    if testsuite := inputs.get('testsuite'):
        args += f'-s"{testsuite}" '
    if user := inputs.get('user'):
        args += f'-u{user} '
    if host := inputs.get('host'):
        args += f'-h{host} '
    if endpoint := inputs.get('endpoint'):
        args += f'-e{endpoint} '

    if properties := inputs.get('properties'):
        for prop, value in properties.items():
            args += f'-D{prop}="{value}" '

    if report_format := inputs.get('format'):
        args += f'-F{report_format} '
    if report_type := inputs.get('type'):
        args += f'-R"{report_type}" '
    if report_target := inputs.get('target'):
        args += f'-f{report_target} '

    steps = [
        {'run': 'touch dynamic_env.sh'},
        {
            'run': f'. dynamic_env.sh;testrunner.sh {args}"{project}" '
            '|| echo SoapUI failed with rc:$?'
        },
        {'run': SOAPUI_REPORTS},
    ]
    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    SOAPUI_CATEGORY: soapui_action,
}
