# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the robotframework provider.

The following categories are provided:

- execute
- params
- robot
"""

from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
ROBOT_CATEGORY = 'robot'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'
SQUASHTM_PARAM_FILE = '_SQUASH_TF_TESTCASE_PARAM_FILES'

REPORT_TYPE = 'application/vnd.opentestfactory.robotframework-output+xml'

ROBOT_XML_REPORT = 'output.xml'
ROBOT_REPORTS = ['log.html', 'report.html']
ROBOT_EXT_SCREENSHOTS = [
    '*.png',
    '*.jpg',
    '*.jpeg',
    'browser/screenshot/*.png',
    'browser/screenshot/*.jpg',
    'browser/screenshot/*.jpeg',
]
ROBOT_REPORTS_ARCHIVE = 'RobotFramework_reports.tar'
ROBOT_ATTACH_REPORTS = [
    {'uses': 'actions/get-file@v1', 'with': {'path': ROBOT_REPORTS_ARCHIVE}},
    {
        'uses': 'actions/get-file@v1',
        'with': {'path': ROBOT_XML_REPORT, 'type': REPORT_TYPE},
    },
]
ROBOT_CLEAN_REPORTS = [
    {'uses': 'actions/delete-file@v1', 'with': {'path': path}}
    for path in ROBOT_REPORTS + [ROBOT_XML_REPORT] + [ROBOT_REPORTS_ARCHIVE]
]

ROBOT_ALLUREREPORTS = ['*-result.json', '*-attachment.html']
ROBOT_ATTACH_ALLUREREPORTS = [
    {
        'uses': 'actions/get-files@v1',
        'with': {
            'pattern': '*-result.json',
            'warn-if-not-found': 'No allure-robotframework in execution host',
        },
    },
    {
        'uses': 'actions/get-files@v1',
        'with': {
            'pattern': '*-attachment.html',
            'warn-if-not-found': 'No attachment with Robot reports for Allure or no '
            'allure-robotframework in execution host',
        },
    },
]
ROBOT_CLEAN_ALLUREREPORTS = [
    {'uses': 'actions/delete-file@v1', 'with': {'path': path}}
    for path in ROBOT_ALLUREREPORTS
]

ROBOT_CLEAN_SCREENSHOTS = [
    {'uses': 'actions/delete-file@v1', 'with': {'path': path}}
    for path in ROBOT_EXT_SCREENSHOTS
]


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: robotframework/execute@v1
      with:
        test: foobar
    ```

    `test` is of the form `{datasource}[#{testcase}]`.
    """
    test_reference = inputs['test']

    parts = test_reference.partition('#')
    datasource = parts[0]
    if len(parts[2].rstrip()) > 0:
        test_case_arg = f'--test "{parts[2]}" '
    else:
        test_case_arg = ''

    robot_screenshots = []
    for item in ROBOT_EXT_SCREENSHOTS:
        robot_screenshots.append(core.normalize_path(item))

    steps = [
        *ROBOT_CLEAN_REPORTS,
        *ROBOT_CLEAN_SCREENSHOTS,
        *ROBOT_CLEAN_ALLUREREPORTS,
        {
            'run': f'robot --nostatusrc --listener "allure_robotframework;." {test_case_arg}"{datasource}"'
        },
        {
            'uses': 'actions/create-archive@v1',
            'with': {
                'path': ROBOT_REPORTS_ARCHIVE,
                'format': 'tar',
                'patterns': ROBOT_REPORTS + robot_screenshots,
                'warn-if-not-found': True,
            },
        },
        *ROBOT_ATTACH_REPORTS,
        *ROBOT_ATTACH_ALLUREREPORTS,
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: robotframework/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.',
        )

    path = f'{uuid4()}.ini'
    steps = [
        {
            'uses': 'actions/create-file@v1',
            'with': {'data': data, 'path': path, 'format': 'ini'},
        },
        {'run': core.export_variable(SQUASHTM_PARAM_FILE, path)},
    ]

    return steps


########################################################################
# 'robot' action


def robot_action(inputs):
    """Process 'robot' action.

    Run a Robot Framework test suite.

    `robot` actions have a mandatory `datasource` input.

    They also have two optional inputs:

    - `test`, which specify a test case present in the script given in
      the datasource.  By default, all test cases in the datasource
      are executed.
    - `reports_for_allure`, a boolean, that enables the generation of
      Allure reports.  By default, Allure reports are not generated.

    If you want to generate Allure reports, the `allure-robotframework`
    Python library must be installed in the execution environment.

    ## Examples

    This first example runs all tests in the `foobar` test suite:

    ```yaml
    - uses: robotframework/robot@v1
      with:
        datasource: foobar
    ```

    This second example runs the `foo` test in the `foobar` test suite,
    and an Allure report will be generated:

    ```yaml
    - uses: robotframework/robot@v1
      with:
        datasource: foobar
        test: foo
        reports_for_allure: true
    ```
    """
    datasource = inputs['datasource']

    test_case = inputs.get('test')
    allure_reporting = inputs.get('reports_for_allure', False)

    if not isinstance(allure_reporting, bool):
        core.fail('reports_for_allure must be a boolean, not a string.')

    if test_case:
        test_case_arg = f'--test "{test_case}" '
    else:
        test_case_arg = ''
    if allure_reporting:
        allure_arg = '--listener "allure_robotframework;." '
    else:
        allure_arg = ''

    robot_screenshots = []
    for item in ROBOT_EXT_SCREENSHOTS:
        robot_screenshots.append(core.normalize_path(item))

    steps = [
        *ROBOT_CLEAN_REPORTS,
        *ROBOT_CLEAN_SCREENSHOTS,
        *ROBOT_CLEAN_ALLUREREPORTS,
        {'run': f'robot --nostatusrc {allure_arg}{test_case_arg}"{datasource}"'},
        {
            'uses': 'actions/create-archive@v1',
            'with': {
                'path': ROBOT_REPORTS_ARCHIVE,
                'format': 'tar',
                'patterns': ROBOT_REPORTS + robot_screenshots,
                'warn-if-not-found': True,
            },
        },
        *ROBOT_ATTACH_REPORTS,
    ]
    if allure_reporting:
        steps.extend(ROBOT_ATTACH_ALLUREREPORTS)

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    ROBOT_CATEGORY: robot_action,
}
