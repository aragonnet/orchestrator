# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the cypress provider.

The following categories are provided:

- execute
- params
- cypress
"""

import re

from opentf.toolkit import core

########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
CYPRESS_CATEGORY = 'cypress'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

REPORT_TYPE = 'application/vnd.opentestfactory.cypress-surefire+xml'


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: cypress/execute@v1
      with:
        test: cypressProject/cypress/test.spec.js
    ```
    """
    test_reference = inputs['test']

    parts = test_reference.partition('/')
    datasource = parts[0]
    if len(parts[2].rstrip()):
        test_spec_path = f'--spec "{parts[2]}" '
    else:
        test_spec_path = ' '
    test_spec = ''
    while len(parts[2].rstrip()):
        test_spec = parts[2]
        parts = parts[2].partition('/')
    parts = test_spec.partition('.')
    test_report_path = "target/surefire-reports/" + parts[0] + "-report.xml"
    steps = [
        {'run': core.delete_file(f'{datasource}/{test_report_path}')},
        {
            'working-directory': datasource,
            'run': f'cypress run {test_spec_path} '
            f'--reporter junit --reporter-options "mochaFile={test_report_path}"',
            'continue-on-error': True,
            'variables': {'NO_COLOR': '1'},
        },
        {'run': core.attach_file(f'{datasource}/{test_report_path}', type=REPORT_TYPE)},
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: cypress/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )

    steps = []
    global_data = data.get('global', {})
    test_data = data.get('test', {})
    for key in global_data:
        if key not in test_data:
            value = re.sub(r'(?<!\\)(\\\\)*"', r'\\"', global_data[key])
            steps.append({'run': core.export_variable(f'CYPRESS_{key}', value)})
    for key in test_data:
        value = re.sub(r'(?<!\\)(\\\\)*"', r'\\"', test_data[key])
        steps.append({'run': core.export_variable(f'CYPRESS_{key}', value)})

    return steps


########################################################################
# 'cypress' action


def cypress_action(inputs):
    """Process 'cypress' action.

    ```yaml
    - uses: cypress/cypress@v1
      with:
        browser: chrome
        reporter: junit
        reporter-options: "mochaFile=mocha_results/test-output-[hash].xml,toConsole=true"
        headless: true
        env: profile=postgres
        config-file: cypress/config/...
    ```

    If the action is used more than once in a job, it is up to the
    caller to ensure no previous test execution results remains before
    executing a new test.

    It is also up to the caller to attach the relevant reports so that
    publishers can do their job too, by using the `actions/get-files@v1`
    action or some other means.
    """
    args = ''
    if inputs.get('headless'):
        args += ' --headless'
    if browser := inputs.get('browser'):
        args += f' --browser {browser}'
    if reporter := inputs.get('reporter'):
        args += f' --reporter {reporter}'
    if reporter_options := inputs.get('reporter-options'):
        args += f' --reporter-options {reporter_options}'
    if env := inputs.get('env'):
        args += f' --env {env}'
    if config_file := inputs.get('config-file'):
        args += f' --config-file {config_file}'

    steps = [
        {
            'run': f'cypress run {args}',
            'continue-on-error': True,
            'variables': {'NO_COLOR': '1'},
        }
    ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    CYPRESS_CATEGORY: cypress_action,
}
