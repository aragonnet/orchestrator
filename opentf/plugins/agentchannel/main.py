# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An agent-based channel plugin."""

from collections import defaultdict
from datetime import datetime, timedelta
from io import BytesIO
from queue import Queue
import ntpath
import os
import re
import sys
import threading

from importlib.metadata import version

from flask import request, send_file

from opentf.commons import (
    SERVICECONFIG,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    AGENTREGISTRATION,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    ConfigError,
    make_uuid,
    make_event,
)

from opentf.toolkit import core
from opentf.toolkit.channels import (
    make_script,
    ADDMASK_COMMAND,
    ATTACH_COMMAND,
    DEBUG_COMMAND,
    ERROR_COMMAND,
    SETOUTPUT_COMMAND,
    STOPCOMMANDS_COMMAND,
    WARNING_COMMAND,
    DEFAULT_CHANNEL_LEASE,
    PUT_FILE_COMMAND,
    RUNNER_OS,
    SCRIPTFILE_DEFAULT,
    SCRIPTPATH_DEFAULT,
    SHELL_DEFAULT,
    SHELL_TEMPLATE,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/agentchannel.yaml'

W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green
O = '\033[33m'  # orange
B = '\033[34m'  # blue
P = '\033[35m'  # purple

CHANNEL_REQUEST = -1
CHANNEL_RELEASE = -2
CHANNEL_NOTIFY = -3

LIVENESS_LIMIT = 300  # in seconds

CHUNK_SIZE = 4096

########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args):
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Job Helpers

AGENTS = {}
AGENTS_LEASES = {}
AGENTS_SEMAPHORES = {}

STEPS_QUEUES = defaultdict(Queue)
JOBS = {}
STEPS = {}
JOBS_AGENTS = {}
CHANNELS_AGENTS = {}
JOBS_MASKS = defaultdict(list)

LOCALSTORE = defaultdict(dict)
PENDINGSTORE = defaultdict(dict)

REQUESTS_QUEUE = Queue()

DEFERRED_RESULT = {}
DEFERRED_PROCESSING_SENTINEL = '__DEFERRED_PROCESSING_SENTINEL__'


# Steps helpers


def list_job_steps(job_id):
    """List all steps for job."""
    if job_id not in JOBS:
        JOBS[job_id] = {}
    return JOBS[job_id]


def add_job_step(job_id, command):
    """Adding new step to existing job."""
    JOBS[job_id][command['metadata']['step_sequence_id']] = command


def get_os(agent_id):
    """Get agent os."""
    return (set(AGENTS[agent_id]['spec']['tags']) & RUNNER_OS).pop()


# Job agent helpers


def get_job_agent(job_id, channel_id):
    """Get agent_id for job.

    # Required parameters

    - job_id: a string
    - channel_id: a string

    # Returned value

    An _agent ID_ or None.  An agent_id is a string.
    """
    if agent_id := JOBS_AGENTS.get(job_id):
        return agent_id

    for candidate in list(CHANNELS_AGENTS[channel_id]):
        lease = AGENTS_LEASES.get(candidate)
        if lease and lease >= datetime.now():
            try:
                AGENTS[candidate]['status']['currentJobID'] = job_id
                JOBS_AGENTS[job_id] = candidate
                return candidate
            except KeyError:
                debug(f'Candidate agent {candidate} no longer exists, ignoring.')

    return None


def get_matching_agents(tags):
    """List all agents capable of handling tags.

    # Required parameters

    - tags: a collection of strings

    # Returned value

    A possibly empty list of _agentids_.  An agent_id is a string.
    """
    requiredtags = set(tags)
    matching = []
    for agent_id, body in AGENTS.items():
        if requiredtags.issubset(set(body['spec']['tags'])):
            matching.append(agent_id)
    if not matching:
        warning(f'Could not find environment matching request {tags}.')
    return matching


def touch_agent(agent_id):
    """Refresh agent activity status."""
    if agent_id in AGENTS:
        AGENTS[agent_id]['status'][
            'lastCommunicationTimestamp'
        ] = datetime.now().isoformat()


def is_alive(agent_id):
    """Check if agent has shown recent activity."""
    if agent_id not in AGENTS:
        return False

    limit = (
        datetime.now()
        - timedelta(
            seconds=AGENTS[agent_id]['spec'].get('liveness_probe', LIVENESS_LIMIT)
        )
    ).isoformat()
    if AGENTS[agent_id]['status']['lastCommunicationTimestamp'] >= limit:
        return True
    return False


def is_available(agent_id):
    """Check if agent has a lease or is busy handling a job."""
    if agent_id in JOBS_AGENTS.values():
        return False
    if lease := AGENTS_LEASES.get(agent_id):
        return lease < datetime.now()
    return True


def make_offers(metadata, candidates):
    """Make at least one offer.

    Currently makes exactly one offer, assuming the tags are valid.
    This may change at any time.

    # Required parameters

    - metadata: a dictionary
    - candidates: a non-empty list of strings

    # Returned value

    None.
    """
    for agent_id in candidates:
        if not is_available(agent_id):
            continue
        if not is_alive(agent_id):
            continue
        channel_id = metadata['channel_id'] = make_uuid()
        metadata['channel_tags'] = AGENTS[agent_id]['spec']['tags']
        metadata['channel_os'] = get_os(agent_id)
        metadata['channel_temp'] = AGENTS[agent_id].get(
            'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
        )
        metadata['channel_lease'] = DEFAULT_CHANNEL_LEASE
        AGENTS_LEASES[agent_id] = datetime.now() + timedelta(
            seconds=DEFAULT_CHANNEL_LEASE
        )
        CHANNELS_AGENTS[channel_id] = [agent_id]
        prepare = make_event(
            EXECUTIONRESULT,
            metadata=metadata,
            status=0,
        )
        publish(prepare, context=app.config['CONTEXT'])
        break
    else:
        warning(
            f'Could not find valid candidate for job {metadata["job_id"]}: compatible agents are either busy or unreachable.'
        )


# os helpers


def _publish_preparedresult(agent_id):
    """Publish prepared result if all deferred actions done."""
    if not DEFERRED_RESULT.get(agent_id):
        return

    if not LOCALSTORE.get(agent_id) and not PENDINGSTORE.get(agent_id):
        publish(DEFERRED_RESULT[agent_id], context=app.config['CONTEXT'])
        try:
            del DEFERRED_RESULT[agent_id]
        except KeyError:
            warning(f'Could not remove prepared result for agent {agent_id}.')


def prepare_command(command):
    """Prepare command.

    Prepare command for execution via an agent.

    Returns True if the command was prepared, False if something was not
    right (lease expired, ...).
    """
    metadata = command['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']
    channel_id = metadata['channel_id']

    if isinstance(CHANNELS_AGENTS[channel_id], list):
        # No agent set for channel yet
        if (agent_id := get_job_agent(job_id, channel_id)) is None:
            return False
        CHANNELS_AGENTS[channel_id] = agent_id
        AGENTS_SEMAPHORES[agent_id] = threading.Semaphore(0)
        release = False
    else:
        agent_id = CHANNELS_AGENTS[channel_id]
        release = True

    runner_os = get_os(agent_id)
    root = AGENTS[agent_id]['spec'].get('script_path', SCRIPTPATH_DEFAULT[runner_os])
    script_file = SCRIPTFILE_DEFAULT[runner_os].format(
        root=root, job_id=job_id, step_sequence_id=step_sequence_id
    )
    script = make_script(command, job_id, runner_os, root)
    shell = SHELL_TEMPLATE[command.get('shell') or SHELL_DEFAULT[runner_os]]

    STEPS[agent_id] = command

    script_uuid = make_uuid()
    LOCALSTORE[agent_id][script_uuid] = {'content': script, 'filename': script_file}
    STEPS_QUEUES[agent_id].put(
        {'kind': 'put', 'path': script_file, 'file_id': script_uuid, 'root': ''}
    )
    STEPS_QUEUES[agent_id].put({'kind': 'exec', 'command': shell.format(script_file)})
    if release:
        AGENTS_SEMAPHORES[agent_id].release()
    return True


def release_resources(agent_id, job_id, channel_id):
    """Release resources after teardown."""
    try:
        del STEPS_QUEUES[agent_id]
    except KeyError:
        pass
    try:
        del PENDINGSTORE[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS_LEASES[agent_id]
    except KeyError:
        pass
    try:
        del JOBS_AGENTS[job_id]
    except KeyError:
        pass
    try:
        del JOBS[job_id]
    except KeyError:
        pass
    try:
        del JOBS_MASKS[job_id]
    except KeyError:
        pass
    try:
        del CHANNELS_AGENTS[channel_id]
    except KeyError:
        pass
    try:
        del AGENTS_SEMAPHORES[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS[agent_id]['status']['currentJobID']
    except KeyError:
        pass


def mask(line, job_id):
    """Remove masked values."""
    for masked in JOBS_MASKS[job_id]:
        line = line.replace(masked, '***')
    return line


def prepare_result(body, agent_id):
    """Prepare result.

    Returns True if the execution result has been sent, False if it is
    deferred, awaiting attachments.
    """
    attachments = []
    attachments_metadata = {}
    outputs = {}
    logs = []
    stop_command = None

    is_windows = get_os(agent_id) == 'windows'
    separator = '\\' if is_windows else '/'
    job_id, step_sequence_id = (
        STEPS[agent_id]['metadata']['job_id'],
        STEPS[agent_id]['metadata']['step_sequence_id'],
    )
    stdout, stderr, resp = body['stdout'], body['stderr'], body['exit_status']
    if step_sequence_id == CHANNEL_RELEASE and is_windows:
        resp = 0  # windows returns 1 as we remove the running script

    stdout_content = list(stdout)
    for line in stdout_content:
        # Parsing stdout for workflow commands
        if stop_command is not None:
            if line == stop_command:
                stop_command = None
            else:
                logs.append(mask(line, job_id))
            continue

        if wcmd := re.match(ATTACH_COMMAND, line):
            remote = wcmd.group(2)
            if is_windows:
                remote = ntpath.normpath(remote)
            try:
                local_path = (
                    f'/tmp/{job_id}_{step_sequence_id}_{remote.split(separator)[-1]}'
                )
                attachments.append(local_path)
                attachments_metadata[local_path] = {'uuid': make_uuid()}
                if wcmd.group(1):
                    for parameter in wcmd.group(1).strip().split(','):
                        if '=' not in parameter:
                            # publish executionerror
                            error(f'Invalid workflow command parameter: {parameter}.')
                        key, _, value = parameter.strip().partition('=')
                        attachments_metadata[local_path][key] = value
                file_uuid = make_uuid()
                PENDINGSTORE[agent_id][file_uuid] = local_path
                STEPS_QUEUES[agent_id].put(
                    {'kind': 'get', 'path': remote, 'file_id': file_uuid}
                )
            except Exception as err:
                # publish executionerror
                error(f'Could not read {remote}: {err}.')
        elif wcmd := re.match(PUT_FILE_COMMAND, line):
            data = wcmd.group(1)
            remote_path = wcmd.group(2)
            working_directory_path = core.join_path(
                job_id, STEPS[agent_id].get('working-directory'), is_windows
            )
            targeted_remote_path = core.join_path(
                working_directory_path, remote_path, is_windows
            )

            try:
                file_uuid = make_uuid()
                file_ = '/tmp/in_{uuid}_{name}'.format(
                    uuid=STEPS[agent_id]['metadata']['workflow_id'], name=data
                )
                if not os.path.exists(file_):
                    # publish execeptionerror
                    error(f'Invalid resources.files reference {data}.')
                    resp = 2
                else:
                    LOCALSTORE[agent_id][file_uuid] = {
                        'filepath': file_,
                        'filename': data,
                    }
                    STEPS_QUEUES[agent_id].put(
                        {
                            'kind': 'put',
                            'path': targeted_remote_path,
                            'file_id': file_uuid,
                            'root': job_id,
                        }
                    )
            except Exception as err:
                # publish executionerror
                error(
                    f'Could not send file {data} to remote path {remote_path}: {err}.'
                )
                resp = 2
        elif wcmd := re.match(SETOUTPUT_COMMAND, line):
            outputs[wcmd.group(1)] = wcmd.group(2)
        elif wcmd := re.match(DEBUG_COMMAND, line):
            logs.append(f'DEBUG,{mask(wcmd.group(1), job_id)}')
        elif wcmd := re.match(WARNING_COMMAND, line):
            logs.append(f'WARNING,{mask(wcmd.group(3), job_id)}')
        elif wcmd := re.match(ERROR_COMMAND, line):
            logs.append(f'ERROR,{mask(wcmd.group(3), job_id)}')
        elif wcmd := re.match(STOPCOMMANDS_COMMAND, line):
            stop_command = f'::{wcmd.group(1)}::'
        elif wcmd := re.match(ADDMASK_COMMAND, line):
            JOBS_MASKS[job_id].append(wcmd.group(1))
        else:
            logs.append(mask(line, job_id))

    stderr_content = list(stderr)
    for line in stderr_content:
        logs.append(mask(line, job_id))

    stdout_, stderr_ = '\n'.join(stdout_content), '\n'.join(stderr_content)

    print(
        f'Job {B}{job_id}{W} step {B}{step_sequence_id}{W}\n'
        + f'{G}[{stdout_}]{W}\n{R}[{stderr_}]{W}\n{P}(RC={resp}){W}'
    )
    prepare = make_event(
        EXECUTIONRESULT,
        metadata=STEPS[agent_id]['metadata'],
        status=resp,
    )
    if outputs:
        prepare['outputs'] = outputs
    if logs:
        prepare['logs'] = logs

    if attachments:
        prepare['attachments'] = attachments
        prepare['metadata']['attachments'] = attachments_metadata

    if attachments or LOCALSTORE[agent_id]:
        STEPS_QUEUES[agent_id].put(DEFERRED_PROCESSING_SENTINEL)
        DEFERRED_RESULT[agent_id] = prepare
        return False

    publish(prepare, context=app.config['CONTEXT'])

    if step_sequence_id == CHANNEL_RELEASE:
        release_resources(agent_id, job_id, agent_id)

    return True


def prepare_offers():
    """Prepare offers.

    Offers are serialized so that no race condition occurs and so that
    a 'fair' allocation strategy can be defined.
    """
    while True:
        try:
            metadata, candidates = REQUESTS_QUEUE.get()
            make_offers(metadata, candidates)
        except Exception as err:
            error(f'Internal error while making offer: {err}.')


########################################################################
## Main

try:
    app = make_app(
        name='agentchannel',
        description='Create and start an agent-based channel plugin.',
        configfile=CONFIG_FILE,
        schema=SERVICECONFIG,
    )
except ConfigError:
    sys.exit(2)


# Channel plugin entrypoint


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json() or {}
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(EXECUTIONCOMMAND, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid ExecutionCommand request: {extra}.'
        )

    metadata = body['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id == CHANNEL_REQUEST:
        # A new job, can we make an offer?
        requiredtags = body['runs-on']
        if not isinstance(requiredtags, list):
            requiredtags = [requiredtags]
        candidates = get_matching_agents(requiredtags)
        if not candidates:
            return make_status_response(
                'OK', f'Not providing execution environment matching {requiredtags}.'
            )

        REQUESTS_QUEUE.put((metadata, candidates))
        return make_status_response('OK', f'Making an offer for job {job_id}')

    if metadata.get('channel_id') not in CHANNELS_AGENTS:
        return make_status_response(
            'OK', f'Job {job_id} not handled by this channel plugin.'
        )

    steps = list_job_steps(job_id)
    if step_sequence_id in steps:
        return make_status_response(
            'AlreadyExists',
            f'Step {step_sequence_id} already received for job {job_id}.',
        )

    if not prepare_command(body):
        return make_status_response('OK', f'Lease expired for job {job_id}.')

    return make_status_response(
        'OK', f'Step #{step_sequence_id} added to job {job_id} queue.'
    )


# Agents registration


@app.route('/agents', methods=['POST'])
def register_agent():
    """Register a new agent.

    # Registration format

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: AgentRegistration
    metadata:
      name: {name}
    spec:
      tags:
      - {tag1}
      - ...
      script_path: {path}  # not required
    ```

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the registration is successful (`.status` is `'Success'`),
    `.details.uuid` is the agent ID.
    """
    if not request.is_json:
        return make_status_response('BadRequest', 'Not a JSON document.')

    try:
        body = request.get_json() or {}
    except Exception:
        return make_status_response('BadRequest', 'Not a valid JSON document.')

    valid, extra = validate_schema(AGENTREGISTRATION, body)
    if not valid:
        return make_status_response(
            'Invalid',
            'Not a valid AgentRegistration manifest.',
            details={'error': str(extra)},
        )

    body['metadata']['creationTimestamp'] = datetime.now().isoformat()
    body['status'] = {
        'communicationCount': 0,
        'lastCommunicationTimestamp': datetime.now().isoformat(),
        'communicationStatusSummary': defaultdict(int),
    }
    sid = make_uuid()
    AGENTS[sid] = body
    msg = f'Agent {body["metadata"]["name"]} successfully registered (id={sid}, tags={body["spec"]["tags"]}).'
    info(msg)
    return make_status_response(
        'Created', msg, details={'uuid': sid, 'version': version('opentf-orchestrator')}
    )


@app.route('/agents', methods=['GET'])
def list_agents():
    """Return list of agents.

    # Returned value

    A _list_.  A list is a dictionary with the following entries:

    - kind: a string
    - apiVersion: a string (`'v1'`)
    - items: a list of dictionaries (`what`)
    """
    return {'apiVersion': 'v1', 'kind': 'AgentRegistrationsList', 'items': AGENTS}


@app.route('/agents/<agent_id>', methods=['DELETE'])
def cancel_agent(agent_id):
    """Deregister agent.

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the deregistration is successful, `.status` is `'Success'`.  If
    `agent_id` is not known, `.status` is `'Failure'` and `reason` is
    `'NotFound'`.
    """
    if agent_id not in AGENTS:
        return make_status_response('NotFound', f'Agent {agent_id} not known.')
    try:
        del AGENTS[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS_LEASES[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS_SEMAPHORES[agent_id]
    except KeyError:
        pass
    try:
        del DEFERRED_RESULT[agent_id]
    except KeyError:
        pass
    return make_status_response('OK', f'Agent {agent_id} canceled.')


# Agents endpoints


@app.route('/agents/<agent_id>', methods=['GET'])
def get_command(agent_id):
    """Get command, if any.

    # Required parameter

    - agent_id: a string (a registered agent id)

    # Returned value

    A _status_.  `NotFound` if the agent id is not known.  `NoContent` if the
    agent id is known but there is no pending commands for it.  `OK` if at
    least one command is available.

    If `OK`, `details` contains the command to process.  The command is either
    an `execute` command, a `get` command, or a `put` command.
    """
    if agent_id not in AGENTS:
        return make_status_response('NotFound', f'Unknown agent_id {agent_id}.')

    touch_agent(agent_id)
    if STEPS_QUEUES[agent_id].empty():
        return make_status_response(
            'NoContent', 'Nothing to do, please try again later.'
        )

    command = STEPS_QUEUES[agent_id].get()
    if command == DEFERRED_PROCESSING_SENTINEL:
        _publish_preparedresult(agent_id)
        return get_command(agent_id)

    return make_status_response('OK', 'Command available.', details=command)


@app.route('/agents/<agent_id>', methods=['POST'])
def set_execution_result(agent_id):
    """Handling execution result.

    If there are attachments, defer publishing until they are all
    received.

    The request body is a JSON object.

    If no execution error occurred, it contains the following fields:

    - `stdout`: a possibly empty list of strings
    - `stderr`: a possibly empty list of strings
    - `exit_status`: an integer

    If an execution error occurred, it contains the following field:

    - `details`: a dictionary

    # Required parameters

    - agent_id: a string (a registered agent id)

    # Returned value

    A _status_.
    """
    if agent_id not in AGENTS:
        return make_status_response('NotFound', f'Agent {agent_id} not found.')

    touch_agent(agent_id)
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('Invalid', 'Body is not an object.')

    if 'details' in body:
        # something went wrong while processing command or its side effects
        # (attachments & put-files deferred processing)
        if prepare := DEFERRED_RESULT.get(agent_id):
            prepare['status'] = 2
            prepare.setdefault('logs', [])
            prepare['logs'].append(f'ERROR,{body["details"].get("error")}')
            try:
                del DEFERRED_RESULT[agent_id]
            except KeyError:
                pass
            try:
                del PENDINGSTORE[agent_id]
            except KeyError:
                pass
            try:
                del LOCALSTORE[agent_id]
            except KeyError:
                pass
        else:
            prepare = make_event(
                EXECUTIONERROR,
                metadata=STEPS[agent_id]['metadata'],
                details=body['details'],
            )
        publish(prepare, context=app.config['CONTEXT'])
        return make_status_response('OK', 'Propagating execution error.')

    try:
        sent = prepare_result(body, agent_id)
        if sent and STEPS_QUEUES[agent_id].empty():
            if sem := AGENTS_SEMAPHORES.get(agent_id):
                sem.acquire(timeout=30)
        return make_status_response(
            'OK', 'Awaiting attachments.' if not sent else 'Results published.'
        )
    except Exception as err:
        prepare = make_event(
            EXECUTIONERROR,
            metadata=STEPS[agent_id]['metadata'],
            details={'error': str(err)},
        )
        publish(prepare, context=app.config['CONTEXT'])
        return make_status_response(
            'Invalid', f'An exception occurred while handling result: {err}.'
        )


@app.route('/agents/<agent_id>/files/<file_id>', methods=['GET'])
def get_file(agent_id, file_id):
    """Return file content.

    Used by agents to copy orchestrator-side files to their local
    execution environment.

    Will publish ExecutionResult if all pending attachments have been
    received.

    # Required parameters

    - agent_id: a string
    - file_id: a string

    # Returned value

    A _Response_ object representing a _status_ or a file.  If `code` is
    not 200, a status.  A file otherwise.
    """
    touch_agent(agent_id)
    if agent_id not in LOCALSTORE:
        return make_status_response('NotFound', f'Unknown agent_id {agent_id}.')
    if file_id not in LOCALSTORE[agent_id]:
        return make_status_response(
            'NotFound', f'File {file_id} not found for agent {agent_id}.'
        )

    if 'content' in LOCALSTORE[agent_id][file_id]:
        try:
            what = send_file(
                BytesIO(LOCALSTORE[agent_id][file_id]['content'].encode()),
                download_name=LOCALSTORE[agent_id][file_id]['filename'],
                mimetype='application/octet-stream',
            )
        except OSError as err:
            msg = f'Could not deliver file {file_id}: {err}.'
            error(msg)
            what = make_status_response('InternalError', msg)
    elif 'filepath' in LOCALSTORE[agent_id][file_id]:
        try:
            what = send_file(
                LOCALSTORE[agent_id][file_id]['filepath'],
                download_name=LOCALSTORE[agent_id][file_id]['filename'],
                mimetype='application/octet-stream',
            )
        except OSError as err:
            msg = f'Could not deliver file {file_id}: {err}.'
            error(msg)
            what = make_status_response('InternalError', msg)
    else:
        msg = 'Invalid data in LOCALSTORE'
        warning(msg)
        what = make_status_response('InternalError', msg)
    try:
        del LOCALSTORE[agent_id][file_id]
    except KeyError:
        warning(f'Could not remove entry {file_id} from local store.')

    return what


@app.route('/agents/<agent_id>/files/<file_id>', methods=['PUT', 'POST'])
def add_file(agent_id, file_id):
    """Add file content.

    Used by agents to copy local execution environment files to the
    orchestrator side, so that plugins can process them.

    Will publish ExecutionResult if all pending attachments have been
    received.

    # Required parameters

    - agent_id: a string
    - file_id: a string

    # Returned value

    A _status_.  A status is a dictionary.

    - `OK` if the file was properly received ;
    - `NotFound` if `agent_id` is not expecting files or `file_id` is
      not expected ;
    - `BadRequest` if something went wrong while receiving file.
    """
    touch_agent(agent_id)
    if agent_id not in PENDINGSTORE:
        return make_status_response('NotFound', f'Unknown agent_id {agent_id}.')
    if file_id not in PENDINGSTORE[agent_id]:
        return make_status_response(
            'NotFound', f'File {file_id} not found for agent {agent_id}.'
        )
    try:
        local_path = PENDINGSTORE[agent_id][file_id]
        with open(local_path, 'wb') as out:
            while True:
                chunk = request.stream.read(CHUNK_SIZE)
                if len(chunk) == 0:
                    break
                out.write(chunk)

        try:
            del PENDINGSTORE[agent_id][file_id]
        except KeyError:
            warning(f'Could not remove entry {file_id} from pending store.')

    except Exception as err:
        return make_status_response(
            'BadRequest', f'Failed to get file {file_id}: {err}.'
        )

    return make_status_response('OK', f'Content received for {file_id}.')


# Main


def main():
    """Start the agent-based channel plugin."""
    try:
        info('Starting offers handling thread.')
        threading.Thread(target=prepare_offers, daemon=True).start()
    except Exception as err:
        error('Could not start offers handling thread: %s.', str(err))

    try:
        info('Subscribing to ExecutionCommand.')
        sub = subscribe(kind=EXECUTIONCOMMAND, target='inbox', app=app)
    except Exception as err:
        error('Could not subscribe to eventbus: %s.', str(err))
        sys.exit(2)

    try:
        info('Starting agent-based channel plugin.')
        run_app(app)
    finally:
        info('Unsubscribing from ExecutionCommand.')
        unsubscribe(sub, app=app)


if __name__ == '__main__':
    main()
