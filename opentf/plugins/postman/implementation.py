# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the postman provider.

The following categories are provided:

- execute
- params
- postman
"""

import json
import os
import sys

from datetime import datetime
from uuid import uuid4

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
POSTMAN_CATEGORY = 'postman'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

GLOBAL_PARAMS_FILE_PATH = '_opentf_global_params.json'
ENVIRONMENT_PARAMS_FILE_PATH = '_opentf_environment_params.json'

EMPTY_GLOBAL_PARAMS_FILE_PATH = '_empty_opentf_global_params.json'
EMPTY_ENVIRONMENT_PARAMS_FILE_PATH = '_empty_opentf_environment_params.json'

PARAMS_TEMPLATE = {
    "id": None,
    "name": None,
    "values": [],
    "_postman_variable_scope": None,
    "_postman_exported_at": None,
    "_postman_exported_using": "Postman/8.12.1",
}

POSTMAN_XML_REPORT = 'newman-run-report.xml'
POSTMAN_HTML_REPORT = 'newman-run-report.html'

REPORT_TYPE = 'application/vnd.opentestfactory.postman-surefire+xml'

########################################################################
## Local Toolkit


def make_global_object():
    """Make a global template to create a globals json file"""
    result = PARAMS_TEMPLATE.copy()
    result['id'] = str(uuid4())
    result['_postman_exported_at'] = datetime.now().isoformat()
    result['_postman_variable_scope'] = 'globals'
    result['name'] = 'GlobalsOpenTestFactoryCollection'
    return result


def make_environment_object():
    """Make an environment template to create an environment json file"""
    result = PARAMS_TEMPLATE.copy()
    result['id'] = str(uuid4())
    result['_postman_exported_at'] = datetime.now().isoformat()
    result['_postman_variable_scope'] = 'environment'
    result['name'] = 'EnvironmentOpenTestFactoryCollection'
    return result


def exit_on_none_existing_file(path: str) -> str:
    """Check existing file"""
    path = core.normalize_path(path)
    if core.runner_on_windows():
        ret = f'@if not exist {path} @exit 1'
    else:
        ret = f'[ -f {path} ] && echo "{path} file exists."'
    return ret


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: postman/execute@v1
      with:
        test: path/to/collection#folderName#requestName
    ```
    """
    core.debug('Identified as the "execute" action')

    test_reference = inputs['test']

    parts = test_reference.partition('#')
    datasource = parts[0]
    if parts[2].rstrip():
        parts = parts[2].partition('#')
        folder_name_arg = f'--folder "{parts[0]}" --bail folder '
        if parts[2].rstrip():
            request_name_arg = parts[2]
        else:
            request_name_arg = ''
    else:
        folder_name_arg = request_name_arg = ''

    if core.runner_on_windows():
        rename_global_params_file = f'if not exist {GLOBAL_PARAMS_FILE_PATH} @ren {EMPTY_GLOBAL_PARAMS_FILE_PATH} {GLOBAL_PARAMS_FILE_PATH}'
        rename_environment_params_file = f'if not exist {ENVIRONMENT_PARAMS_FILE_PATH} @ren {EMPTY_ENVIRONMENT_PARAMS_FILE_PATH} {ENVIRONMENT_PARAMS_FILE_PATH}'
    else:
        rename_global_params_file = f'if [ ! -f {GLOBAL_PARAMS_FILE_PATH} ]; then mv {EMPTY_GLOBAL_PARAMS_FILE_PATH} {GLOBAL_PARAMS_FILE_PATH}; fi'
        rename_environment_params_file = f'if [ ! -f {ENVIRONMENT_PARAMS_FILE_PATH} ]; then mv {EMPTY_ENVIRONMENT_PARAMS_FILE_PATH} {ENVIRONMENT_PARAMS_FILE_PATH}; fi'

    steps = [
        {'run': core.delete_file(f'{POSTMAN_XML_REPORT}')},
        {'run': core.delete_file(f'{POSTMAN_HTML_REPORT}')},
        {
            'uses': 'actions/create-file@v1',
            'with': {
                'data': make_global_object(),
                'path': EMPTY_GLOBAL_PARAMS_FILE_PATH,
                'format': 'json',
            },
        },
        {'run': f'{rename_global_params_file}'},
        {
            'uses': 'actions/create-file@v1',
            'with': {
                'data': make_environment_object(),
                'path': EMPTY_ENVIRONMENT_PARAMS_FILE_PATH,
                'format': 'json',
            },
        },
        {'run': f'{rename_environment_params_file}'},
        {
            'run': f'newman run "{datasource}" {folder_name_arg} -g {GLOBAL_PARAMS_FILE_PATH} -e {ENVIRONMENT_PARAMS_FILE_PATH} --reporters junit,html --reporter-junit-export {POSTMAN_XML_REPORT} --reporter-html-export {POSTMAN_HTML_REPORT}',
            'continue-on-error': True,
        },
        {'run': core.delete_file(f'{EMPTY_GLOBAL_PARAMS_FILE_PATH}')},
        {'run': core.delete_file(f'{EMPTY_ENVIRONMENT_PARAMS_FILE_PATH}')},
        {'run': core.delete_file(f'{GLOBAL_PARAMS_FILE_PATH}')},
        {'run': core.delete_file(f'{ENVIRONMENT_PARAMS_FILE_PATH}')},
        {'run': exit_on_none_existing_file(f'{POSTMAN_XML_REPORT}')},
        {'run': core.attach_file(f'{POSTMAN_XML_REPORT}', type=REPORT_TYPE)},
        {'run': core.attach_file(f'{POSTMAN_HTML_REPORT}')},
    ]
    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: postman/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.debug('Identified as the "param" action')

    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )

    globals = make_global_object()
    if data.get('global'):
        globals['values'] = [
            {"key": key, "value": str(value), "enabled": True}
            for key, value in data.get('global', {}).items()
        ]

    environment = make_environment_object()
    if data.get('test'):
        environment['values'] = [
            {"key": key, "value": str(value), "enabled": True}
            for key, value in data.get('test', {}).items()
        ]

    if core.runner_on_windows():
        delete_global_param_file = (
            f'if exist {GLOBAL_PARAMS_FILE_PATH} @del {GLOBAL_PARAMS_FILE_PATH}'
        )
        delete_environment_param_file = f'if exist {ENVIRONMENT_PARAMS_FILE_PATH} @del {ENVIRONMENT_PARAMS_FILE_PATH}'
    else:
        delete_global_param_file = f'if [ -f {GLOBAL_PARAMS_FILE_PATH} ]; then rm {GLOBAL_PARAMS_FILE_PATH}; fi'
        delete_environment_param_file = f'if [ -f {ENVIRONMENT_PARAMS_FILE_PATH} ]; then rm {ENVIRONMENT_PARAMS_FILE_PATH}; fi'

    steps = [
        {'run': f'{delete_global_param_file}'},
        {'run': f'{delete_environment_param_file}'},
        {
            'uses': 'actions/create-file@v1',
            'with': {
                'data': globals,
                'path': GLOBAL_PARAMS_FILE_PATH,
                'format': 'json',
            },
        },
        {
            'uses': 'actions/create-file@v1',
            'with': {
                'data': environment,
                'path': ENVIRONMENT_PARAMS_FILE_PATH,
                'format': 'json',
            },
        },
    ]
    return steps


########################################################################
# 'postman' action


def postman_action(inputs):
    """Process 'postman' action.

    `postman` actions have a mandatory `collection` input:

    ```yaml
    - uses: postman/postman@v1
      with:
        collection: path/to/collection (JSON_file|URL)
    ```

    ```yaml
    - uses: postman/postman@v1
      with:
        collection: path/to/collection (JSON_file|URL)
        folder: folderName
        environment: path/to/Postman_environment (JSON_file|URL)
        iteration-data: path/to/Data_file (JSON|CSV)
        globals: path/to/Postman_globals_file (JSON)
        iteration-count: value1 (number)
        delay-request: value2 (number in ms)
        timeout-request: value3 (number in ms)
        bail: (folder|failure|true)
    ```

    If the action is used more than once in a job, it is up to the
    caller to ensure no previous test execution results remains before
    executing a new test.

    It is also up to the caller to attach the relevant reports so that
    publishers can do their job too, by using the `actions/get-files@v1`
    action or some other means.
    """
    core.debug('Identified as the "postman" action')

    collection = inputs['collection']

    args = ''
    if folder := inputs.get('folder'):
        args += f'--folder "{folder}" '
    if environment := inputs.get('environment'):
        args += f'-e "{environment}" '
    if iteration_data := inputs.get('iteration-data'):
        args += f'-d "{iteration_data}" '
    if globals_vars := inputs.get('globals'):
        args += f'-g "{globals_vars}" '
    if iteration_count := inputs.get('iteration-count'):
        args += f'-n {iteration_count} '
    if delay_request := inputs.get('delay-request'):
        args += f'--delay-request {delay_request} '
    if timeout_request := inputs.get('timeout-request'):
        args += f'--timeout-request {timeout_request} '
    if bail := inputs.get('bail'):
        if bail == 'folder':
            args += f'--bail {bail} '
        elif bail == 'failure':
            args += f'--bail {bail} '
        else:
            args += '--bail '
    args += f'--reporters junit,html --reporter-junit-export {POSTMAN_XML_REPORT} --reporter-html-export {POSTMAN_HTML_REPORT} '

    steps = [
        {'run': f'newman run "{collection}" {args}', 'continue-on-error': True},
        {'run': exit_on_none_existing_file(f'{POSTMAN_XML_REPORT}')},
        {'run': core.attach_file(f'{POSTMAN_XML_REPORT}', type=REPORT_TYPE)},
        {'run': core.attach_file(f'{POSTMAN_HTML_REPORT}')},
    ]
    return steps


########################################################################
# known categories


KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    POSTMAN_CATEGORY: postman_action,
}
