# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An inception execution environment plugin."""

import sys

from flask import request

from opentf.commons import (
    SERVICECONFIG,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    ConfigError,
    make_uuid,
    make_event,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/inceptionee.yaml'

W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green
O = '\033[33m'  # orange
B = '\033[34m'  # blue
P = '\033[35m'  # purple

SETUP_JOB = -1
TEARDOWN_JOB = -2
NOTIFY_JOB = -3

DEFAULT_CHANNEL_LEASE = 60  # how long to keep the offer, in seconds


########################################################################
## Helpers

CHANNELS = {}
FILES = {}
UUIDS = {}


########################################################################
## Main


try:
    app = make_app(
        name='inceptionee',
        description='Create and start an inception execution env plugin.',
        configfile=CONFIG_FILE,
        schema=SERVICECONFIG,
    )
except ConfigError:
    sys.exit(2)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(EXECUTIONCOMMAND, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid ExecutionCommand request: {extra}.'
        )

    metadata = body['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id == SETUP_JOB:
        # A new job, can we make an offer?
        requiredtags = body['runs-on']
        if not isinstance(requiredtags, list):
            requiredtags = [requiredtags]
        if 'inception' not in requiredtags:
            return make_status_response(
                'OK', f'Not providing execution environment matching {requiredtags}.'
            )

        channel_id = metadata['channel_id'] = make_uuid()
        metadata['channel_tags'] = requiredtags
        metadata['channel_os'] = 'windows' if 'windows' in requiredtags else 'linux'
        metadata['channel_temp'] = '/tmp'
        metadata['channel_lease'] = DEFAULT_CHANNEL_LEASE
        CHANNELS[channel_id] = body['metadata']['workflow_id']
        FILES.setdefault(body['metadata']['workflow_id'], {})
        UUIDS.setdefault(body['metadata']['workflow_id'], {})
        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
        publish(prepare, context=app.config['CONTEXT'])
        return make_status_response('OK', f'Making an offer for job {job_id}')

    if metadata.get('channel_id') in CHANNELS:
        workflow_id = body['metadata']['workflow_id']
        attachments = []
        attachments_metadata = {}
        if 'working-directory' in body:
            app.logger.info(f'>>> [working-directory: {body["working-directory"]}]')
        for item in body['scripts']:
            app.logger.info(f'>>> {item}')
            if item.startswith('::inception::'):
                _, _, wid, name, target = item.split('::')
                if wid != workflow_id:
                    app.logger.warning('Invalid workflow_id in inception command')
                    continue
                FILES[wid][name] = target
                UUIDS[wid][name] = make_uuid()
                app.logger.info(f'{name} added to cache as {target}')
            elif '::attach' in item:
                while '::attach' in item:
                    _, _, item = item.partition('::attach')
                    type_, _, item = item.partition('::')
                    file, _, item = item.partition('\n')
                    file = file.strip('"').rsplit('/', 1)[-1]
                    if '\\' in file:
                        file = file.rsplit('\\', 1)[-1]
                    if file in FILES[workflow_id]:
                        remote_path = FILES[workflow_id][file]
                        app.logger.info(f'{file} found in cache as {remote_path}')
                        local_path = f'/tmp/{job_id}_{step_sequence_id}_{file}'
                        with open(local_path, 'wb') as dst:
                            with open(remote_path, 'rb') as src:
                                dst.write(src.read())
                        attachments.append(local_path)
                        attachments_metadata[local_path] = {
                            'uuid': UUIDS[workflow_id][file]
                        }
                        if type_:
                            for parameter in type_.strip().split(','):
                                if '=' not in parameter:
                                    app.logger.error(
                                        f'Invalid workflow command parameter: {parameter}'
                                    )
                                key, _, value = parameter.strip().partition('=')
                                attachments_metadata[local_path][key] = value
                    else:
                        app.logger.warning(f'Could not find {file} in cache')

        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
        if attachments:
            prepare['attachments'] = attachments
        if attachments_metadata:
            prepare['metadata']['attachments'] = attachments_metadata
        publish(prepare, context=app.config['CONTEXT'])

        return make_status_response(
            'OK', f'Step #{metadata["step_sequence_id"]} added to job {job_id} queue.'
        )

    return make_status_response(
        'OK', f'Job {job_id} not handled by this channel plugin.'
    )


def main():
    """Start the Inception EE service."""
    try:
        app.logger.info('Subscribing to ExecutionCommand.')
        sub = subscribe(kind=EXECUTIONCOMMAND, target='inbox', app=app)
        app.logger.info('Providing environments for the following tags: {inception}')
    except Exception as err:
        app.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        app.logger.info('Starting inception execution environment agent.')
        run_app(app)
    finally:
        unsubscribe(sub, app=app)


if __name__ == '__main__':
    main()
