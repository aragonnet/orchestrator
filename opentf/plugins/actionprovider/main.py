# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An action provider plugin.

Provides default implementation for common actions;

# Usage

```
python3 -m opentf.plugins.actionprovider.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from opentf.toolkit import make_plugin, run_plugin

from .checkout import checkout_action, CHECKOUT_CATEGORY
from .files import (
    createfile_action,
    createarchive_action,
    touchfile_action,
    deletefile_action,
    getfiles_action,
    getfile_action,
    prepareinception_action,
    putfile_action,
    CREATEFILE_CATEGORY,
    CREATEARCHIVE_CATEGORY,
    TOUCHFILE_CATEGORY,
    DELETEFILE_CATEGORY,
    GETFILES_CATEGORY,
    GETFILE_CATEGORY,
    PREPAREINCEPTION_CATEGORY,
    PUTFILE_CATEGORY,
)

KNOWN_CATEGORIES = {
    CHECKOUT_CATEGORY: checkout_action,
    CREATEARCHIVE_CATEGORY: createarchive_action,
    CREATEFILE_CATEGORY: createfile_action,
    TOUCHFILE_CATEGORY: touchfile_action,
    DELETEFILE_CATEGORY: deletefile_action,
    GETFILE_CATEGORY: getfile_action,
    GETFILES_CATEGORY: getfiles_action,
    PREPAREINCEPTION_CATEGORY: prepareinception_action,
    PUTFILE_CATEGORY: putfile_action,
}

########################################################################
## Main

plugin = make_plugin(
    name='actionprovider',
    description='Create and start a actionprovider provider.',
    providers=KNOWN_CATEGORIES,
)


if __name__ == '__main__':
    run_plugin(plugin)
