# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Category implementation for the 'action' provider.

The following categories are provided:

- create-archive

- create-file
- delete-file
- get-file
- get-files
- put-file
- touch-file

- prepare-inception
"""

import base64
import json

import yaml

from opentf.commons import make_uuid
from opentf.toolkit import core


########################################################################
## Constants

TOUCHFILE_CATEGORY = 'touch-file'
CREATEARCHIVE_CATEGORY = 'create-archive'
CREATEFILE_CATEGORY = 'create-file'
DELETEFILE_CATEGORY = 'delete-file'
GETFILES_CATEGORY = 'get-files'
GETFILE_CATEGORY = 'get-file'
PREPAREINCEPTION_CATEGORY = 'prepare-inception'
PUTFILE_CATEGORY = 'put-file'


########################################################################
## Formatters


def produce_ini_content(data):
    """Format data as ini file content."""
    content = ''

    for primary in data:
        content += f'[{primary}]\n'
        for key, value in data[primary].items():
            content += f'{key}={value}\n'

    return content


def produce_json_content(data):
    """Format data as json string."""
    return json.dumps(data)


def produce_txt_content(data):
    """Format data as txt string."""
    return data


def produce_yaml_content(data):
    """Format data as yaml string."""
    return yaml.safe_dump(data)


########################################################################
## 'prepare-inception' action


def prepareinception_action(inputs):
    """Process 'prepare-inception' action.

    `prepare-inception` actions have a mandatory input per file it
    prepares.

    ```yaml
    - uses: actions/prepare-inception@v1
      with:
        export.xml: ${{ resources.files.export }}
        report.html: ${{ resources.files.report }}
    ```
    """
    workflow_id = core._getbody()['metadata']['workflow_id']
    return [
        {'run': f'::inception::{workflow_id}::{name}::{value["url"]}'}
        for name, value in inputs.items()
    ]


########################################################################
## 'touch-file' action


def touchfile_action(inputs):
    """Process 'touch-file' action.

    Ensure file exists on the execution environment.

    `touch-file` actions have mandatory `path` input:

    ```yaml
    - uses: actions/touch-file@v1
      with:
        path: foobar.ini
    ```

    This would create an empty `'foobar.ini'` file if it did not already
    exist.
    """
    return [{'run': core.touch_file(inputs['path'])}]


########################################################################
## 'create-file' action


SUPPORTED_CONTENT_TYPES = {
    'ini': produce_ini_content,
    'json': produce_json_content,
    'txt': produce_txt_content,
    'yaml': produce_yaml_content,
}


def createfile_action(inputs):
    """Process 'create-file' action.

    `create-file` actions have mandatory `data`, `format` and `path`
    inputs:

    ```yaml
    - uses: actions/create-file@v1
      with:
        data:
          foo:
            key1: value1
            key2: value2
          bar:
            key1: value1
            key2: value2
        format: ini
        path: foobar.ini
    ```

    This would create a file `'foobar.ini'` with the following content:

    ```ini
    [foo]
    key1=value1
    key2=value2
    [bar]
    key1=value1
    key2=value2
    ```

    The following values for `format` are handled so far:

    - ini
    - json
    - txt
    - yaml
    """
    format_ = inputs['format']
    path = core.normalize_path(inputs['path'])
    data = inputs['data']

    if format_ not in SUPPORTED_CONTENT_TYPES:
        core.fail(
            f'Unknown format {format_}, expecting one of {set(SUPPORTED_CONTENT_TYPES)}.'
        )

    marker = make_uuid()
    content = SUPPORTED_CONTENT_TYPES[format_](data)

    if core.runner_on_windows():
        encoded = str(base64.b64encode(bytes(content, 'utf8')), 'utf8')
        what = f'@echo {encoded} > {marker} & @certutil -f -decode {marker} {path} & @del {marker}'
    else:
        what = f'cat << {marker} > {path}\n{content}\n{marker}'

    return [{'run': what}]


def deletefile_action(inputs):
    """Process 'delete-file' action.

    `delete-file` actions have a mandatory `path` înput:

    ```yaml
    - uses: actions/delete-file@v1
      with:
        path: foobar.ini
    ```
    """
    return [{'run': core.delete_file(inputs['path'])}]


def getfile_action(inputs):
    """Process 'get-file' action.

    `get-file` actions attach the specified file so that publisher
    plugins can process them.  They have mandatory `path` input:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foo.xml'
    ```

    The `get_file` action has an optional `type` input, which is
    expected to be a media type:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foobar.xml'
        type: 'application/xml'
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'bar.json'
      working-directory: /data/foo
    ```
    """
    if 'type' in inputs:
        attach = core.attach_file(inputs['path'], type=inputs['type'])
    else:
        attach = core.attach_file(inputs['path'])
    return [{'run': attach}]


def getfiles_action(inputs):
    """Process 'get-files' action.

    `get-files` actions attach the matching files so that publisher
    plugins can process them.  They have mandatory `pattern` input:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
    ```

    An optional `type` input may be specified.  It is expected to be
    a media type.

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.json'
      working-directory: /data/foo
    ```

    If you want to get files recursively in a folder tree, use the
    `**/*.ext` pattern format:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '**/*.html'
      working-directory: /data/foo
    ```

    An optional `warn-if-not-found` input may be specified.  A warning
    will be issued if no file matching pattern is found.
    """
    pattern = inputs['pattern']

    warn = inputs.get('warn-if-not-found')
    type_ = inputs.get('type')
    if type_:
        type_ = ' type=' + type_.strip()
    else:
        type_ = ''

    if pattern.startswith('**/') or pattern.startswith('**\\'):
        file_pattern = pattern[3:]
        if core.runner_on_windows():
            what = f'@for /f %%i in (\'dir /s /b "{file_pattern}"\') do @echo ::attach{type_}::%%i'
            if warn:
                what = f'({what}) || echo ::warning::{warn} & exit 0'
        else:
            what = f'for f in $(find -name \\{file_pattern}); do echo "::attach{type_}::$(pwd)/$f" ; done'
            if warn is None:
                what += ';'
            else:
                what = f'if test -z "$(find . -name \'{file_pattern}\' -print -quit)"; then echo "::warning::{warn}"; else {what} fi'
    else:
        if core.runner_on_windows():
            what = f'(@for %%i in ({pattern}) do @echo ::attach{type_}::%CD%\\%%i)'
            if warn is not None:
                what += f' & if not exist {pattern} echo ::warning::{warn}'
        else:
            what = f'for f in {pattern} ; do echo "::attach{type_}::$(pwd)/$f" ; done'
            if warn is None:
                what += ';'
            else:
                what = f'if test -z "$(find . -maxdepth 1 -name \'{pattern}\' -print -quit)"; then echo "::warning::{warn}"; else {what}; fi'

    return [{'run': what}]


def putfile_action(inputs):
    """Process 'put-file' action.

    `put-file` actions have mandatory `data` and `path` inputs:

    ```yaml
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
    ```
    """
    if core.runner_on_windows():
        what = f'echo ::put file={inputs["file"]}::{inputs["path"]}'
    else:
        what = f'echo "::put file={inputs["file"]}::{inputs["path"]}"'

    return [{'run': what}]


########################################################################
## 'create-archive' action


DEFAULT_ARCHIVE_FORMAT = 'tar.gz'
SUPPORTED_ARCHIVE_FORMATS = {
    DEFAULT_ARCHIVE_FORMAT: 'tar -czf',
    'tar': 'tar -cf',
}
LIST_ARCHIVE_FILES = 'list_tar_files.txt'
DEPTH_LEVEL1 = ' -maxdepth 1'
ARCHIVE_ERROR_MESSAGE = 'No file matching patterns is found to archive.'
ARCHIVE_WARNING_MESSAGE = (
    'The specified patterns do not match any files. The generated archive is empty.'
)


def _add_pattern_handling_steps(
    steps, file_pattern: str, depth_level: str, recursive_mode: bool
):
    """Add to steps the commands required to handle file_pattern.

    `depth_level` allows to authorize or not the recursive searches
    in the subfolders of the current directory.
    If `depth_level` = DEPTH_LEVEL1, then the search is only performed
    in the current directory.

    `recursive_mode` is True when the pattern format starts with '**',
    otherwise it defaults to False.
    """
    if core.runner_on_windows():
        if depth_level == DEPTH_LEVEL1:
            search_pattern = f'''@echo off
                    for %%G in ({file_pattern}) do (echo %%G>> {LIST_ARCHIVE_FILES})'''
        else:
            search_pattern = f'''@echo off
                    set "firstDirectory=%CD%"
                    setlocal EnableDelayedExpansion
                    for /f "tokens=*" %%G in ('dir /b /s "{file_pattern}"') do (
                    set "item=%%G"
                    set "item=!item:%firstDirectory%\=!"
                    echo !item!>> {LIST_ARCHIVE_FILES}
                    )'''
        steps.append(
            {
                'run': search_pattern,
                'continue-on-error': True,
            }
        )
    else:
        if recursive_mode is False and file_pattern.count("*") == 0:
            steps.append(
                {
                    'run': f'echo "{file_pattern}">> {LIST_ARCHIVE_FILES}',
                    'continue-on-error': True,
                }
            )
        else:
            pattern_directory = file_pattern.rpartition('/')[0]
            if pattern_directory == '':
                pattern_directory = '.'
            pattern_name = file_pattern.rpartition('/')[2]
            if pattern_name == '':
                pattern_name = '*'
            steps.append(
                {
                    'run': f'find {pattern_directory}{depth_level} -name "{pattern_name}" -print>> {LIST_ARCHIVE_FILES}',
                    'continue-on-error': True,
                }
            )


def createarchive_action(inputs):
    """Process 'create-archive' action.

    `create-archive` actions create an archive from selected files or directories.
    They have mandatory `path` and `patterns` inputs.

    If no files or directories match the specified patterns, an error message is issued,
    no archive is created, and the workflow is stopped. This is the default behavior.

    An optional `warn-if-not-found` entry can be specified. Instead of the default behavior,
    if the specified patterns do not match any files, a warning message is issued,
    an empty archive is created, and the workflow continues.

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
    ```

    An optional `format` input may be specified.
    Possible archive formats are `tar` or `tar.gz`.
    Default format is `tar.gz`.

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar
        format: tar
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
    ```

    If you need to archive files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        format: tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
      working-directory: /data/foo
    ```

    If you want to archive files recursively in a folder tree, use the
    `**/*.png` pattern format:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*.png'
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
      working-directory: /data/foo
    ```

    If you want to display a warning message when no files matching one on the patterns are found,
    use the `warn-if-not-found` input. This allows to create an empty archive:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
        warn-if-not-found: true
      working-directory: /data/foo
    ```
    """
    path = inputs['path']
    patterns = inputs['patterns']

    warn = inputs.get('warn-if-not-found')
    format_ = inputs.get('format')
    if format_ is None:
        format_ = DEFAULT_ARCHIVE_FORMAT
    if format_ not in SUPPORTED_ARCHIVE_FORMATS:
        core.fail(
            f'Unknown {format_} archive format, expecting one of {set(SUPPORTED_ARCHIVE_FORMATS)}.'
        )
    archive_cmd = SUPPORTED_ARCHIVE_FORMATS[format_]

    steps = [
        {'run': core.delete_file(path)},
        {'run': core.delete_file(LIST_ARCHIVE_FILES)},
        {'run': core.touch_file(LIST_ARCHIVE_FILES)},
    ]

    for pattern in patterns:
        depth_level = ' '
        recursive_mode = False
        if pattern.startswith('**/') or pattern.startswith('**\\'):
            file_pattern = pattern[3:]
            recursive_mode = True
        elif pattern.endswith('/') or pattern.endswith('\\'):
            file_pattern = pattern
        else:
            file_pattern = pattern
            depth_level = DEPTH_LEVEL1
        _add_pattern_handling_steps(steps, file_pattern, depth_level, recursive_mode)

    what = f'{archive_cmd} "{path}" -T {LIST_ARCHIVE_FILES}'
    if core.runner_on_windows():
        if warn:
            steps.append(
                {
                    'run': f'{what} & for /f %%i in ("{LIST_ARCHIVE_FILES}") do if %%~zi equ 0 (echo ::warning::{ARCHIVE_WARNING_MESSAGE})'
                }
            )
        else:
            steps.append(
                {
                    'run': f'for /f %%i in ("{LIST_ARCHIVE_FILES}") do if %%~zi gtr 0 ({what}) else (echo ::error::{ARCHIVE_ERROR_MESSAGE} & exit 1)'
                }
            )
    else:
        if warn:
            steps.append(
                {
                    'run': f'{what}; if [ ! -s "{LIST_ARCHIVE_FILES}" ]; then echo "::warning::{ARCHIVE_WARNING_MESSAGE}"; fi'
                }
            )
        else:
            steps.append(
                {
                    'run': f'if [ -s "{LIST_ARCHIVE_FILES}" ]; then {what}; else echo "::error::{ARCHIVE_ERROR_MESSAGE}"; exit 1;fi'
                }
            )

    return steps
