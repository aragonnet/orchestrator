# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" The checkout action """

import re

from urllib.parse import urlsplit, urlunsplit

from opentf.toolkit import core


########################################################################
# Constants

CHECKOUT_CATEGORY = 'checkout'

SHA_PATTERN = r'^[0-9a-fA-F]{40}$'


def maybe_override(repository, contexts):
    """Override credentials if respository defined in resources."""
    if contexts is None or contexts.get('resources') is None:
        return repository
    if 'repositories' not in contexts['resources']:
        return repository
    predefined = contexts['resources']['repositories']
    org = urlsplit(repository)
    for item in predefined.values():
        if f'/{item["repository"]}' == org.path:
            dst = urlsplit(item['endpoint'])
            if (
                org.scheme == dst.scheme
                and org.hostname == dst.hostname
                and org.port == dst.port
            ):
                if org.netloc == dst.netloc:
                    return repository
                return urlunsplit(
                    (org.scheme, dst.netloc, org.path, org.query, org.fragment)
                )
    return repository


def checkout_action(inputs):
    """Process the checkout message.

    `checkout` actions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for two optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name or a commit sha (defaults to the default branch if
      unspecified)
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
    ```
    """
    repository = maybe_override(inputs['repository'], core._getcontexts())

    sha = ''
    ref = inputs.get('ref', '')
    path = inputs.get('path', '')
    if ref:
        if re.match(SHA_PATTERN, ref):
            if not path:
                # humanish name, aka, what's before /.git[/]
                target = repository.strip('/').split('/')[-1]
                if target.endswith('.git'):
                    target = target[:-4]
            else:
                target = path
            sha = f';cd {target};git checkout {ref}'
            ref = '-n '
        else:
            ref = f'-b {ref} '

    return [{'run': f'git clone {ref}{repository} {path}{sha}'}]
