# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# plugin.yaml
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  description: |
    Common actions that can be used on any execution environment.

    Provide actions in the following areas:

    - git commands
    - files commands
    - inception commands
events:
- categoryPrefix: actions

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: checkout
  description: |
    Checkout a repository at a particular version.

    `checkout` actions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for two optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name, or a commit sha (defaults to the default branch if
      unspecified)
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
    ```
branding:
  icon: arrow-down
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: checkout
  categoryVersion: v2
- categoryPrefix: action  # for TM generator
  category: checkout
  categoryVersion: v2
inputs:
  repository:
    description: the repository to clone
    required: true
  ref:
    description: the branch, tag, or sha to checkout
    required: false
  path:
    description: where to clone the repository
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: create-file
  description: |
    Create a file on the execution environment.

    `create-file` actions have mandatory `data`, `format` and `path`
    inputs.

    The following values for `format` are handled:

    - ini
    - json
    - txt
    - yaml

    ## Example

    ```yaml
    - uses: actions/create-file@v1
      with:
        data:
          foo:
            key1: value1
            key2: value2
          bar:
            key1: value1
            key2: value2
        format: ini
        path: foobar.ini
    ```

    This will create a `foobar.ini` file with the following content:

    ```ini
    [foo]
    key1=value1
    key2=value2
    [bar]
    key1=value1
    key2=value2
    ```
branding:
  icon: file-plus
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: create-file
inputs:
  data:
    description: the file content
    required: true
  format:
    description: the file format, one of ini, json, txt, yaml
    required: true
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: delete-file
  description: |
    Delete a file on the execution environment.

    `delete-file` actions have a mandatory `path` înput:

    ```yaml
    - uses: actions/delete-file@v1
      with:
        path: foobar.ini
    ```
branding:
  icon: file-minus
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: delete-file
inputs:
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: get-file
  description: |
    Attach a file from the execution environment.

    `get-file` actions attach the specified file so that publisher
    plugins can process them.  They have mandatory `path` input and
    an optional `type` input.

    ## Examples

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foo.xml'
    ```

    It is a good practice to specify the attachment type, if known:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foobar.xml'
        type: 'application/xml'
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'bar.json'
      working-directory: /data/foo
    ```
branding:
  icon: upload
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: get-file
inputs:
  path:
    description: the file to attach
    required: true
  type:
    description: the file type
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: get-files
  description: |
    Attach a set of files from the execution environment.

    `get-files` actions attach the matching files so that publisher
    plugins can process them.  They have mandatory `pattern` input.

    An optional `type` input may be specified.  It is expected to be
    a media type.

    An optional `warn-if-not-found` input may be specified.  A warning
    will be issued if no file matching pattern is found.

    ## Examples

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
    ```

    It is a good practice to specify the attachment type, if known.  Please
    note that all matching files will be decorated with the specified type:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
        type: 'application/xml'
    ```

    If you want to get files recursively in a folder tree, use the
    `**/*.ext` pattern format:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '**/*.html'
      working-directory: /data/foo
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.json'
      working-directory: /data/foo
    ```
branding:
  icon: upload
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: get-files
inputs:
  pattern:
    description: the pattern that identifies the files to attach
    required: true
  type:
    description: the file type
    required: false
  warn-if-not-found:
    description: the warning to display if no matching file is found
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: put-file
  description: |
    Put a file on the execution environment.

    `put-file` actions have mandatory `data` and `path` inputs.

    ## Examples

    Put a file in the current directory in an execution environment:
    ```yaml
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
    ```
    If you need to put a file somewhere other than the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
      working-directory: /foo/bar
    ```

    If you want to put a file in the execution environment from a PEaC, the file
    must be declared as a PEaC resource:

    ```yaml
    metadata:
      name: put-file example
    resources:
      files:
      - foo.json
    jobs:
      keyword-driven:
        runs-on: ssh
        steps:
        - uses: actions/put-file@v1
          with:
            file: foo.json
            path: bar/baz.json
        working-directory: /qux/quux/corge
    ```

    This example can be run with the following cURL command:

    ```bash
    curl -X POST \
         -F workflow=@{my-PEaC.yaml} \
         -F foo.json=@foo.json \
         -H "Authorization: Bearer {my-token}" \
         http://example.com/workflows
    ```

    If the refered file is not part of the `resources` section of your PEaC file,
    the step will fail with an error code 2.
branding:
  icon: file-plus
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
  - categoryPrefix: actions
    category: put-file
inputs:
  file:
    description: |
      the file to upload, which must be an entry in the `resources.files` part of
      the workflow
    required: true
  path:
    description: the file destination, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: touch-file
  description: |
    Ensure a file exists on the execution environment.

    If the file does not exist, it will be created (with an empty content).

    `touch-file` actions have a mandatory `path` înput:

    ```yaml
    - uses: actions/touch-file@v1
      with:
        path: foobar.ini
    ```
branding:
  icon: file
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: touch-file
inputs:
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: prepare-inception
  description: |
    Preload the inception environment with data.

    `prepare-inception` actions have a mandatory input per file it
    prepares.

    ## Example

    ```yaml
    - uses: actions/prepare-inception@v1
      with:
        export.xml: ${{ resources.files.export }}
        report.html: ${{ resources.files.report }}
    ```
branding:
  icon: film
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: prepare-inception
inputs:
  pattern:
    description: the pattern that identifies the files to attach
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: create-archive
  description: |
    Create an archive from a set of selected files or directories on the execution environment.

    `create-archive` actions create an archive from selected files or directories.
    They have mandatory `path` and `patterns` inputs.

    If no files or directories match the specified patterns, an error message is issued,
    no archive is created, and the workflow is stopped. This is the default behavior.

    An optional `warn-if-not-found` entry can be specified. Instead of the default behavior,
    if the specified patterns do not match any files, a warning message is issued,
    an empty archive is created, and the workflow continues.

    An optional `format` input may be specified.
    Possible archive formats are `tar` or `tar.gz`. Default format is `tar.gz`.

    ## Examples

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
    ```

    If you want to specify a 'tar' format to create the archive instead the 'tar.gz' default format,
    use the `format` input:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar
        format: tar
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
    ```

    If you need to archive files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        format: tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
      working-directory: /data/foo
    ```

    If you want to archive files recursively in a folder tree, use the
    `**/*.png` pattern format:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*.png'
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
      working-directory: /data/foo
    ```

    If you want to display a warning message when no files matching one on the patterns are found,
    use the `warn-if-not-found` input. This allows to create an empty archive:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - myfolder/
        - foo.html
        - '*.png'
        warn-if-not-found: true
      working-directory: /data/foo
    ```
branding:
  icon: upload
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
  - categoryPrefix: actions
    category: create-archive
inputs:
  path:
    description: the archive name, relative to the current working directory
    required: true
  patterns:
    description: |
        a list of directory or file name patterns, possibly with paths. They may contain placeholders (* or **).
    required: true
  format:
    description: the archive format, either tar or tar.gz if specified, defaulting to tar.gz if not specified.
    required: false
  warn-if-not-found:
    description: |
      A boolean.  Set to `true` to enable the generation of a warning message when no files matching
      one on the patterns are found. This allows to create an empty archive.
      By default, the set value is `false`.
    required: false