# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A dummy event rewriter plugin.

This rewriter plugin translates events from one format to another format.

# Usage

```
python3 -m opentf.plugins.rewriter.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

import sys

from flask import request

from opentf.commons import (
    ALLURE_COLLECTOR_OUTPUT,
    WORKFLOWRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
    publish,
    make_event,
)

########################################################################
## Constants

CONFIG_FILE = 'conf/rewriter.yaml'

########################################################################
## Main

app = make_app(
    name='rewriter',
    description='Create and start a rewriter plugin.',
    configfile=CONFIG_FILE,
)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new result message."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(ALLURE_COLLECTOR_OUTPUT, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid {body["kind"]} request: {extra}'
        )

    workflow_id = body['metadata']['workflow_id']

    with_ = body.get('with') or {}
    if report := with_.get('allureReport'):
        publish(
            make_event(
                WORKFLOWRESULT,
                metadata={
                    'name': 'WorkflowResult generated from AllureCollectorOutput',
                    'workflow_id': workflow_id,
                },
                attachments=[report],
            ),
            context=app.config['CONTEXT'],
        )

    return make_status_response('OK', '')


def main():
    """Start the Local Publisher service."""
    try:
        app.logger.info(f'Subscribing to {ALLURE_COLLECTOR_OUTPUT}.')
        uuid_a = subscribe(kind=ALLURE_COLLECTOR_OUTPUT, target='/inbox', app=app)
    except Exception as err:
        app.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        app.logger.info('Starting rewriter service.')
        run_app(app)
    finally:
        unsubscribe(uuid_a, app=app)


if __name__ == '__main__':
    main()
