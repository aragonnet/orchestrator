# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A SSH channel plugin."""


from collections import defaultdict
from datetime import datetime, timedelta
from io import BytesIO
from queue import Queue
import ntpath
import re
import sys
import threading

from flask import request
from paramiko.client import SSHClient, AutoAddPolicy
from scp import SCPClient

from opentf.commons import (
    SSHSERVICECONFIG,
    EXECUTIONCOMMAND,
    EXECUTIONERROR,
    EXECUTIONRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    ConfigError,
    make_uuid,
    make_event,
)

from opentf.toolkit import core
from opentf.toolkit.channels import (
    make_script,
    ADDMASK_COMMAND,
    ATTACH_COMMAND,
    DEBUG_COMMAND,
    ERROR_COMMAND,
    PUT_FILE_COMMAND,
    SETOUTPUT_COMMAND,
    STOPCOMMANDS_COMMAND,
    WARNING_COMMAND,
    DEFAULT_CHANNEL_LEASE,
    RUNNER_OS,
    SCRIPTFILE_DEFAULT,
    SCRIPTPATH_DEFAULT,
    SHELL_DEFAULT,
    SHELL_TEMPLATE,
)

########################################################################
## Constants

CONFIG_FILE = 'conf/sshee.yaml'

W = '\033[0m'  # white (normal)
R = '\033[31m'  # red
G = '\033[32m'  # green
O = '\033[33m'  # orange
B = '\033[34m'  # blue
P = '\033[35m'  # purple


CHANNEL_REQUEST = -1
CHANNEL_RELEASE = -2
CHANNEL_NOTIFY = -3


########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args):
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Job Helpers

HOSTS = {}
HOSTS_LEASES = {}

STEPS_QUEUE = Queue()
JOBS = {}
JOBS_HOSTS = {}
JOBS_MASKS = defaultdict(list)
CHANNELS = {}

REQUESTS_QUEUE = Queue()


# Steps helpers


def list_job_steps(job_id):
    """List all steps for job.

    Returns a dictionary.  Keys are step sequence IDs and values are
    commands.
    """
    if job_id not in JOBS:
        JOBS[job_id] = {}
    return JOBS[job_id]


def add_job_step(job_id, command):
    """Adding new step to existing job."""
    JOBS[job_id][command['metadata']['step_sequence_id']] = command


def get_job_host(job_id, channel_id):
    """Get host_id for job.

    # Required parameters

    - job_id: a string
    - channel_id: a string

    # Returned value

    A _host ID_ or None.  A host_id a string.
    """
    if job_id in JOBS_HOSTS:
        return JOBS_HOSTS[job_id]

    for host_id in CHANNELS[channel_id]:
        if host_id in HOSTS_LEASES and HOSTS_LEASES[host_id] >= datetime.now():
            JOBS_HOSTS[job_id] = host_id
            return host_id

    return None


def connect(host_id, client):
    """Connect to host.

    Handling various connection modes:

    - username, password
    - username, key_filename
    - username, key_filename, passphrase
    """
    host = HOSTS[host_id]
    if 'ssh_host_keys' in host:
        client.load_host_keys(host['ssh_host_keys'])
    if host.get('missing_host_key_policy', 'reject') == 'auto-add':
        client.set_missing_host_key_policy(AutoAddPolicy)
    if 'key_filename' in host and 'passphrase' in host:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            key_filename=host['key_filename'],
            passphrase=host['passphrase'],
        )
    elif 'key_filename' in host:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            key_filename=host['key_filename'],
        )
    else:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            password=host['password'],
        )


def get_tags():
    """Get tags provided by plugin configuration.

    # Returned value

    A possibly empty set of strings.
    """
    tags = set()
    for host in HOSTS.values():
        for tag in host['tags']:
            tags.add(tag)
    return tags


def get_hosts_from_conf():
    """Exit if plugin configuration is invalid.

    Fills the HOST list with valid entries.
    """
    HOSTS.clear()
    for target in app.config['CONTEXT']['targets']:
        if target not in app.config['CONFIG']['pools']:
            error(f'Could not find target {target} in pool.')
            sys.exit(2)

        pool = app.config['CONFIG']['pools'][target]
        for entry in pool:
            if 'key_filename' in entry and 'password' in entry:
                error(
                    f'Bad host specification for {entry["host"]} in pool {target},'
                    + ' cannot specify password and key_filename at the same time.'
                )
                entry['disabled'] = True
            if 'key_filename' not in entry and 'password' not in entry:
                error(
                    f'Bad host specification for {entry["host"]} in pool {target},'
                    + ' must specify either password or key_filename.'
                )
                entry['disabled'] = True
            if len(set(entry['tags']) & RUNNER_OS) != 1:
                error(
                    f'Bad host specification for {entry["host"]} in pool {target},'
                    + f' must specify exactly one of {RUNNER_OS}.'
                )
                entry['disabled'] = True
            if 'disabled' not in entry:
                what = entry.copy()
                if 'host' in entry:
                    what['id'] = make_uuid()
                    HOSTS[what['id']] = what
                else:
                    for host in entry['hosts']:
                        what = entry.copy()
                        del what['hosts']
                        what['id'] = make_uuid()
                        what['host'] = host
                        HOSTS[what['id']] = what
    info(f'Providing environments for the following tags: {get_tags()}')


def get_matching_hosts(tags):
    """List all nondisabled hosts capable of handling tags.

    # Required parameters

    - tags: a collection of strings

    # Returned value

    A possibly empty list of _hosts IDs_.  A host ID is a string.
    """
    requiredtags = set(tags)
    matching = []
    for host in HOSTS.values():
        if requiredtags.issubset(set(host['tags'])):
            matching.append(host['id'])
    if not matching:
        warning(f'Could not find environment matching request {tags}.')
    return matching


def get_os(host_id):
    """Get host os."""
    return (set(HOSTS[host_id]['tags']) & RUNNER_OS).pop()


def is_available(host_id):
    """Check if host has a lease or is busy handling a job."""
    if host_id in JOBS_HOSTS.values():
        return False
    if host_id in HOSTS_LEASES:
        return HOSTS_LEASES[host_id] < datetime.now()
    return True


def is_alive(host_id):
    """Checks if host is available."""
    try:
        with SSHClient() as client:
            connect(host_id, client)
            return True
    except Exception as err:
        warning(f'Could not reach host {host_id}: {err}')
    return False


def make_offers(metadata, candidates):
    """Make at least one offer.

    Currently makes exactly one offer, assuming the tags are valid.
    This may change at any time.

    # Required parameters

    - metadata: a dictionary
    - candidates: a non-empty list of strings

    # Returned value

    None.
    """
    for host_id in candidates:
        if not is_available(host_id):
            continue
        if not is_alive(host_id):
            continue
        channel_id = metadata['channel_id'] = make_uuid()
        metadata['channel_tags'] = HOSTS[host_id]['tags']
        metadata['channel_os'] = get_os(host_id)
        metadata['channel_temp'] = HOSTS[host_id].get(
            'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
        )
        metadata['channel_lease'] = DEFAULT_CHANNEL_LEASE
        HOSTS_LEASES[host_id] = datetime.now() + timedelta(
            seconds=DEFAULT_CHANNEL_LEASE
        )
        CHANNELS[channel_id] = [host_id]
        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
        publish(prepare, context=app.config['CONTEXT'])
        break
    else:
        warning(f'Could not find valid candidate for job {metadata["job_id"]}')


def release_resources(host_id, job_id, channel_id):
    """Release resources after teardown."""
    try:
        del JOBS_HOSTS[job_id]
    except KeyError:
        pass
    try:
        del JOBS[job_id]
    except KeyError:
        pass
    try:
        del JOBS_MASKS[job_id]
    except KeyError:
        pass
    try:
        del HOSTS_LEASES[host_id]
    except KeyError:
        pass
    try:
        del CHANNELS[channel_id]
    except KeyError:
        pass


########################################################################


def mask(line, job_id):
    """Remove masked values."""
    for masked in JOBS_MASKS[job_id]:
        line = line.replace(masked, '***')
    return line


def run_command(command):
    """Run a command.

    Runs in its own thread, so that it does not prevent other steps from
    running in parallel.

    Will publish an ExecutionError event if a technical error occurs
    while processing the command.

    # Required parameters

    - command: a dictionary
    """
    metadata = command['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']
    channel_id = metadata['channel_id']

    if isinstance(CHANNELS[channel_id], list):
        if (host_id := get_job_host(job_id, channel_id)) is None:
            msg = f'Channel lease {channel_id} expired for job {job_id}'
            error(msg)
            publish(
                make_event(EXECUTIONERROR, metadata=metadata, details={'error': msg}),
                context=app.config['CONTEXT'],
            )
            release_resources(host_id, job_id, channel_id)
            return
        CHANNELS[channel_id] = host_id
    else:
        host_id = CHANNELS[channel_id]

    runner_os = get_os(host_id)
    is_windows = runner_os == 'windows'
    root = HOSTS[host_id].get('script_path', SCRIPTPATH_DEFAULT[runner_os])
    script_file = SCRIPTFILE_DEFAULT[runner_os].format(
        root=root, job_id=job_id, step_sequence_id=step_sequence_id
    )
    script = make_script(command, job_id, runner_os, root)
    shell = SHELL_TEMPLATE[command.get('shell') or SHELL_DEFAULT[runner_os]]
    attachments = []
    attachments_metadata = {}
    outputs = {}
    logs = []
    stop_command = None
    try:
        with SSHClient() as client:
            connect(host_id, client)
            scp = SCPClient(client.get_transport())
            scp.putfo(BytesIO(bytes(script, 'utf-8')), script_file)
            _, stdout, stderr = client.exec_command(shell.format(script_file))
            resp = stdout.channel.recv_exit_status()
            if step_sequence_id == CHANNEL_RELEASE and runner_os == 'windows':
                resp = 0  # windows returns 1 as we remove the running script
            stdout_content = list(stdout)
            for line in stdout_content:
                # Parsing stdout for workflow commands
                if stop_command is not None:
                    if line == stop_command:
                        stop_command = None
                    else:
                        logs.append(mask(line, job_id))
                    continue

                if wcmd := re.match(ATTACH_COMMAND, line):
                    remote = wcmd.group(2)
                    if is_windows:
                        remote = ntpath.normpath(remote)
                    try:
                        local_path = (
                            f'/tmp/{job_id}_{step_sequence_id}_{remote.split("/")[-1]}'
                        )
                        scp.get(
                            remote_path=remote,
                            local_path=local_path,
                        )
                        attachments.append(local_path)
                        attachments_metadata[local_path] = {'uuid': make_uuid()}
                        if wcmd.group(1):
                            for parameter in wcmd.group(1).strip().split(','):
                                if '=' not in parameter:
                                    error(
                                        f'Invalid workflow command parameter: {parameter}'
                                    )
                                key, _, value = parameter.strip().partition('=')
                                attachments_metadata[local_path][key] = value
                    except Exception as err:
                        error(f'Could not read {remote}: {err}.')
                elif wcmd := re.match(PUT_FILE_COMMAND, line):
                    data = wcmd.group(1)
                    remote_path = wcmd.group(2)
                    working_directory_path = core.join_path(
                        job_id, command.get('working-directory'), is_windows
                    )
                    targeted_remote_path = core.join_path(
                        working_directory_path, remote_path, is_windows
                    )
                    try:
                        scp.put(
                            remote_path=targeted_remote_path,
                            files='/tmp/in_{uuid}_{name}'.format(
                                uuid=metadata['workflow_id'], name=data
                            ),
                        )
                    except Exception as err:
                        error(
                            f'Could not send file {data} to remote path {remote_path}: {err}.'
                        )
                        resp = 2
                elif wcmd := re.match(SETOUTPUT_COMMAND, line):
                    outputs[wcmd.group(1)] = wcmd.group(2)
                elif wcmd := re.match(DEBUG_COMMAND, line):
                    logs.append(f'DEBUG,{mask(wcmd.group(1), job_id)}')
                elif wcmd := re.match(WARNING_COMMAND, line):
                    logs.append(f'WARNING,{mask(wcmd.group(3), job_id)}')
                elif wcmd := re.match(ERROR_COMMAND, line):
                    logs.append(f'ERROR,{mask(wcmd.group(3), job_id)}')
                elif wcmd := re.match(STOPCOMMANDS_COMMAND, line):
                    stop_command = f'::{wcmd.group(1)}::'
                elif wcmd := re.match(ADDMASK_COMMAND, line):
                    JOBS_MASKS[job_id].append(wcmd.group(1))
                else:
                    logs.append(mask(line, job_id))

        stderr_content = list(stderr)
        for line in stderr_content:
            logs.append(mask(line, job_id))

        print(
            f'Job {B}{job_id}{W} step {B}{step_sequence_id}{W}\n'
            + f'{G}[{stdout_content}]{W}\n{R}[{stderr_content}]{W}\n{P}(RC={resp}){W}'
        )
        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=resp)
        if attachments:
            prepare['attachments'] = attachments
        if attachments_metadata:
            prepare['metadata']['attachments'] = attachments_metadata
        if outputs:
            prepare['outputs'] = outputs
        if logs:
            prepare['logs'] = logs
        publish(prepare, context=app.config['CONTEXT'])
    except Exception as err:
        error(f'An exception occurred while communicating to SSH environment:\n{err}')
        publish(
            make_event(
                EXECUTIONERROR,
                metadata=metadata,
                details={'error': str(err)},
            ),
            context=app.config['CONTEXT'],
        )

    if step_sequence_id == CHANNEL_RELEASE:
        release_resources(host_id, job_id, metadata['channel_id'])


def prepare_commands():
    """Prepare command handler and start it."""
    while True:
        try:
            command = STEPS_QUEUE.get()
            job_id = command['metadata']['job_id']
            if command['metadata']['step_sequence_id'] in list_job_steps(job_id):
                continue
            add_job_step(job_id, command)

            threading.Thread(target=run_command, args=(command,), daemon=True).start()
        except Exception as err:
            error(f'Internal error while preparing command thread: {err}')


def prepare_offers():
    """Prepare offers.

    Offers are serialized so that no race condition occurs and so that
    a 'fair' allocation stragegy can be defined.
    """
    while True:
        try:
            metadata, candidates = REQUESTS_QUEUE.get()
            make_offers(metadata, candidates)
        except Exception as err:
            error(f'Internal error while making offer: {err}.')


########################################################################
## Main


try:
    app = make_app(
        name='sshchannel',
        description='Create and start a SSH channel plugin.',
        configfile=CONFIG_FILE,
        schema=SSHSERVICECONFIG,
    )
    get_hosts_from_conf()
except ConfigError:
    sys.exit(2)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json() or {}
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(EXECUTIONCOMMAND, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid ExecutionCommand request: {extra}.'
        )

    metadata = body['metadata']
    job_id = metadata['job_id']

    if metadata['step_sequence_id'] == CHANNEL_REQUEST:
        # A new job, can we make an offer?
        requiredtags = body['runs-on']
        if not isinstance(requiredtags, list):
            requiredtags = [requiredtags]
        candidates = get_matching_hosts(requiredtags)
        if not candidates:
            return make_status_response(
                'OK', f'Not providing execution environment matching {requiredtags}.'
            )
        REQUESTS_QUEUE.put((metadata, candidates))
        return make_status_response('OK', f'Making an offer for job {job_id}')

    if metadata.get('channel_id') not in CHANNELS:
        return make_status_response(
            'OK', f'Job {job_id} not handled by this channel plugin.'
        )
    steps = list_job_steps(job_id)
    if metadata['step_sequence_id'] in steps:
        return make_status_response(
            'AlreadyExists',
            f'Step {metadata["step_sequence_id"]} already received for job {job_id}.',
        )

    STEPS_QUEUE.put(body)
    return make_status_response(
        'OK', f'Step #{metadata["step_sequence_id"]} added to job {job_id} queue.'
    )


def main():
    """Start the SSH channel plugin."""
    try:
        info('Starting steps handling thread.')
        threading.Thread(target=prepare_commands, daemon=True).start()
    except Exception as err:
        error('Could not start steps handling thread: %s.', str(err))
        sys.exit(2)
    try:
        info('Starting offers handling thread.')
        threading.Thread(target=prepare_offers, daemon=True).start()
    except Exception as err:
        error('Could not start offers handling thread: %s.', str(err))

    tags = get_tags()
    try:
        info('Subscribing to ExecutionCommand.')
        sub = subscribe(kind=EXECUTIONCOMMAND, target='inbox', app=app)
        info(f'Providing environments for the following tags: {tags}')
    except Exception as err:
        error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        info('Starting SSH channel plugin.')
        run_app(app)
    finally:
        unsubscribe(sub, app=app)


if __name__ == '__main__':
    main()
