# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the skf provider.

The following categories are provided:

- execute
- params
- skf
"""

import json
import os
import sys

from opentf.toolkit import core

########################################################################
## Constants

EXECUTE_CATEGORY = 'execute'
PARAM_CATEGORY = 'params'
SKF_CATEGORY = 'skf'
GLOBAL_PARAMS_FILE_PATH = 'global_params.txt'
CONTEXT_PARAMS_FILE_PATH = 'context_params.txt'
SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

REPORT_TYPE = 'application/vnd.opentestfactory.skf-surefire+xml'

########################################################################
# 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: skf/execute@v1
      with:
        test: foobar
    ```
    `test` is of the form `{root_project}/{ecosystem}#{testscript.ta}`
    to execute a specific test script

    The root ecosystem is always `tests`. If the skf project contains an ecosystem tree, the targeted
    ecosystem can be expressed as follows:

        {ecosystem_I}.{ecosystem_1}.{ecosystem_a}
    """
    test_reference = inputs['test']

    json_dictionary = {}
    ecosystem = ''
    ecosystem_path = ''
    root_project = ''
    parts = test_reference.partition('/')
    test_case_arg = test_reference
    default_ecosystem = 'tests'

    while len(parts[2].rstrip()) > 0:
        root_project = root_project + parts[0] + '/'
        test_case_arg = parts[2]
        parts = parts[2].partition('/')
    test_definition_part = test_case_arg.partition('#')
    ecosystems_list = test_definition_part[0].split('.')

    if ecosystems_list[0] != default_ecosystem:
        core.fail(
            f"The default ecosystem must be '{default_ecosystem}' in the test reference."
        )

    for i in range(1, len(ecosystems_list)):
        ecosystem += ecosystems_list[i] + '/'
        ecosystem_path += ecosystems_list[i] + '.'

    if test_definition_part[1] == '' or test_definition_part[2] == '':
        core.fail("You must target a ta script file")
    else:
        test_dictionary = {}
        test_script = test_definition_part[2]
        json_dictionary = {'test': []}
        test_dictionary['script'] = ecosystem + test_script
        json_dictionary['test'].append(test_dictionary)

    json_path = 'test.json'
    json_object = json.dumps(json_dictionary)
    test_object = json_object[1:-3]

    if core.runner_on_windows():
        root_project = root_project.replace('/', '\\')
        test_object = test_object.replace("\"", "^\"")
        skf_parametrized_execution_script_template = os.path.join(
            sys.modules['opentf'].__path__[0],
            'plugins/skf/resources/skf_parametrized_execution_script_template.bat',
        )
        json_file_path = f'{root_project}\\test.json'
    else:
        test_object = test_object.replace("\"", "\\\"")
        skf_parametrized_execution_script_template = os.path.join(
            sys.modules['opentf'].__path__[0],
            'plugins/skf/resources/skf_parametrized_execution_script_template.sh',
        )
        json_file_path = f'{root_project}/test.json'

    with open(skf_parametrized_execution_script_template, 'r') as file:
        skf_json_file_builder = file.read().format(
            test=test_object,
            global_param_file=GLOBAL_PARAMS_FILE_PATH,
            context_param_file=CONTEXT_PARAMS_FILE_PATH,
            json_path=json_file_path,
        )

    surefire_reports_path = f'{root_project}/target/squashTA/surefire-reports/'
    surefire_xml_report_path = (
        f'{surefire_reports_path}TEST-{default_ecosystem}.{ecosystem_path}xml'
    )
    surefire_txt_report_path = (
        f'{surefire_reports_path}{default_ecosystem}.{ecosystem_path}txt'
    )
    output_path = f'{default_ecosystem}.{ecosystem_path}'[:-1]
    surefire_txt_output_report_path = f'{surefire_reports_path}{output_path}-output.txt'

    html_report_path = f'{root_project}/target/squashTA/html-reports.tar'
    skf_reports = [
        f'{surefire_reports_path}/*xml',
        f'{surefire_reports_path}/*txt',
        html_report_path,
    ]
    skf_clean_reports = [
        {
            'uses': 'actions/delete-file@v1',
            'with': {'path': path},
        }
        for path in skf_reports
    ]

    steps = [
        *skf_clean_reports,
        {'run': f'{skf_json_file_builder}'},
        {
            'run': f'mvn squash-ta:run -Dta.success.threshold=FAIL -Dta.test.suite={{file:{json_path}}}',
            'working-directory': root_project,
        },
        {
            'run': 'tar -cvf html-reports.tar html-reports',
            'working-directory': f'{root_project}/target/squashTA',
        },
        {
            'uses': 'actions/get-file',
            'with': {'path': html_report_path},
        },
        {
            'uses': 'actions/get-file',
            'with': {'path': surefire_xml_report_path, 'type': REPORT_TYPE},
        },
        {
            'uses': 'actions/get-file',
            'with': {'path': surefire_txt_report_path},
        },
        {
            'uses': 'actions/get-file',
            'with': {'path': surefire_txt_output_report_path},
        },
    ]

    return steps


########################################################################
# 'params' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: skf/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    data = inputs['data']
    format_ = inputs['format']

    if format_ != SQUASHTM_FORMAT:
        core.fail('Unknown format value for params.')

    if data.keys() - {'global', 'test'}:
        core.fail(
            'Unexpected keys found in data, was only expecting global and/or test.'
        )

    global_params_dictionary = {}
    global_param_dictionary = {}
    context_params_dictionary = {}
    context_param_dictionary = {}

    if 'global' in data:
        for key, value in data['global'].items():
            global_param_dictionary[key] = value

        global_params_dictionary['param'] = global_param_dictionary

    global_json_object = json.dumps(global_params_dictionary)
    global_param_object = global_json_object[1:-1]

    if global_param_object:
        global_param_object = ',' + global_param_object

    if 'test' in data:
        for key, value in data['test'].items():
            context_param_dictionary[key] = value
        context_params_dictionary['param'] = context_param_dictionary
    context_json_object = json.dumps(context_params_dictionary)
    context_param_object = context_json_object[1:-1]

    if context_param_object:
        context_param_object = ',' + context_param_object

    if core.runner_on_windows():
        global_param_object = global_param_object.replace("\"", "^\"")
        context_param_object = context_param_object.replace("\"", "^\"")
        write_global_param_file = (
            f'echo.{global_param_object} > {GLOBAL_PARAMS_FILE_PATH}'
        )
        write_context_param_file = (
            f'echo.{context_param_object} > {CONTEXT_PARAMS_FILE_PATH}'
        )
    else:
        global_param_object = global_param_object.replace("\"", "\\\"")
        context_param_object = context_param_object.replace("\"", "\\\"")
        write_global_param_file = (
            f'echo {global_param_object} > {GLOBAL_PARAMS_FILE_PATH}'
        )
        write_context_param_file = (
            f'echo {context_param_object} > {CONTEXT_PARAMS_FILE_PATH}'
        )

    steps = [
        {
            'uses': 'actions/delete-file@v1',
            'with': {'path': GLOBAL_PARAMS_FILE_PATH},
        },
        {
            'uses': 'actions/delete-file@v1',
            'with': {'path': CONTEXT_PARAMS_FILE_PATH},
        },
        {'run': write_global_param_file},
        {'run': write_context_param_file},
    ]

    return steps


########################################################################
# 'skf' action


def skf_action(inputs):
    """Process 'skf' action.

    Run a SKF test suite.

    `skf` actions have a mandatory `root_project` input.

    They also have optional inputs:

    - `tests` and `script`, which specify the path of the script to run from the default ecosystem ('tests')
       Without `tests` and `script`, all test cases in the datasource are executed.

    - 'data' which allows to transmit to the specific informed tests local parameters

    - 'global_parameters' which allows to transmit to the tests  global parameters

    - 'testsuite', which allows you to run a test suite defined in a json file in skf format.
       If this parameter is used, no other optional parameter will be taken into account.


    ## Examples

    This first example runs all tests in the `foobar` project directory:

    ```yaml
    - uses: skf/skf@v1
      with:
        root_project: foobar
    ```

    This second example runs the `foo` and `bar` test scripts in the `foobar` test directory and pass
    test and global parameters:

    ```yaml
    - uses: skf/skf@v1
      with:
        root_project: foobar
        tests:
          - script: foo
            data:
                 test_param1: test_value1
                 test_param2: test_value2
          - script: sub_ecosystem/bar
            data:
                 test_param1: test_value1
                 test_param2: test_value2
        global_parameters
          global_param1: global_value1
          global_param2: global_value2

    ```
        This third example runs all tests defined in the json test suite file:

    ```yaml
    - uses: skf/skf@v1
      with:
        root_project: foobar
        testsuite: path to the json file from the root_project
    ```
    """
    root_project = inputs['root_project']

    json_path = 'test.json'
    json_dictionary = {}
    is_testsuite = False
    is_test_case_or_ecosystem = False

    if 'tests' in inputs:
        json_dictionary = {'test': []}
        test_dictionary = {}
        for test_script in inputs['tests']:
            test_dictionary['script'] = test_script['script']
            if 'data' in test_script:
                test_param_dictionary = {}
                for key in test_script['data']:
                    test_param_dictionary[key] = test_script['data'][key]
                test_dictionary['param'] = test_param_dictionary
            else:
                core.debug('No context parameter specified')
            json_dictionary['test'].append(test_dictionary)
            test_dictionary = {}
        is_test_case_or_ecosystem = True

    else:
        json_dictionary['filter'] = '**/*.ta'
        core.debug('No test case specified')

    if 'global_parameters' in inputs:
        global_parameters = inputs['global_parameters']
        global_param_dictionary = {}
        for key in global_parameters:
            global_param_dictionary[key] = global_parameters[key]
        json_dictionary['param'] = global_param_dictionary
    else:
        core.debug('No global parameter specified')

    json_object = json.dumps(json_dictionary)

    if 'testsuite' in inputs:
        target_file = inputs['testsuite']
        is_testsuite = True
    else:
        target_file = json_path
        core.debug('No testsuite specified')

    if is_testsuite & is_test_case_or_ecosystem:
        core.fail("Invalid PEAC: you cannot use both 'tests' and 'testsuite'")
    else:
        core.debug('Valid PEAC is about to be executed')

    if core.runner_on_windows():
        json_object = json_object.replace("\"", "^\"")
    else:
        json_object = json_object.replace("\"", "\\\"")

    surefire_reports_path = f'{root_project}/target/squashTA/surefire-reports/'
    html_report_path = f'{root_project}/target/squashTA/html-reports.tar'
    skf_reports = [
        f'{surefire_reports_path}/*xml',
        f'{surefire_reports_path}/*txt',
        html_report_path,
    ]

    skf_clean_reports = [
        {
            'uses': 'actions/delete-file@v1',
            'with': {'path': path},
        }
        for path in skf_reports
    ]

    steps = [
        *skf_clean_reports,
        {'run': f'echo {json_object} > {root_project}/{json_path}'},
        {
            'run': f'mvn squash-ta:run -Dta.success.threshold=FAIL -Dta.test.suite={{file:{target_file}}}',
            'working-directory': root_project,
        },
        {
            'run': 'tar -cvf html-reports.tar html-reports',
            'working-directory': f'{root_project}/target/squashTA',
        },
        {
            'uses': 'actions/get-file',
            'with': {'path': html_report_path},
        },
        {
            'uses': 'actions/get-files',
            'with': {'pattern': '*.txt'},
            'working-directory': surefire_reports_path,
        },
        {
            'uses': 'actions/get-files',
            'with': {'pattern': '*.xml', 'type': REPORT_TYPE},
            'working-directory': surefire_reports_path,
        },
    ]

    return steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    SKF_CATEGORY: skf_action,
}
