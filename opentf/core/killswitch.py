# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A killswitch service.

A killswitch service can be used to cancel a currently executing
_workflow_.

Due to the asynchronous nature of workflow executions, some actions may
still occur on execution environments and SUT when a workflow is
canceled.
"""

from opentf.commons import (
    make_status_response,
    publish,
    WORKFLOWCANCELED,
    make_app,
    run_app,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/killswitch.yaml'


########################################################################
## Main

app = make_app(
    name='killswitch',
    description='Create and start a killswitch service.',
    configfile=CONFIG_FILE,
)


@app.route('/workflows/<workflow_id>', methods=['DELETE'])
def cancel_workflow(workflow_id):
    """Cancel workflow."""
    publish(
        {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': WORKFLOWCANCELED,
            'metadata': {'name': 'User cancellation', 'workflow_id': workflow_id},
        },
        context=app.config['CONTEXT'],
    )
    return make_status_response('OK', f'Workflow {workflow_id} canceled.')


def main():
    """Start the killswitch service."""
    app.logger.info('Starting killswitch service.')
    run_app(app)


if __name__ == '__main__':
    main()
