# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# localstore

from flask import request, send_file

import requests

from opentf.commons import (
    SERVICECONFIG,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
)


########################################################################
# Helpers

LOCALSTORE = defaultdict(lambda: {})


def download_file(url, local_filename, headers):
    """Download file to local_filename."""
    r = requests.get(url, stream=True, headers=headers)
    with open(local_filename, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)


########################################################################
## Main

try:
    app = make_app(
        name='localstore',
        description='Create and start a local store service.',
        configfile=CONFIG_FILE,
        schema=SERVICECONFIG,
        static_url_path='/workflows',
        static_folder='/tmp',
    )
except ConfigError:
    sys.exit(2)


@app.route('/workflows/<workflow_id>/files', methods=['GET'])
def list_files(workflow_id):
    pass


@app.route('/workflows/<workflow_id>/files/<file_id>', methods=['PUT'])
def put_file(workflow_id, file_id):
    pass


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    valid, extra = validate_schema(EXECUTIONCOMMAND, body)
    if not valid:
        return make_status_response(
            'BadRequest', f'Not a valid ExecutionCommand request: {extra}.'
        )

    metadata = body['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id == SETUP_JOB:
        # A new job, can we make an offer?
        requiredtags = body['runs-on']
        if not isinstance(requiredtags, list):
            requiredtags = [requiredtags]
        candidates = get_matching_agents(requiredtags)
        if not candidates:
            return make_status_response(
                'OK', f'Not providing execution environment matching {requiredtags}.'
            )

        make_offers(metadata, candidates)
        return make_status_response('OK', f'Making an offer for job {job_id}')

    if metadata.get('channel_id') not in CHANNELS:
        return make_status_response(
            'OK', f'Job {job_id} not handled by this channel plugin.'
        )

    steps = list_job_steps(job_id)
    if step_sequence_id in steps:
        return make_status_response(
            'AlreadyExists',
            f'Step {step_sequence_id} already received for job {job_id}.',
        )

    if step_sequence_id == TEARDOWN_JOB:
        prepare = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': EXECUTIONRESULT,
            'metadata': metadata,
            'status': 0,
        }
        publish(prepare, context=app.config['CONTEXT'])
        try:
            del STEPS_QUEUES[JOBS_AGENTS[job_id]]
            del JOBS_AGENTS[job_id]
            del JOBS[job_id]
        except KeyError:
            pass
        return make_status_response('OK', f'Cleaning job {job_id}.')

    prepare_command(body, get_job_agent(metadata))

    return make_status_response(
        'OK', f'Step #{metadata["step_sequence_id"]} added to job {job_id} queue.'
    )


# Main


def main():
    """Start the local store service."""
    try:
        info('Subscribing to WorkflowCompleted & WorkflowCanceled.')
        uuid_1 = subscribe(kind=WORKFLOWCOMPLETED, target='inbox', app=app)
        uuid_2 = subscribe(kind=WORKFLOWCANCELED, target='inbox', app=app)
    except Exception as err:
        error('Could not subscribe to eventbus: %s.', str(err))
        sys.exit(2)

    try:
        info('Starting local store service.')
        run_app(app)
    finally:
        info('Unsubscribing from WorkflowCompleted & WorkflowCanceled.')
        unsubscribe(uuid_2, app=app)
        unsubscribe(uuid_1, app=app)


if __name__ == '__main__':
    main()
