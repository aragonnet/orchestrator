# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A receptionist service."""

from datetime import datetime

from flask import request

import yaml

from opentf.commons import (
    make_status_response,
    publish,
    validate_schema,
    WORKFLOW,
    make_app,
    run_app,
    validate_pipeline,
    make_uuid,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/receptionist.yaml'

IN_FILENAME_TEMPLATE = '/tmp/in_{uuid}_{name}'

DEFAULT_NAMESPACE = 'default'


########################################################################
## Multipart/form-data Helpers
##
## curl -X POST -F workflow=@Squashfile -F log=XX=YY http://localhost:5000/demo


def _ensure_no_duplicates():
    """Return the set of duplicates fields/files in request."""
    return {field for field in request.form if request.files.get(field)}


def _maybe_adjust_variables(workflow):
    """Prepend variables, if any."""
    if data := request.form.get('variables'):
        variables = data.splitlines()
    elif data := request.files.get('variables'):
        variables = str(data.read(), 'UTF-8').splitlines()
    else:
        variables = []
    if variables:
        workflow.setdefault('variables', {})
    for entry in variables:
        lhs, _, rhs = entry.partition('=')
        if lhs:
            workflow['variables'][lhs] = rhs


def _ensure_files_attached(workflow):
    """Ensure required files are attached."""
    expected = workflow.get('resources', {}).get('files', [])
    for idx, name in enumerate(expected):
        if not isinstance(name, str):
            continue
        if not request.files.get(name):
            app.logger.warning(f'Missing required resource file {name}')
            return False
        filename = IN_FILENAME_TEMPLATE.format(
            uuid=workflow['metadata']['workflow_id'], name=name
        )
        with open(filename, 'wb') as file:
            file.write(request.files.get(name).read())
        expected[idx] = {'name': name, 'url': filename}
    return True


########################################################################
## Main

app = make_app(
    name='receptionist',
    description='Create and start a receptionist service.',
    configfile=CONFIG_FILE,
)


@app.route('/workflows', methods=['POST'])
def handle_workflow():
    """Register a new workflow.

    # Workflow format

    Please refer to `schemas/opentestfactory.org/v1alpha1/Workflow.json'.

    Allowed content types are JSON and YAML documents:

    - application/json
    - application/*+json
    - application/x-yaml
    - text/yaml

    Content length must be specified for YAML documents, and must be
    less than 1000000 bytes long.

    # Returned value

    A _status manifest_.  A status manifest is a dictionary with the
    following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the workflow is valid (`status` is `'Success'`), `details.workflow_id` is
    the workflow ID.
    """
    try:
        if request.is_json:
            workflow = request.get_json() or {}
        elif request.mimetype in ('application/x-yaml', 'text/yaml'):
            if request.content_length and request.content_length < 1000000:
                workflow = yaml.safe_load(request.data)
            else:
                return make_status_response(
                    'Invalid',
                    'Content-size must be specified and less than 1000000 for YAML Workflows.',
                )
        elif request.mimetype == 'multipart/form-data':
            if duplicates := _ensure_no_duplicates():
                return make_status_response(
                    'Invalid', f'Entries provided as a file and a field: {duplicates}.'
                )
            workflow = yaml.safe_load(
                request.form.get('workflow') or request.files.get('workflow').read()
            )
        else:
            return make_status_response(
                'Invalid', 'Not a known format, expecting JSON or YAML.'
            )
    except Exception as err:
        return make_status_response('Invalid', f'Could not parse workflow: {err}.')

    valid, extra = validate_schema(WORKFLOW, workflow)
    if not valid:
        return make_status_response(
            'Invalid', 'Not a valid workflow manifest.', details={'error': str(extra)}
        )
    valid, extra = validate_pipeline(workflow)
    if not valid:
        return make_status_response(
            'Invalid', 'Not a valid workflow manifest.', details={'error': str(extra)}
        )

    workflow['kind'] = WORKFLOW.rsplit('/')[-1]
    workflow['apiVersion'] = 'opentestfactory.org/v1alpha1'
    annotations = workflow['metadata'].get('annotations', {})
    annotations['opentestfactory.org/ordering'] = extra
    workflow['metadata'].setdefault('namespace', DEFAULT_NAMESPACE)
    workflow['metadata']['annotations'] = annotations
    workflow['metadata']['creationTimestamp'] = datetime.now().isoformat()
    workflow['metadata']['workflow_id'] = workflow_id = make_uuid()

    if request.mimetype == 'multipart/form-data':
        _maybe_adjust_variables(workflow)
        if not _ensure_files_attached(workflow):
            missing = {
                name
                for name in workflow.get('resources', {}).get('files', [])
                if isinstance(name, str) and not request.files.get(name)
            }
            return make_status_response(
                'Invalid', f'Not all expected files were attached: {missing}.'
            )
    elif workflow.get('resources', {}).get('files', []):
        return make_status_response(
            'Invalid', 'Expecting files, must use multipart/form-data.'
        )

    resp = publish(workflow, context=app.config['CONTEXT'])
    if resp.status_code != 200:
        return make_status_response(
            'Conflict', 'Could not publish workflow.', details={'error': resp.json()}
        )

    msg = (
        f'Workflow {workflow["metadata"]["name"]} accepted (workflow_id={workflow_id}).'
    )
    app.logger.info(msg)

    return make_status_response('Created', msg, details={'workflow_id': workflow_id})


def main():
    """Start the Receptionist service."""
    app.logger.info('Starting Receptionist service.')
    run_app(app)


if __name__ == '__main__':
    main()
