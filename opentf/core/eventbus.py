# Copyright (c) 2021, 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An EventBus facade.

Can work as a standalone service, but may be replaced by a facade to an
existing eventbus/mq manager.
"""

from typing import Any, Dict, List, Mapping, Optional, Tuple, NoReturn

from collections import defaultdict
from queue import Queue

import concurrent.futures
import re
import sys
import threading

from datetime import datetime
from uuid import uuid4

from flask import request

import requests

from opentf.commons import (
    make_status_response,
    validate_schema,
    EVENTBUSCONFIG,
    SUBSCRIPTION,
    make_app,
    run_app,
    ConfigError,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/eventbus.yaml'


Object = Dict[str, Any]

NAME_PATTERN = r'^[0-9a-zA-Z]+([0-9A-Za-z-_.]*[0-9a-zA-Z])?$'
LABEL_PATTERN = r'^([^/]+/)?([0-9A-Za-z-_.]{1,63})$'
DNS_LABEL_PATTERN = r'^(?![0-9]+$)(?!-)[a-z0-9-]{1,63}(?<!-)$'

KEY = r'[a-z0-9A-Z-_./]+'
VALUE = r'[a-z0-9A-Z-_./@]+'
EQUAL_EXPR = rf'^({KEY})\s*([=!]?=)\s*({VALUE})(?:,|$)'
SET_EXPR = rf'^({KEY})\s+(in|notin)\s+\(({VALUE}(\s*,\s*{VALUE})*)\)(?:,|$)'
EXISTS_EXPR = rf'^{KEY}(?:,|$)'
NEXISTS_EXPR = rf'^!{KEY}(?:,|$)'

FIELD_SELECTOR = 'opentestfactory.org/fieldselector'
LABEL_SELECTOR = 'opentestfactory.org/labelselector'

########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args) -> NoReturn:
    """Log error message and exit."""
    app.logger.error(*args)
    sys.exit(2)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Selectors helpers


def _split_exprs(exprs: str) -> List[str]:
    """Split a comma-separated list of expressions.

    # Required parameters

    - exprs: a string

    # Returned value

    A (possibly empty) list of _expressions_.  An expression is a
    string, stripped.
    """
    result = []
    while exprs:
        match = re.match(SET_EXPR, exprs)
        if not match:
            match = re.match(EQUAL_EXPR, exprs)
        if not match:
            match = re.match(EXISTS_EXPR, exprs)
        if not match:
            match = re.match(NEXISTS_EXPR, exprs)
        if not match:
            raise ValueError(f'Invalid expression {exprs}')
        result.append(exprs[: match.end()].strip(', '))
        exprs = exprs[match.end() :].strip(', ')

    return result


def _resolve_path(path: str, obj: Object) -> Tuple[bool, Optional[str]]:
    def inner(items, obj) -> Tuple[bool, Optional[str]]:
        head, rest = items[0], items[1:]
        if head in obj:
            return (True, obj[head]) if not rest else inner(rest, obj[head])
        return False, None

    return inner(path.split('.'), obj)


def _evaluate_fields(req: str, obj: Object) -> bool:
    if req == '':
        return True
    if re.match(EXISTS_EXPR, req):
        return _resolve_path(req, obj)[0]
    if re.match(NEXISTS_EXPR, req):
        return not _resolve_path(req[1:], obj)[0]
    expr = re.match(SET_EXPR, req)
    if expr:
        key, ope, list_, _ = expr.groups()
        found, value = _resolve_path(key, obj)
        if found:
            values = [v.strip() for v in list_.split(',')]
            if ope == 'in':
                return value in values
            return value not in values
        return ope == 'notin'
    expr = re.match(EQUAL_EXPR, req)
    if expr is None:
        raise ValueError(f'Invalid expression {req}.')
    key, ope, expected = expr.groups()
    found, value = _resolve_path(key, obj)
    if found:
        if ope in ('=', '=='):
            return value == expected
        return value != expected
    return ope == '!='


def _evaluate(req: str, labels: Mapping[str, str]) -> bool:
    """Evaluate whether req matches labels.

    # Required parameters

    - req: a string
    - labels: a dictionary

    # Returned value

    A boolean.  True if `req` is satisfied by `labels`, False otherwise.

    # Raised exceptions

    A _ValueError_ exception is raised if `req` is not a valid
    expression.
    """
    if req == '':
        return True
    if re.match(EXISTS_EXPR, req):
        return req in labels
    if re.match(NEXISTS_EXPR, req):
        return req[1:] not in labels
    expr = re.match(SET_EXPR, req)
    if expr:
        key, ope, list_, _ = expr.groups()
        if key in labels:
            values = [v.strip() for v in list_.split(',')]
            if ope == 'in':
                return labels[key] in values
            return labels[key] not in values
        return ope == 'notin'
    expr = re.match(EQUAL_EXPR, req)
    if expr is None:
        raise ValueError(f'Invalid expression {req}.')
    key, ope, value = expr.groups()
    if key in labels:
        if ope in ('=', '=='):
            return labels[key] == value
        return labels[key] != value
    return ope == '!='


def _match_field_selector(obj: Object, selector: str) -> bool:
    """Return True if the object matches the selector."""
    return all(_evaluate_fields(sel, obj) for sel in _split_exprs(selector))


def _match_label_selector(obj: Object, selector: str) -> bool:
    """Return True if the service matches the selector.

    An empty selector always matches.

    The complete selector feature has been implemented.  `selector` is
    of form:

        expr[,expr]*

    where `expr` is one of `key`, `!key`, or `key op value`, with
    `op` being one of `=`, `==`, or `!=`.  The
    `key in (value[, value...])` and `key notin (value[, value...])`
    set-based requirements are also implemented.

    # Required parameters

    - obj: a Definition (a dictionary)
    - selector: a string

    # Returned value

    A boolean.
    """
    metadata = obj.get('metadata', {}).get('labels', {})
    return all(_evaluate(sel, metadata) for sel in _split_exprs(selector))


########################################################################
## Helpers

SUBSCRIPTIONS = {}
TOKENS = {}

PUBLICATIONS_QUEUES = Queue()


def _get_int_contextparam(contextparam, default):
    """Read integer context parameter."""
    try:
        return int(app.config['CONTEXT'].get(contextparam, default))
    except ValueError as err:
        fatal('Configuration parameter %s not an integer: %s', contextparam, str(err))


def is_interested(subscriber: str, publication: Dict[str, Any]) -> bool:
    """Check whether a subscriber is interested in a given publication."""
    manifest = SUBSCRIPTIONS[subscriber]
    fieldselector = manifest['metadata']['annotations'][FIELD_SELECTOR]
    labelselector = manifest['metadata']['annotations'][LABEL_SELECTOR]
    try:
        return _match_field_selector(
            publication, fieldselector
        ) and _match_label_selector(publication, labelselector)
    except Exception as err:
        error(f'While looking for {subscriber} interest: {err}')
        return False


def dispatch_publication(recipients, publication):
    """Send publication to recipients."""
    pub_id = str(uuid4())
    now = datetime.now().isoformat()
    for item in recipients:
        try:
            subscriber = SUBSCRIPTIONS[item]['spec']['subscriber']
            debug(
                'About to send publication to subscriber %s (%s).',
                item,
                str(subscriber),
            )
            req_headers = {
                'X-Subscription-ID': item,
                'X-Publication-ID': pub_id,
            }

            if item in TOKENS:
                debug('Adding authorization header from subscriptor tokens')
                req_headers['Authorization'] = TOKENS[item]

            resp = requests.post(
                subscriber['endpoint'],
                json=publication,
                headers=req_headers,
                verify=not subscriber.get('insecure-skip-tls-verify', False),
            )
            SUBSCRIPTIONS[item]['status']['publicationCount'] += 1
            SUBSCRIPTIONS[item]['status']['lastPublicationTimestamp'] = now
            SUBSCRIPTIONS[item]['status']['publicationStatusSummary'][
                resp.status_code
            ] += 1
            debug(
                'Publication status code %d from %s.', resp.status_code, str(subscriber)
            )
            if resp.status_code // 100 == 4:
                debug('Failure details: %s', resp.text)
        except Exception as err:
            error(
                'Failed to dispatch publication for subscription %s: %s.',
                item,
                str(err),
            )


def handle_publications():
    """Handle publications dispatching."""
    with concurrent.futures.ThreadPoolExecutor(
        max_workers=MAX_PUBLICATION_THREADS
    ) as executor:
        while True:
            publication, recipients = PUBLICATIONS_QUEUES.get()
            executor.submit(dispatch_publication, recipients, publication)


########################################################################
## Main


try:
    app = make_app(
        name='eventbus',
        description='Create and start an eventbus service.',
        defaultcontext={'host': '127.0.0.1', 'port': 38368, 'ssl_context': 'adhoc'},
        configfile=CONFIG_FILE,
        schema=EVENTBUSCONFIG,
    )
    MAX_PUBLICATION_THREADS = _get_int_contextparam('max_publication_threads', 4)
    info('Using a pool of %d publication threads.', MAX_PUBLICATION_THREADS)
except ConfigError:
    sys.exit(2)


@app.route('/subscriptions', methods=['POST'])
def register_subscription():
    """Register a new subscription.

    # Subscription format

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: Subscription
    metadata:
      name: {name}
    spec:
      selector:
        matchKind: {kind}
        matchLabels:
          {label}: {value}
        matchFields:
          # ...
      subscriber:
        endpoint: {endpoint}
    ```

    The `.spec.selector` part is optional (if not specified, will match
    all publications), as are its subparts.

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the subscription is successful (`.status` is `'Success'`),
    `.details.uuid` is the subscription ID.
    """
    if not request.is_json:
        return make_status_response('BadRequest', 'Not a JSON document.')

    try:
        body = request.get_json()
        if not isinstance(body, dict):
            return make_status_response('BadRequest', 'Not a JSON object.')
    except:
        return make_status_response('BadRequest', 'Not a valid JSON document.')

    valid, extra = validate_schema(SUBSCRIPTION, body)
    if not valid:
        return make_status_response(
            'Invalid',
            'Not a valid Subscription manifest.',
            details={'error': str(extra)},
        )

    fieldselector = []
    labelselector = []
    try:
        if body['spec'].get('selector'):
            selector = body['spec']['selector']
            if 'matchKind' in selector:
                fieldselector += ['kind==' + selector['matchKind']]
            if 'matchLabels' in selector:
                labelselector += [
                    f'{label}=={value}'
                    for label, value in selector['matchLabels'].items()
                ]
            if 'matchFields' in selector:
                fieldselector += [
                    f'{field}=={value}'
                    for field, value in selector['matchFields'].items()
                ]
        debug(f'FieldSelector is {fieldselector}.')
        debug(f'LabelSelector is {labelselector}.')
    except Exception as err:
        return make_status_response(
            'Invalid', 'Failed to parse selector.', details={'error': str(err)}
        )

    body['metadata']['creationTimestamp'] = datetime.now().isoformat()
    body['status'] = {
        'publicationCount': 0,
        'lastPublicationTimestamp': None,
        'publicationStatusSummary': defaultdict(int),
    }
    body['metadata']['annotations'] = {
        FIELD_SELECTOR: ','.join(fieldselector),
        LABEL_SELECTOR: ','.join(labelselector),
    }
    body['metadata']['subscription_id'] = sid = str(uuid4())
    SUBSCRIPTIONS[sid] = body

    authorization = request.headers.get('Authorization')
    if (
        authorization
        and authorization.lower() != 'bearer reuse'
        and authorization.lower().strip() != 'bearer'
    ):
        TOKENS[sid] = authorization
        debug('Registering subscriptor token')

    msg = f'Subscription {body["metadata"]["name"]} successfully registered (id={sid}).'
    info(msg)
    return make_status_response('Created', msg, details={'uuid': sid})


@app.route('/subscriptions', methods=['GET'])
def list_subscriptions():
    """Return list of subcriptions.

    # Returned value

    A _list_.  A list is a dictionary with the following entries:

    - kind: a string
    - apiVersion: a string (`'v1'`)
    - items: a list of dictionaries (`what`)
    """
    return {'apiVersion': 'v1', 'kind': 'SubscriptionsList', 'items': SUBSCRIPTIONS}


@app.route('/subscriptions/<subscription_id>', methods=['DELETE'])
def cancel_subscription(subscription_id):
    """Cancel subscription.

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the cancellation is successful, `.status` is `'Success'`.  If
    `subscription_id` is not known, `.status` is `'Failure'` and
    `reason` is `'NotFound'`.
    """
    if subscription_id not in SUBSCRIPTIONS:
        return make_status_response(
            'NotFound', f'Subscription {subscription_id} not known.'
        )
    try:
        del SUBSCRIPTIONS[subscription_id]
    except KeyError:
        pass

    if subscription_id in TOKENS:
        try:
            del TOKENS[subscription_id]
        except KeyError:
            pass
    return make_status_response('OK', f'Subscription {subscription_id} canceled.')


@app.route('/publications', methods=['POST'])
def add_publication():
    """Receive a new publication.

    Publication must be a valid JSON document.

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the publication is successful (`.status` is `'Success'`),
    `.message` tells whether there are interested listeners or not.
    """
    if not request.is_json:
        return make_status_response('BadRequest', 'Publication must be a JSON object.')

    try:
        body = request.get_json()
        if not isinstance(body, dict):
            return make_status_response('BadRequest', 'Not a JSON object.')
    except:
        return make_status_response('BadRequest', 'Not a valid JSON object.')

    if 'kind' in body and 'apiVersion' in body:
        info(f'{body["apiVersion"]}/{body["kind"]}')
    else:
        info('Processing unformatted publication')
    debug('Handling publication')
    debug(body)
    recipients = [
        subscription
        for subscription in SUBSCRIPTIONS
        if is_interested(subscription, body)
    ]

    if not recipients:
        debug('No matching subscriptions found.')
        return make_status_response(
            'OK', 'Publication received, but no matching subscription.'
        )

    debug(
        'Found the following matching subscriptions, dispatching: %s', str(recipients)
    )
    PUBLICATIONS_QUEUES.put((body, recipients))
    return make_status_response('OK', 'Publication received.')


if __name__ == '__main__':
    try:
        threading.Thread(target=handle_publications, daemon=True).start()
    except Exception as err:
        fatal('Could not start publications handling thread: %s.', str(err))
    run_app(app)
