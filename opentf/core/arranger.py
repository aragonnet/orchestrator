# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An orchestrator service."""

from typing import Any, Dict, Iterable, List, NoReturn, Optional, Union

from collections import defaultdict
from queue import Queue

import ntpath
import posixpath
import sys
import threading

from datetime import datetime

from flask import request

from opentf.commons import (
    WORKFLOW,
    WORKFLOWCANCELED,
    WORKFLOWCOMPLETED,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERRESULT,
    PROVIDERCOMMAND,
    validate_schema,
    validate_pipeline,
    make_status_response,
    subscribe,
    unsubscribe,
    publish,
    make_app,
    run_app,
    get_execution_sequence,
    ConfigError,
    make_uuid,
    get_actor,
    make_event,
)

from opentf.commons.expressions import (
    evaluate_bool as evaluate_bool_unguarded,
    evaluate_items as evaluate_items_unguarded,
    evaluate_str as evaluate_str_unguarded,
)


########################################################################
## Constants

JSON = Dict[str, Any]

CONFIG_FILE = 'conf/arranger.yaml'

CHANNEL_REQUEST = -1
CHANNEL_RELEASE = -2
CHANNEL_NOTIFY = -3

STATUS_RUNNING = 'RUNNING'  # only for workflows
STATUS_PENDING = 'PENDING'  # only for workflows
STATUS_CANCELED = 'cancelled'  # only for workflows
STATUS_SUCCESS = 'success'  # for jobs and workflows
STATUS_FAILURE = 'failure'  # for jobs and workflows
STATUS_SKIPPED = 'skipped'  # for steps

SETUP_RETRY_INCREMENT = 30  # in seconds

########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args) -> NoReturn:
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Helpers


def _get_int_contextparam(contextparam: str, default: int) -> int:
    """Read integer context parameter."""
    try:
        return int(app.config['CONTEXT'].get(contextparam, default))
    except ValueError as err:
        fatal('Configuration parameter %s not an integer: %s', contextparam, str(err))


def _set_category_labels(metadata: JSON, category: str) -> None:
    """Set category labels in metadata."""
    category_prefix = category_version = '-'
    if '/' in category:
        category_prefix, category = category.split('/')
    if '@' in category:
        category, category_version = category.split('@')
    if 'labels' not in metadata:
        metadata['labels'] = {}
    metadata['labels']['opentestfactory.org/category'] = category
    metadata['labels']['opentestfactory.org/categoryPrefix'] = category_prefix
    metadata['labels']['opentestfactory.org/categoryVersion'] = category_version


class ArrangerException(Exception):
    """Base arranger exceptions."""


class BadRequest(ArrangerException):
    """BadRequest exception."""


class NotFound(ArrangerException):
    """NotFound exception."""


class Invalid(ArrangerException):
    """Invalid exception."""


class AlreadyExists(ArrangerException):
    """AlreadyExists exception."""


def _validate_request_and_workflow_id(body: JSON, schema: str) -> str:
    """Validate body, ensuring valid schema and known workflow."""
    valid, extra = validate_schema(schema, body)
    if not valid:
        raise BadRequest(f'Not a valid {schema} result: {extra}.')

    workflow_id = body['metadata']['workflow_id']
    if workflow_id not in CURRENT_WORKFLOWS:
        raise NotFound(f'Workflow {workflow_id} not found.')

    return workflow_id


def _get_step_timeout(step: JSON, job_timeout, job_starttime) -> int:
    """Get step execution timeout, in seconds."""
    remaining_seconds = job_timeout - (datetime.now() - job_starttime).seconds
    if step_timeout := step.get('timeout-minutes'):
        return min(step_timeout * 60, remaining_seconds)
    return remaining_seconds


def _adjust_path(path1: str, path2: Optional[str], runner_on_windows: bool) -> str:
    """Join two paths.

    If the execution environment is windows-based, forward slashes will
    be replaced by backward slashes.

    No attempt is made to ensure operation makes sense.

    If `path2` is None, returns `path1`.
    """
    if path2 is None:
        if runner_on_windows:
            return ntpath.normpath(path1)
        return posixpath.normpath(path1)

    if runner_on_windows:
        return ntpath.normpath(ntpath.join(path1, path2))
    return posixpath.join(path1, path2)


def _apply_provider_hooks(
    steps: List[JSON],
    labels: JSON,
    workflow_hooks: List[JSON],
    provider_hooks: List[JSON],
    instance_hooks: List[JSON],
    contexts: JSON,
) -> None:
    """Apply hooks, if any."""
    if not workflow_hooks and not provider_hooks and not instance_hooks:
        debug('No hooks defined for provider.')
        return

    hooks = workflow_hooks + provider_hooks + instance_hooks
    before = []
    after = []
    for hook in hooks:
        debug(f'Checking hook {hook["name"]}.')
        match = False
        for event in hook.get('events', []):
            if 'category' not in event and 'categoryPrefix' not in event:
                continue
            if 'category' in event and 'categoryPrefix' in event:
                event_match = event['category'] in (
                    '_',
                    labels.get('opentestfactory.org/category'),
                )
                if event_match:
                    event_match = event['categoryPrefix'] in (
                        '_',
                        labels.get('opentestfactory.org/categoryPrefix'),
                    )
            elif 'category' in event:
                event_match = event['category'] in (
                    '_',
                    labels.get('opentestfactory.org/category'),
                )
            else:
                event_match = event['categoryPrefix'] in (
                    '_',
                    labels.get('opentestfactory.org/categoryPrefix'),
                )
            if event_match and event.get('categoryVersion'):
                event_match = event.get('categoryVersion') in (
                    '_',
                    labels.get('opentestfactory.org/categoryVersion'),
                )
            match |= event_match
        if not match:
            continue
        if 'if' in hook and not evaluate_if(hook['if'], contexts):
            debug(f'Conditional is false for hook {hook["name"]}')
            continue
        before += [step.copy() for step in hook.get('before', [])]
        after = [step.copy() for step in hook.get('after', [])] + after

    for pos, item in enumerate(before):
        steps.insert(pos, item)
    steps.extend(after)


def _test_and_remove(lock, sequence_id: int, pending: List[int]) -> bool:
    """Return True if sequence_id was in pending."""
    with lock:
        if found := (sequence_id in pending):
            pending.remove(sequence_id)
    return found


def _test_and_set(lock, step: JSON) -> bool:
    """Return True if step was not already answered."""
    with lock:
        if not (answered := step.get('_answered')):
            step['_answered'] = True
    return not answered


## Expressions


def evaluate_items(items, contexts: JSON):
    """Safely perform expression evaluation in items."""
    try:
        return evaluate_items_unguarded(items, contexts)
    except Exception as err:
        raise ExecutionError(f'Invalid expression in {items}: {err}') from None


def evaluate_if(expr: str, contexts: JSON) -> bool:
    """Safely evaluate boolean expression."""
    try:
        return evaluate_bool_unguarded(expr, contexts)
    except Exception as err:
        raise ExecutionError(f'Invalid conditional {expr}: {err}') from None


def evaluate_str(expr: str, contexts: JSON) -> str:
    """Safely perform expression evaluation in expr."""
    try:
        return evaluate_str_unguarded(expr, contexts)
    except Exception as err:
        raise ExecutionError(f'Invalid expression {expr}: {err}') from None


########################################################################
## Workflow handling

WORKFLOWS_QUEUE = Queue()
CURRENT_JOBS: Dict[str, 'Job'] = {}  # Inprocess jobs instances
CURRENT_WORKFLOWS: Dict[str, 'Workflow'] = {}  # Inprocess workflows instances
PENDING_LOCK = threading.Lock()
STEP_LOCK = threading.Lock()


class ExecutionError(Exception):
    """An ExecutionError exception.

    Only expected to be raised in a workflow thread.  Will publish
    the corresponding ExecutionError event if in this context.
    """

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        return str(self.message)


class Workflow:
    """A workflow.

    The constructor takes one parameter:

    - a workflow manifest
    """

    def __init__(self, manifest: JSON):
        """Initialize workflow.

        # Required parameter

        - manifest: a dictionary
        """
        self.manifest = manifest
        self.workflow_id = manifest['metadata']['workflow_id']

        for name, job in manifest['jobs'].items():
            job['_id'] = name  # Name at workflow.jobs level
            job.setdefault('name', name)
            job['metadata'] = {'job_origin': []}

        self.contexts = {
            'squashtf': {  # legacy
                'actor': manifest['metadata'].get('actor'),
                'token': manifest['metadata'].get('token'),
                'workflow': manifest['metadata']['name'],
            },
            'opentf': {
                'actor': manifest['metadata'].get('actor'),
                'token': manifest['metadata'].get('token'),
                'workflow': manifest['metadata']['name'],
            },
        }

        if 'actor' in manifest['metadata']:
            del manifest['metadata']['actor']
        if 'token' in manifest['metadata']:
            del manifest['metadata']['token']

        self.variables = evaluate_items(manifest.get('variables', {}), self.contexts)

        contexts_and_variables = self.contexts.copy()
        contexts_and_variables['variables'] = self.variables
        contexts_and_variables['resources'] = self.contexts['resources'] = {
            k: {
                x['name']: evaluate_items(x, contexts_and_variables)
                for x in manifest['resources'][k]
            }
            for k in manifest.get('resources', {})
        }

        self.defaults = evaluate_items(
            manifest.get('defaults', {}), contexts_and_variables
        )

        self.semaphore = threading.Semaphore(0)
        self.status = STATUS_PENDING
        self._jobs_count = 0
        self.pending_jobs = {}
        self.steps_backlog = []
        seq = get_execution_sequence(manifest)
        if seq is None:
            raise ArrangerException('Could not compute execution sequence.')
        self.jobs_backlog = [manifest['jobs'][job] for job in seq]
        self.results = {}
        self.done = False

    # Getters

    def get_contexts(self) -> JSON:
        """Get a copy of the workflow contexts.

        # Returned value

        A dictionary with three entries, one per Workflow context:

        - `squashtf` (legacy)
        - `opentf`
        - `resources`
        """
        contexts = self.contexts.copy()
        contexts['opentf'] = contexts['opentf'].copy()
        contexts['squashtf'] = contexts['squashtf'].copy()  # legacy
        return contexts

    def get_defaults(self) -> JSON:
        """Get a copy of the defaults."""
        return self.defaults.copy()

    def get_metadata(self) -> JSON:
        """Get metadata."""
        return self.manifest['metadata']

    def get_variables(self) -> JSON:
        """Get a copy of the variables.

        # Returned value

        A dictionary.  Keys are variables names, and values have already
        been evaluated.
        """
        return self.variables.copy()

    def get_workflow_id(self) -> str:
        """Get workflow_id."""
        return self.workflow_id

    # Jobs

    def get_needs(self, jobs: Union[str, Iterable[str]]) -> JSON:
        """Return outputs of job in jobs.

        # Required parameters

        - jobs: a string or a list of strings

        # Returned value

        A dictionary.  Keys are items in `jobs`.  Values are
        dictionaries with the following entries:

        - result: a string, one of `success`, `failure`, or `cancelled`.
        - outputs: a dictionary
        """
        if isinstance(jobs, str):
            jobs = [jobs]
        return {job: self.results[job] for job in jobs}

    def set_job_result(self, job: str, status: str) -> None:
        """Set job result."""
        self.results[job] = {'result': status, 'outputs': {}}

    def set_job_output(self, job: str, output: str, value: str) -> None:
        """Set job output value."""
        self.results[job]['outputs'][output] = value

    def inc_jobs_count(self) -> None:
        """Increment jobs count, raises ExecutionError if > MAX_JOBS."""
        self._jobs_count += 1
        if self._jobs_count > MAX_JOBS:
            raise ExecutionError(
                f'Jobs count exceeded ({MAX_JOBS}) for workflow {self.workflow_id}.'
            )

    def abort_pending_jobs(self) -> None:
        """Abort pending jobs, if any."""
        for job_id in list(self.pending_jobs):
            if job := CURRENT_JOBS.get(job_id):
                try:
                    job.set_status(STATUS_FAILURE)
                    job.complete()
                except Exception:
                    pass

    # Lifecycle

    def fail(
        self, reason: str = '', origin: Optional['Job'] = None, raised: bool = False
    ) -> None:
        """Set workflow status to failure.

        Publish an ExecutionError event followed by a WorkflowCanceled
        event.  Will only emit those events once.

        If `raised` is True, will not publish ExecutionError.

        # Optional parameters

        - reason: a string (an empty string by default)
        - origin: a Job or None (None by default)
        - raised: a boolean (False by default)
        """
        if self.done or self.status == STATUS_FAILURE:
            return
        self.status = STATUS_FAILURE
        self.done = True
        if not raised:
            metadata = (origin or self).get_metadata()
            if origin and origin.channel_id:
                metadata['channel_id'] = origin.channel_id
            publish(
                make_event(
                    EXECUTIONERROR,
                    metadata=metadata,
                    details={'error': reason},
                ),
                context=app.config['CONTEXT'],
            )
        self.abort_pending_jobs()
        publish(
            make_event(
                WORKFLOWCANCELED,
                metadata=(origin or self).get_metadata(),
                details={'reason': reason, 'status': self.status},
            ),
            context=app.config['CONTEXT'],
        )

    def cancel(self, reason: str = '', origin: Optional['Job'] = None) -> None:
        """Set workflow status to cancelled if appropriate.

        Can be called many times.  Will only emit a WorkflowCanceled
        event once.

        # Optional parameters

        - reason: a string (an empty string by default)
        - origin: a Job or None (None by default)
        """
        if self.done or self.status == STATUS_CANCELED:
            return
        self.done = True
        self.abort_pending_jobs()
        if self.status != STATUS_FAILURE:
            self.status = STATUS_CANCELED
            publish(
                make_event(
                    WORKFLOWCANCELED,
                    metadata=(origin or self).get_metadata(),
                    details={'reason': reason, 'status': self.status},
                ),
                context=app.config['CONTEXT'],
            )

    def complete(self) -> None:
        """Finalize workflow.

        Produces a WorkflowCompleted event.
        """
        if self.done:
            return
        self.done = True
        if self.status == STATUS_RUNNING:
            self.status = STATUS_SUCCESS
        publish(
            make_event(WORKFLOWCOMPLETED, metadata=self.get_metadata()),
            context=app.config['CONTEXT'],
        )


class Job:
    """A workflow job.

    The constructor takes 2 parameters:

    - a job manifest (a dictionary)
    - a Workflow object

    # Attributes

    _Job_ objects expose the following attributes:

    - manifest: the job manifest (a dictionary)
    - timeout: the job maximum duration, in seconds (an integer)
    - variables: a copy of the provided set of variables, merged with
      the job variables part, if any
    - defaults: a copy of the provided defaults, merged with the job
      defaults part, if any
    - sequence_id: current step sequence id (an integer, starting at 0)
    - starttime: job start time (a datetime object, None by default)

    # Methods

    - `complete()`
    - `get_metadata()`
    - `set_runner_temp()`
    - `set_runner_os()`
    - `get_contexts()`
    - `get_variables()`
    - `get_step()`
    - `set_step()`
    """

    def __init__(self, manifest: JSON, workflow: Workflow):
        self.uuid = make_uuid()
        self._id = manifest['_id']
        self.manifest = manifest
        self.workflow = workflow
        self.name = manifest['name']

        manifest['metadata']['job_id'] = self.uuid
        manifest['metadata']['name'] = self.name
        manifest['metadata']['workflow_id'] = workflow.workflow_id

        # contexts do not contain variables, they are added if needed
        self.contexts = workflow.get_contexts()
        self.contexts['opentf']['job'] = self.name
        self.contexts['squashtf']['job'] = self.name  # legacy
        self.contexts['job'] = {'status': STATUS_SUCCESS}
        self.contexts['runner'] = {}
        self.contexts['steps'] = {}
        self.contexts['needs'] = workflow.get_needs(manifest.get('needs', []))

        self.variables = workflow.get_variables()
        self.variables.update(
            evaluate_items(manifest.get('variables', {}), self.contexts)
        )
        contexts_and_variables = self.contexts.copy()
        contexts_and_variables['variables'] = self.variables

        self.defaults = workflow.get_defaults()
        self.defaults.update(
            evaluate_items(manifest.get('defaults', {}), contexts_and_variables)
        )
        for item, value in self.defaults.items():
            self.manifest.setdefault(item, value)

        self._steps_count = 0
        self.timeout = manifest.get('timeout-minutes', DEFAULT_TIMEOUT_MINUTES) * 60
        self.sequence_id = CHANNEL_REQUEST
        self.starttime = None
        self.channel_id = None
        self.status = STATUS_SUCCESS
        self._step_names = defaultdict(lambda: 1)
        self._current_step = None

        # job setup configuration
        self.setup_timeout = (
            manifest.get('timeout-minutes', DEFAULT_TIMEOUT_MINUTES) * 60
        )
        self.setup_step: Optional[JSON] = None
        self.step_origin_status = defaultdict(lambda: 0)

    def get_metadata(self) -> JSON:
        """Returb job metadata."""
        return self.manifest['metadata']

    # Runner context

    def set_runner_temp(self, temp: str) -> None:
        """Set runner.temp."""
        self.contexts['runner']['temp'] = temp

    def set_runner_os(self, runner_os: str) -> None:
        """Set runner.os."""
        self.contexts['runner']['os'] = runner_os

    def assign_channel(self, channel_id: str) -> None:
        """Set channel ID."""
        self.channel_id = channel_id

    # Contexts

    def get_contexts(self) -> JSON:
        """Get a copy of the contexts."""
        return self.contexts.copy()

    def get_variables(self) -> JSON:
        """Get a copy of the variables."""
        return self.variables.copy()

    # Current step

    def get_step(self) -> JSON:
        """Get the current step definition."""
        if not self._current_step:
            raise Exception('Internal error: no current step.')
        return self._current_step

    def set_step(self, step: JSON) -> None:
        """Set current step."""
        self._current_step = step
        name = step.get('uses', 'run').replace('/', '').replace('@', '')
        count = self._step_names[name]
        self._step_names[name] += 1
        self.contexts['opentf']['step'] = f'{name}{"" if count == 1 else count}'
        self.contexts['squashtf'][
            'step'
        ] = f'{name}{"" if count == 1 else count}'  # legacy

    def set_step_output(self, key: str, value: str) -> None:
        """Set step output."""
        step_context = self.contexts['steps'].setdefault(
            self.get_step()['id'], {'outputs': {}}
        )
        step_context['outputs'][key] = value

    def set_step_outcome(self, outcome: str) -> None:
        """Set step outcome and conclusion."""
        step_context = self.contexts['steps'].setdefault(
            self.get_step()['id'], {'outputs': {}}
        )
        step_context['outcome'] = outcome
        if outcome == STATUS_FAILURE and self.get_step().get('continue-on-error'):
            step_context['conclusion'] = STATUS_SUCCESS
        else:
            step_context['conclusion'] = outcome
        if (
            step_context['conclusion'] == STATUS_FAILURE
            and self.status == STATUS_SUCCESS
        ):
            self.set_status(STATUS_FAILURE)

    # Job

    def get_workflow_id(self) -> str:
        """Get workflow_id."""
        return self.workflow.get_workflow_id()

    def set_status(self, status: str) -> None:
        """Set job status."""
        self.contexts['job']['status'] = self.status = status

    def inc_steps_count(self) -> None:
        """Increment steps count, raises ExecutionError if > MAX_STEPS."""
        self._steps_count += 1
        if self._steps_count > MAX_STEPS:
            raise ExecutionError(
                f'Steps count exceeded ({MAX_STEPS}) in job {self.uuid} for workflow {self.get_workflow_id()}.'
            )

    def process(self) -> None:
        """Process job."""
        contexts = self.get_contexts()
        contexts_and_variables = contexts.copy()
        contexts_and_variables['variables'] = self.variables
        if not evaluate_if(self.manifest.get('if', 'true'), contexts_and_variables):
            return

        self.workflow.inc_jobs_count()
        self.starttime = datetime.now()

        CURRENT_JOBS[self.uuid] = self
        self.workflow.pending_jobs[self.uuid] = set()

        if 'steps' in self.manifest:
            steps = [{'run': 'CHANNEL_REQUEST'}] + self.manifest['steps'].copy()
            for step in steps:
                if 'id' in step:
                    self.contexts['steps'][step['id']] = {'outputs': {}}
                else:
                    step['id'] = make_uuid()
                step.setdefault('metadata', {})
                step['metadata'].setdefault('step_origin', [])
            self.workflow.steps_backlog = steps
            return

        generator = self.manifest['generator']
        _set_category_labels(self.manifest['metadata'], generator)
        publish(
            make_event(
                GENERATORCOMMAND,
                metadata=self.manifest['metadata'],
                generator=evaluate_str(generator, contexts),
                **{
                    'with': evaluate_items(
                        self.manifest.get('with'), contexts_and_variables
                    ),
                },
            ),
            context=app.config['CONTEXT'],
        )
        if not self.workflow.semaphore.acquire(
            timeout=self.timeout - (datetime.now() - self.starttime).seconds
        ):
            raise TimeoutError()

    def complete(self) -> None:
        """Finalize job."""
        metadata = self.manifest['metadata'].copy()
        job_id = metadata['job_id']

        del self.workflow.pending_jobs[job_id]
        del CURRENT_JOBS[job_id]

        if 'steps' in self.manifest:
            metadata['step_sequence_id'] = CHANNEL_RELEASE
            metadata['channel_id'] = self.channel_id
            step_origin_status = self.step_origin_status
            metadata['step_origin_status'] = {
                k: v + 1 for k, v in step_origin_status.items()
            }
            metadata['step_origin'] = list(step_origin_status)
            metadata['step_id'] = make_uuid()
            prepare = make_event(EXECUTIONCOMMAND, metadata=metadata, scripts=[])
            prepare['runs-on'] = self.manifest['runs-on']
            publish(prepare, context=app.config['CONTEXT'])

        self.workflow.set_job_result(self._id, self.status)
        for output, value in self.manifest.get('outputs', {}).items():
            self.workflow.set_job_output(
                self._id, output, evaluate_str(value, self.contexts)
            )

        if self.status == STATUS_FAILURE and not self.manifest.get('continue-on-error'):
            self.workflow.fail(
                f'At least one step in job {job_id} returned a non-zero value.',
                origin=self,
            )


def _process_step(step: Optional[JSON], job: Job) -> None:
    """Process step.

    There was at least one step in the workflow step_backlog.

    Will either publish an EXECUTIONCOMMAND message or a PROVIDERCOMMAND
    message.

    Locks the thread until there is an answer.

    Raises a _Timeout_ exception if no answer in specified time.

    Raises an _ExecutionError_ exception if too many steps.
    """
    if step is None:
        # Republishing offer
        publish(job.setup_step, context=app.config['CONTEXT'])
        step_timeout = SETUP_RETRY_INCREMENT
        job.setup_timeout -= step_timeout
        if not job.workflow.semaphore.acquire(timeout=step_timeout):
            raise TimeoutError()
        return

    job.inc_steps_count()
    job.set_step(step)

    contexts = job.get_contexts()
    step_variables = job.get_variables()
    step_variables.update(evaluate_items(step.get('variables', {}), contexts))
    contexts_and_variables = contexts.copy()
    contexts_and_variables['variables'] = step_variables
    if not evaluate_if(step.get('if', 'success()'), contexts_and_variables):
        job.set_step_outcome(STATUS_SKIPPED)
        return

    step_timeout = _get_step_timeout(step, job.timeout, job.starttime)
    if step_variables:
        step['variables'] = step_variables
    for item, value in job.defaults.get('run', {}).items():
        if item not in step:
            step[item] = value
    if 'name' in step:
        step['name'] = evaluate_str(step['name'], contexts_and_variables)
    if 'with' in step:
        step['with'] = evaluate_items(step['with'], contexts_and_variables)
    if 'run' in step:
        step['run'] = evaluate_str(step['run'], contexts_and_variables)
    for key in ('id', 'uses', 'working-directory'):
        if key in step:
            step[key] = evaluate_str(step[key], contexts)
    metadata = {
        'name': contexts['opentf']['step'],
        'workflow_id': job.get_workflow_id(),
        'job_id': job.manifest['metadata']['job_id'],
        'job_origin': job.manifest['metadata']['job_origin'],
        'step_id': step['id'],
        'step_origin': step['metadata']['step_origin'],
    }
    if 'uses' in step:
        _set_category_labels(metadata, step['uses'])
        prepare = make_event(PROVIDERCOMMAND, metadata=metadata, contexts=contexts)
        prepare['runs-on'] = job.manifest['runs-on']
        prepare['step'] = {
            k: v
            for k, v in step.items()
            if k
            in [
                'name',
                'id',
                'uses',
                'with',
                'variables',
                'working-directory',
                'timeout-minutes',
                'continue-on-error',
            ]
        }
    else:
        job.workflow.pending_jobs[metadata['job_id']].add(job.sequence_id)
        metadata['step_sequence_id'] = job.sequence_id
        if job.sequence_id == CHANNEL_REQUEST:
            metadata['name'] = job.name
            step_timeout = SETUP_RETRY_INCREMENT
            scripts = []
        else:
            metadata['channel_id'] = job.channel_id
            scripts = [step['run']]
            step_origin_status = job.step_origin_status
            for cso in metadata['step_origin']:
                step_origin_status[cso] += 1
        prepare = make_event(EXECUTIONCOMMAND, metadata=metadata, scripts=scripts)
        prepare['runs-on'] = job.manifest['runs-on']
        if step_variables:
            prepare['variables'] = step_variables
        if 'working-directory' in step:
            prepare['working-directory'] = step['working-directory']
        if job.sequence_id == CHANNEL_REQUEST:
            job.setup_timeout -= step_timeout
            job.setup_step = prepare
        job.sequence_id += 1

    publish(prepare, context=app.config['CONTEXT'])
    if not job.workflow.semaphore.acquire(timeout=step_timeout):
        raise TimeoutError()


def workflow_thread(workflow_id: str):
    """Handles a workflow.

    Produces either a WorkflowCompleted or a WorkflowCanceled event.

    A WorkflowCompleted event denotes the successful execution of the
    workflow.  The status is 'success'.

    A WorkflowCanceled event denote the unsuccessful execution of the
    workflow.  The status is either 'cancelled' (user cancellation or
    timeout) or 'failure' (execution error).

    There may be any number of concurrent workflow threads (but they
    are handling different workflows).
    """
    workflow = CURRENT_WORKFLOWS[workflow_id]
    workflow.status = STATUS_RUNNING
    current_job: Optional[Job] = None

    while True:
        # end thread if workflow canceled
        if workflow.status in (STATUS_FAILURE, STATUS_CANCELED):
            break

        try:
            # process a backlog step if available
            if workflow.steps_backlog:
                if not current_job:
                    raise ArrangerException('backlog step with no current job.')
                _process_step(workflow.steps_backlog.pop(0), current_job)
                continue

            if current_job is not None:
                # job has been fully processed, release resources
                current_job.complete()
                current_job = None

            # no step available, try a job
            if workflow.jobs_backlog:
                current_job = Job(workflow.jobs_backlog.pop(0), workflow)
                current_job.process()
                continue

            # done?
            if workflow.status == STATUS_RUNNING and not workflow.pending_jobs:
                workflow.complete()
                break
        except TimeoutError as err:
            if current_job is None:
                message = f'Internal error while handling workflow {workflow_id}: timeout with no job.'
                error(message)
                workflow.fail(message)
                raise Exception('Internal error, timeout with no job.') from err
            if current_job.sequence_id == 0 and current_job.setup_timeout >= 0:
                debug('No offer received for job, retrying.')
                workflow.steps_backlog = [None] + workflow.steps_backlog
                continue
            message = f'Timeout error while handling workflow {workflow_id}: {err}'
            debug(message)
            workflow.cancel(message)
        except ExecutionError as err:
            error(f'Execution error while handling workflow {workflow_id}: {err}')
            workflow.fail(str(err.message))
        except Exception as err:
            message = f'Internal error while handling workflow {workflow_id}: {err}'
            error(message)
            workflow.fail(message)
            raise

    debug(f'Ending thread for workflow {workflow_id}')


def prepare_workflows():
    """Prepare workflow handler and start it."""
    while True:
        manifest = WORKFLOWS_QUEUE.get()

        workflow_id = manifest['metadata']['workflow_id']
        if workflow_id in CURRENT_WORKFLOWS:
            continue

        try:
            CURRENT_WORKFLOWS[workflow_id] = Workflow(manifest)
            threading.Thread(
                target=workflow_thread, args=(workflow_id,), daemon=True
            ).start()
        except Exception as err:
            msg = f'Internal error while preparing workflow thread: {err}'
            error(msg)
            if workflow_id in CURRENT_WORKFLOWS:
                try:
                    del CURRENT_WORKFLOWS[workflow_id]
                except KeyError:
                    pass
            publish(
                make_event(
                    WORKFLOWCANCELED,
                    metadata=manifest['metadata'],
                    details={'reason': msg, 'status': STATUS_FAILURE},
                ),
                context=app.config['CONTEXT'],
            )


########################################################################
## Requests Handlers


def process_workflow(body: JSON) -> None:
    """Process a WORKFLOW event.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _AlreadyExist_ if workflow
    is already being processed.
    """
    valid, extra = validate_schema(WORKFLOW, body)
    if not valid:
        raise BadRequest(f'Not a valid {WORKFLOW}: {extra}.')

    workflow_id = body['metadata'].get('workflow_id')
    if not workflow_id:
        raise BadRequest('Missing workflow_id.')

    if workflow_id in CURRENT_WORKFLOWS:
        raise AlreadyExists(f'Workflow {workflow_id} already exists.')

    if auth := request.headers.get('Authorization'):
        actor = get_actor()
        token = auth.split()[1]
    else:
        actor = '_localhost_'
        token = None
    body['metadata']['actor'] = actor
    body['metadata']['token'] = token

    WORKFLOWS_QUEUE.put(body)


def process_generatorresult(body: JSON) -> None:
    """Process an GENERATORRESULT event.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.  _Invalid_ if `body` does not refer to
    a valid result.
    """
    workflow_id = _validate_request_and_workflow_id(body, GENERATORRESULT)
    job_id = body['metadata']['job_id']
    if job_id not in CURRENT_WORKFLOWS[workflow_id].pending_jobs:
        raise Invalid(
            'Was not expecting expansion results for job.',
            details={'job_id': job_id, 'workflow_id': workflow_id},
        )

    job = CURRENT_JOBS[job_id]
    valid, extra = validate_pipeline(body)
    if not valid:
        job.workflow.fail(reason=str(extra), origin=job)
        raise Invalid(
            'Circular dependencies in generated jobs.',
            details={'error': str(extra)},
        )

    parent_tags = job.manifest.get('runs-on', [])
    parent_vars = job.get_variables()
    if isinstance(parent_tags, str):
        parent_tags = [parent_tags]

    job_origin = body['metadata']['job_origin'] + [job_id]
    for name, jobdef in body['jobs'].items():
        jobdef['_id'] = name
        jobdef.setdefault('name', name)
        jobdef.setdefault('metadata', {})
        jobdef['metadata'] = {'job_origin': job_origin}
        tags = jobdef.get('runs-on')
        if isinstance(tags, str):
            tags = [tags]
        for tag in parent_tags:
            if tag not in tags:
                tags.append(tag)
        jobdef['runs-on'] = tags
        variables = parent_vars.copy()
        variables.update(jobdef.get('variables', {}))
        if variables:
            jobdef['variables'] = variables

    seq = get_execution_sequence(body)
    if seq is None:
        job.workflow.fail('Could not get execution sequence for jobs.')
        raise Invalid('Unexpected circular dependencies in generated jobs.')
    jobs = [body['jobs'][item] for item in seq]
    job.workflow.jobs_backlog = jobs + job.workflow.jobs_backlog
    job.workflow.semaphore.release()


def _add_providersteps(job: Job, parent_step: JSON, response: JSON) -> None:
    """Add returned steps to job backlog."""
    metadata = response['metadata']
    parent_vars = parent_step.get('variables', {})
    parent_directory = parent_step.get('working-directory')
    runner_on_windows = job.contexts.get('runner', {}).get('os') == 'windows'

    step_origin = metadata['step_origin'] + [metadata['step_id']]
    contexts = job.contexts.copy()
    contexts['variables'] = parent_step.get('variables', {})
    if 'with' in parent_step:
        contexts['inputs'] = parent_step['with']

    steps = response['steps']
    try:
        _apply_provider_hooks(
            steps,
            metadata.get('labels', {}),
            workflow_hooks=job.workflow.manifest.get('hooks', []),
            provider_hooks=response.get('hooks', []),
            instance_hooks=[],
            contexts=contexts,
        )

        for step in steps:
            if 'id' not in step:
                step['id'] = make_uuid()
            if parent_directory:
                step['working-directory'] = _adjust_path(
                    parent_directory, step.get('working-directory'), runner_on_windows
                )
            step.setdefault('metadata', {})
            step['metadata']['step_origin'] = step_origin
            variables = parent_vars.copy()
            variables.update(step.get('variables', {}))
            if variables:
                step['variables'] = variables

        job.workflow.steps_backlog = steps + job.workflow.steps_backlog
        job.workflow.semaphore.release()
    except Exception as err:
        error(f'Failed to process ProviderResult: {err}.')
        job.workflow.fail(reason=str(err), origin=job)


def process_providerresult(body: JSON) -> None:
    """Process a PROVIDERRESULT event.

    Duplicates and out-of-band responses are silently ignored.  If
    everything is fine, the workflow semaphore is released.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.  _Invalid_ if `body` does not refer to
    a valid result.
    """
    _ = _validate_request_and_workflow_id(body, PROVIDERRESULT)

    metadata = body['metadata']

    try:
        job = CURRENT_JOBS[metadata['job_id']]
    except KeyError:
        raise Invalid(f'Job {metadata["job_id"]} not current.') from None

    if not (parent_step := job.get_step()):
        raise Invalid(f'Job {metadata["job_id"]} has no steps.')

    if metadata['step_id'] != parent_step['id']:
        raise Invalid(f'Step {metadata["step_id"]} not current.')

    if not _test_and_set(STEP_LOCK, parent_step):
        raise Invalid(f'Step {metadata["step_id"]} already answered.')

    _add_providersteps(job, parent_step, body)


def process_executionresult(body: JSON) -> None:
    """Process a EXECUTIONRESULT event.

    Duplicates and out-of-band responses are silently ignored.  If
    everything is fine, the workflow semaphore is released.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.  _Invalid_ if `body` does not refer to
    a valid result.
    """
    workflow_id = _validate_request_and_workflow_id(body, EXECUTIONRESULT)
    try:
        pending_jobs = CURRENT_WORKFLOWS[workflow_id].pending_jobs
    except KeyError:
        raise Invalid(f'Workflow {workflow_id} not current.') from None

    metadata = body['metadata']
    job_id = metadata['job_id']

    try:
        steps_backlog = pending_jobs[job_id]
    except KeyError:
        if metadata['step_sequence_id'] == CHANNEL_RELEASE:
            return
        raise Invalid(f'Job {job_id} not pending.') from None

    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id < CHANNEL_REQUEST:  # CHANNEL_NOTIFY or CHANNEL_RELEASE
        return

    if not _test_and_remove(PENDING_LOCK, step_sequence_id, steps_backlog):
        raise Invalid(f'Job {job_id} not expecting step {step_sequence_id}.')

    try:
        job = CURRENT_JOBS[job_id]
    except KeyError:
        raise Invalid(f'Job {job_id} no longer current.') from None

    if step_sequence_id == CHANNEL_REQUEST:
        try:
            job.set_runner_os(metadata['channel_os'])
            job.set_runner_temp(metadata['channel_temp'])
            job.assign_channel(metadata['channel_id'])
        except KeyError:
            error(f'Missing channel metadata on offer for job {job_id}.')
            raise Invalid('Missing channel metadata.') from None
        job.setup_timeout = -1

    if body.get('status', 0) != 0:
        job.set_step_outcome(STATUS_FAILURE)
    for output, value in body.get('outputs', {}).items():
        job.set_step_output(output, value)

    job.workflow.semaphore.release()


def process_executionerror(body: JSON) -> None:
    """Process an ExecutionError event.

    Change the status of the workflow to STATUS_FAILURE, so that the
    workflow thread can die.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.
    """
    workflow_id = _validate_request_and_workflow_id(body, EXECUTIONERROR)

    try:
        CURRENT_WORKFLOWS[workflow_id].fail(raised=True)
    except Exception as err:
        error(f'Something went wrong while failing workflow {workflow_id}: {err}.')


def process_canceled(body: JSON) -> None:
    """Process a WorkflowCanceled event.

    Change the status of the workflow to STATUS_CANCELED if it's not
    STATUS_FAILURE, so that the workflow thread can die.

    # Required parameters

    - body: a JSON document

    # Raised exceptions

    _BadRequest_ if body is malformed.  _NotFound_ if unknown
    `metadata.workflow_id`.
    """
    workflow_id = _validate_request_and_workflow_id(body, WORKFLOWCANCELED)

    try:
        CURRENT_WORKFLOWS[workflow_id].cancel()
    except Exception as err:
        error(f'Something went wrong while cancelling workflow {workflow_id}: {err}.')


########################################################################
## Main

SUBSCRIPTIONS = {
    WORKFLOW: process_workflow,
    GENERATORRESULT: process_generatorresult,
    PROVIDERRESULT: process_providerresult,
    EXECUTIONRESULT: process_executionresult,
    EXECUTIONERROR: process_executionerror,
    WORKFLOWCANCELED: process_canceled,
}


try:
    app = make_app(
        name='arranger',
        description='Create and start an orchestrator service.',
        configfile=CONFIG_FILE,
    )
    MAX_JOBS = _get_int_contextparam('max_jobs', 1024)
    MAX_STEPS = _get_int_contextparam('max_job_steps', 1024)
    MAX_WORKERS = _get_int_contextparam('max_workers', 4)

    DEFAULT_TIMEOUT_MINUTES = _get_int_contextparam('default_timeout_minutes', 360)
except ConfigError:
    sys.exit(2)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It accepts and process incoming publications as specified via
    `SUBSCRIPTIONS`.

    # Workflow format

    Please refer to `schemas/opentestfactory.org/v1alpha1/Workflow.json'.

    # Returned value

    A _status manifest_.  A status manifest is a dictionary with the
    following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response(
            'Invalid', 'Not an object or no kind or no apiVersion specified.'
        )

    if processor := SUBSCRIPTIONS.get(f'{body["apiVersion"]}/{body["kind"]}'):
        try:
            processor(body)
            status = make_status_response('OK', '')
        except ArrangerException as err:
            extra = err.args[1] if len(err.args) > 1 else {}
            status = make_status_response(err.__class__.__name__, err.args[0], **extra)
        except Exception as err:
            status = make_status_response(
                'InternalError', f'Internal error while processing {body}: {err}.'
            )
    else:
        status = make_status_response('BadRequest', 'Unexpected event received.')

    return status


def main(svc):
    """Start the Orchestrator service."""
    info('Starting workflow preparation thread.')
    try:
        threading.Thread(target=prepare_workflows, daemon=True).start()
    except Exception as err:
        fatal('Could not start workflow preparation thread: %s.', str(err))

    info(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
    try:
        subs = [subscribe(kind=kind, target='inbox', app=svc) for kind in SUBSCRIPTIONS]
    except Exception as err:
        fatal('Could not subscribe to eventbus: %s.', str(err))

    info('Starting orchestrator service.')
    try:
        info('  maximum number of jobs per workflow: %d', MAX_JOBS)
        info('  maximum number of steps per job: %d', MAX_STEPS)
        info('  maximum number of workers per workflow: %d', MAX_WORKERS)
        info('  default job timeout in minutes: %d', DEFAULT_TIMEOUT_MINUTES)
        run_app(svc)
    finally:
        for sub in subs:
            unsubscribe(sub, app=svc)


if __name__ == '__main__':
    main(app)
