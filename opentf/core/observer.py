# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An observer service."""

import sys

from datetime import datetime, timedelta
from math import ceil
from queue import Queue
import threading

from flask import request

from opentf.commons import (
    ConfigError,
    WORKFLOW,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERCOMMAND,
    PROVIDERRESULT,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    NOTIFICATION,
    make_status_response,
    subscribe,
    unsubscribe,
    validate_schema,
    make_app,
    run_app,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/observer.yaml'

DEFAULT_PER_PAGE = 100
MAX_PER_PAGE = 1000

########################################################################
## Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args):
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


def clean_expired_workflows():
    """Remove completed workflows events."""
    try:
        for workflow_id in list(WORKFLOWS_LASTSEEN):
            if (datetime.now() - WORKFLOWS_LASTSEEN[workflow_id]) > RETENTION_PERIOD:
                info(f'Cleaning events for completed workflow {workflow_id}.')
                try:
                    del WORKFLOWS_STATUS[workflow_id]
                except KeyError:
                    pass
                try:
                    del WORKFLOWS_WORKERS[workflow_id]
                except KeyError:
                    pass
                try:
                    del WORKFLOWS_LASTSEEN[workflow_id]
                except KeyError:
                    pass
    except Exception as err:
        error(f'Could not clean expired events: {err}.')


def _is(event, schema):
    return f'{event["apiVersion"]}/{event["kind"]}' == schema


def _get_int_contextparam(contextparam, default):
    """Read integer context parameter."""
    try:
        return int(app.config['CONTEXT'].get(contextparam, default))
    except ValueError as err:
        fatal('Configuration parameter %s not an integer: %s', contextparam, str(err))


########################################################################

PENDINGEVENT_QUEUE = Queue()


def record_event():
    """Record events.

    Events are serialized so that no race condition occurs and so that
    no event is lost.
    """
    while True:
        try:
            schema, workflow_id, body = PENDINGEVENT_QUEUE.get()
            if schema == WORKFLOW and workflow_id not in WORKFLOWS_STATUS:
                WORKFLOWS_STATUS[workflow_id] = []
                WORKFLOWS_WORKERS[workflow_id] = set()
            if workflow_id not in WORKFLOWS_STATUS:
                warning(
                    f'Out of band message, workflow {workflow_id} unknown in {body}.'
                )
                WORKFLOWS_STATUS[workflow_id] = []
                WORKFLOWS_WORKERS[workflow_id] = set()
            WORKFLOWS_STATUS[workflow_id].append(body)

            if schema in (WORKFLOWCOMPLETED, WORKFLOWCANCELED):
                clean_expired_workflows()
                WORKFLOWS_LASTSEEN[workflow_id] = datetime.now()

            if schema == NOTIFICATION and 'worker' in body.get('spec', {}):
                try:
                    worker_id = body['spec']['worker']['worker_id']
                except KeyError:
                    continue
                try:
                    status = body['spec']['worker']['status']
                except KeyError:
                    continue
                if status == 'setup':
                    WORKFLOWS_WORKERS[workflow_id].add(worker_id)
                elif status == 'teardown':
                    if worker_id in WORKFLOWS_WORKERS[workflow_id]:
                        WORKFLOWS_WORKERS[workflow_id].remove(worker_id)
                    else:
                        warning(f'Unknown worker {worker_id}.')
        except Exception as err:
            error(f'Internal error while recording event: {err}.')


########################################################################
## Main

WORKFLOWS_LASTSEEN = {}
WORKFLOWS_STATUS = {}
WORKFLOWS_WORKERS = {}

try:
    app = make_app(
        name='observer',
        description='Create and start an observer service.',
        configfile=CONFIG_FILE,
    )
    RETENTION_PERIOD = timedelta(
        minutes=_get_int_contextparam('retention_period_minutes', 60)
    )
    info(
        'Using a workflow retention period of %d minutes.',
        RETENTION_PERIOD.seconds // 60,
    )
except ConfigError:
    sys.exit(2)


@app.route('/workflows', methods=['GET'])
def get_workflows():
    """Get running and recently completed workflows.

    # Returned value

    A _status manifest_.  `details.items` is a possibly empty list of
    workflow IDs.
    """
    return make_status_response(
        'OK', 'Running and recent workflows', details={'items': list(WORKFLOWS_STATUS)}
    )


@app.route('/workflows/<workflow_id>/status', methods=['GET'])
def get_workflow_status(workflow_id):
    """Get workflow status.

    Pagination is per <https://datatracker.ietf.org/doc/html/rfc8288>

    # Required parameters

    - workflow_id: an UUID

    # Optional request parameters

    - page: an integer (1-based, 1 by default)
    - per_page: an integer (100 by default, 1000 max)

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflow: a string, one of `PENDING`, `RUNNING`,
    `DONE`, or `FAILED`.
    """
    if workflow_id not in WORKFLOWS_STATUS:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')

    page = request.args.get('page', default=1, type=int)
    per_page = min(
        request.args.get('per_page', default=DEFAULT_PER_PAGE, type=int), MAX_PER_PAGE
    )

    events = WORKFLOWS_STATUS[workflow_id]
    if [event for event in events if _is(event, WORKFLOWCOMPLETED)]:
        msg, summary = 'Workflow completed', 'DONE'
    elif [event for event in events if _is(event, WORKFLOWCANCELED)]:
        msg, summary = 'Workflow canceled', 'FAILED'
    else:
        msg, summary = 'Workflow in progress', 'RUNNING'

    response = make_status_response(
        'OK',
        msg,
        details={
            'items': events[(page - 1) * per_page : page * per_page],
            'status': summary,
        },
    )

    last = ceil(len(events) / per_page)
    links = [
        f'<{request.base_url}?page=1&{per_page=}>; rel="first"',
        f'<{request.base_url}?page={last}&{per_page=}>; rel="last"',
    ]
    if page > 1:
        links.append(f'<{request.base_url}?page={page-1}&{per_page=}>; rel="prev"')
    if page < last:
        links.append(f'<{request.base_url}?page={page+1}&{per_page=}>; rel="next"')
    response.headers['Link'] = ','.join(links)

    return response


@app.route('/workflows/status', methods=['GET'])
def get_workflows_status():
    """Get workflows status.

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflow: a string, one of `IDLE` or `BUSY`.
    """
    running = set()
    for workflow_id, events in WORKFLOWS_STATUS.items():
        for event in events:
            if _is(event, WORKFLOWCOMPLETED) or _is(event, WORKFLOWCANCELED):
                break
        else:
            running.add(workflow_id)
    for workflow_id, workers in WORKFLOWS_WORKERS.items():
        if workers:
            running.add(workflow_id)

    return make_status_response(
        'OK',
        f'{len(running)} workflows in progress'
        if running
        else 'No workflow in progress',
        details={'items': list(running), 'status': 'BUSY' if running else 'IDLE'},
    )


@app.route('/workflows/<workflow_id>/workers', methods=['GET'])
def get_workflow_workers(workflow_id):
    """Get workflow workers.

    # Required parameters

    - workflow_id: an UUID

    # Returned value

    A _status manifest_.  `.details.status` contains an API-readable
    status of the workflow workers: a string, one of `BUSY` or
    `IDLE`.
    """
    if workflow_id not in WORKFLOWS_WORKERS:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')

    workers = WORKFLOWS_WORKERS[workflow_id]
    return make_status_response(
        'OK',
        f'{len(workers)} workers active on workflow',
        details={
            'items': list(workers),
            'status': 'BUSY' if workers else 'IDLE',
        },
    )


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It may be of any kind.

    # Returned value

    A _status manifest_.  A status manifest is a dictionary with the
    following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response(
            'Invalid', 'Not an object or no kind or no apiVersion specified.'
        )

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    valid, extra = validate_schema(schema, body)
    if not valid:
        return make_status_response('BadRequest', f'Not a valid {schema}: {extra}')

    if not body.get('metadata', {}).get('workflow_id'):
        return make_status_response('Invalid', 'No workflow_id specified.')

    workflow_id = body['metadata']['workflow_id']
    PENDINGEVENT_QUEUE.put((schema, workflow_id, body))

    return make_status_response('OK', '')


SUBSCRIPTIONS = [
    WORKFLOW,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    GENERATORCOMMAND,
    GENERATORRESULT,
    PROVIDERCOMMAND,
    PROVIDERRESULT,
    EXECUTIONCOMMAND,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    NOTIFICATION,
]


def main():
    """Start the Observer service."""
    try:
        info('Starting event recording thread.')
        threading.Thread(target=record_event, daemon=True).start()
    except Exception as err:
        fatal('Could not start event recording thread: %s.', str(err))

    try:
        info('Subscribing to all events.')
        uuids = [subscribe(kind=s, target='inbox', app=app) for s in SUBSCRIPTIONS]
    except Exception as err:
        fatal('Could not subscribe to events: %s.', str(err))

    try:
        info('Starting Observer service.')
        run_app(app)
    finally:
        for uuid in uuids:
            unsubscribe(uuid, app=app)


if __name__ == '__main__':
    main()
