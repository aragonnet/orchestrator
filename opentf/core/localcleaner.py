# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A LocalCleaner service."""

import os
import sys
import threading

from datetime import datetime, timedelta
from queue import Queue
from time import sleep

from flask import request

from opentf.commons import (
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    EXECUTIONRESULT,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
)


########################################################################
## Constants

CONFIG_FILE = 'conf/localcleaner.yaml'

SLEEP_DELAY = 60  # in seconds
PENDING_DURATION = 3600  # in seconds

########################################################################
## Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args):
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################


def clean_attachments():
    """Cleanup attachments."""
    while True:
        try:
            stamp, workflow_id = CLEANUP_PENDING.get()
            if workflow_id not in WORKFLOWS_ATTACHMENTS:
                continue

            if (datetime.now() - stamp) < timedelta(seconds=PENDING_DURATION):
                CLEANUP_PENDING.put((stamp, workflow_id))
                sleep(SLEEP_DELAY)
                continue

            for attachment in WORKFLOWS_ATTACHMENTS[workflow_id]:
                try:
                    os.unlink(attachment)
                except IOError as err:
                    warning(f'Could not remove attachment {attachment}: {err}')

            del WORKFLOWS_ATTACHMENTS[workflow_id]
        except Exception as err:
            error(f'Internal error while cleaning attachments: {err}')


########################################################################
## Main

CLEANUP_PENDING = Queue()
WORKFLOWS_ATTACHMENTS = {}

app = make_app(
    name='localcleaner',
    description='Create and start a localcleaner service.',
    configfile=CONFIG_FILE,
)


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It may be of any kind.

    # Returned value

    A _status manifest_.  A status manifest is a dictionary with the
    following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body:
        return make_status_response('Invalid', 'Not an object or no kind specified.')

    if not body.get('metadata', {}).get('workflow_id'):
        return make_status_response('Invalid', 'No workflow_id specified.')

    workflow_id = body['metadata']['workflow_id']
    if body['kind'] == EXECUTIONRESULT:
        if attachments := body.get('attachments'):
            if workflow_id not in WORKFLOWS_ATTACHMENTS:
                WORKFLOWS_ATTACHMENTS[workflow_id] = set()
            for item in attachments:
                WORKFLOWS_ATTACHMENTS[workflow_id].add(item)
    elif (
        body['kind'] in (WORKFLOWCOMPLETED, WORKFLOWCANCELED)
        and workflow_id in WORKFLOWS_ATTACHMENTS
    ):
        debug(f'Scheduling cleanup for workflow {workflow_id} attachments.')
        CLEANUP_PENDING.put((datetime.now(), workflow_id))

    return make_status_response('OK', '')


def main():
    """Start the LocalCleaner service."""
    try:
        info('Starting attachment cleanup thread.')
        threading.Thread(target=clean_attachments, daemon=True).start()
    except Exception as err:
        error('Could not start attachment cleanup thread: %s.', str(err))
        sys.exit(2)

    try:
        info('Subscribing to workflow events.')
        uuid_1 = subscribe(kind=WORKFLOWCOMPLETED, target='inbox', app=app)
        uuid_2 = subscribe(kind=WORKFLOWCANCELED, target='inbox', app=app)
        uuid_3 = subscribe(kind=EXECUTIONRESULT, target='inbox', app=app)
    except Exception as err:
        fatal('Could not subscribe to eventbus: %s.', str(err))

    try:
        info('Starting LocalCleaner service.')
        run_app(app)
    finally:
        unsubscribe(uuid_3, app=app)
        unsubscribe(uuid_2, app=app)
        unsubscribe(uuid_1, app=app)


if __name__ == '__main__':
    main()
